//
//  MyUploadVC.swift
//  Honeycomb
//
//  Created by samarjeet on 15/09/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit
import AlamofireImage
import SwiftDate

class MyUploadVC: BaseViewController //,UICollectionViewDelegate,UICollectionViewDataSource{
{
    private var myUploadsBOsArray:Array<MyUploadsBO>?;
    
    @IBOutlet weak var myuploadtablview: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
     ProgressView.shared.showProgressView(view)
       

        
        self.callMyUploads();
        
        self.addSlideMenuButton();
        
        self.myuploadtablview.tableFooterView = UIView();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: Private Methods (Vineet)
    
    private func callMyUploads()
    {
        let currentTimestamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))     // Current epoch time
        print("currentTimestamp - \(currentTimestamp)");
        
        
        let myUploadsDO = MyUploadsDO();
        myUploadsDO.prepareRequestWithParameters(parameters: UserRegistrationHelper.getInstance().getUid() != nil ? (UserRegistrationHelper.getInstance().getUid()! as AnyObject?): ("" as AnyObject?),
                                                 currentTimestamp as AnyObject?)
        myUploadsDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                    
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    if(baseDO.serverErrorBO!.errorText! == "token mismatch")
                    {
                        let alertController = UIUtil.standardUIAlertController(title: "Alert", message: AppConstants.TEXT_1, preferredStyle: .alert)
                        self.present(alertController, animated: true, completion: nil);
                        
                        App_UI_Helper.appDelegate().logout();
                    }
                }
            }
            else
            {
                // Response received successfully.
                
                if(( myUploadsDO.response as! MyUploadsResponse).myUploadsBOsArray != nil && (myUploadsDO.response as! MyUploadsResponse).myUploadsBOsArray!.count > 0)
                {
                    self.myUploadsBOsArray = (myUploadsDO.response as! MyUploadsResponse).myUploadsBOsArray!
                        ProgressView.shared.hideProgressView()
                    
                    self.myuploadtablview.reloadData();
                }
            }
        }
    }

}

extension MyUploadVC: UITableViewDataSource, UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyuploadCell", for: indexPath) as! MyUploadVideoTableViewCell
        
        cell.videoTitleLabel.text = self.myUploadsBOsArray![indexPath.row].title!;
        
        let createdDate = DateInRegion(string: self.myUploadsBOsArray![indexPath.row].created_at!, format: .iso8601(options: .withInternetDateTime), fromRegion: Region.Local(autoUpdate: true))
        let createdDateString = createdDate?.string(custom: "MMM dd, yyyy");
        
        cell.dateLabel.text = createdDateString;//self.myUploadsBOsArray![indexPath.row].created_at!;
        
        if let imageURL = (self.myUploadsBOsArray![indexPath.row].field_video_posters)
        {
            if(!self.myUploadsBOsArray![indexPath.row].field_video_posters!.isEmpty)
            {
                cell.thumbnilImageView.af_setImage(withURL: URL(string: imageURL)!)
            }
        }
        else
        {
            cell.thumbnilImageView.image = UIImage(named:"HoneyComb_icon");
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.myUploadsBOsArray != nil ? self.myUploadsBOsArray!.count : 0;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let indexPath = tableView.indexPathForSelectedRow
        
        //getting the current cell from the index path
        let currentCell = tableView.cellForRow(at: indexPath!)! as! MyUploadVideoTableViewCell
        
        // getting the text of that cell
        // let currentItem = nbArray[indexPath!.row]["nid"]
        // print("Myvalue\(currentItem!)")
        
        //self.navigationController?.pushViewController(detailViewController, animated: true)
        
    }
}
