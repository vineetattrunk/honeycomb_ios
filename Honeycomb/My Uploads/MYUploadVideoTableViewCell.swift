//
//  UploadVideoTableViewCell.swift
//  Honeycomb
//
//  Created by iSparsh on 9/25/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

class MyUploadVideoTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var thumbnilImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
