//
//  ProfileDetailVC.swift
//  Honeycomb
//
//  Created by iSparsh on 10/11/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

class ProfileDetailVC: BaseViewController,UITextFieldDelegate
{

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var loctionThumbnil: UIImageView!
    
    @IBOutlet weak var mobilenoThumbnil: UIImageView!
    @IBOutlet weak var emailThumbnil: UIImageView!
    
    @IBOutlet weak var descriptionThumbnil: UIImageView!
    @IBOutlet weak var locationTextfield: UITextField!
    
    @IBOutlet weak var mobilenoTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    private var isBeingEdited:Bool = false;
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        addSlideMenuButton()
        addDoneButtonOnKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileDetailVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileDetailVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        userName.text = UserRegistrationHelper.getInstance().getUserName() != nil ? UserRegistrationHelper.getInstance().getUserName() : "";
        locationTextfield.text = UserRegistrationHelper.getInstance().getLocation() != nil ? UserRegistrationHelper.getInstance().getLocation() : "";
        locationTextfield.isUserInteractionEnabled = false
        locationTextfield.borderStyle = UITextBorderStyle.none
        
        emailTextfield.text = UserRegistrationHelper.getInstance().getEmailID() != nil ? UserRegistrationHelper.getInstance().getEmailID() : "";
        emailTextfield.isUserInteractionEnabled = false
        emailTextfield.borderStyle = UITextBorderStyle.none
        
        mobilenoTextfield.text = UserRegistrationHelper.getInstance().getMobileNumber() != nil ? UserRegistrationHelper.getInstance().getMobileNumber() : "";
        mobilenoTextfield.isUserInteractionEnabled = false
        mobilenoTextfield.borderStyle = UITextBorderStyle.none
        
        descriptionTextView.text = "Description"
        descriptionTextView.textColor = UIColor.white
        descriptionTextView.isUserInteractionEnabled = false
       // descriptionTextView.borderStyle = UITextBorderStyle.none
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 150
            }
        }
    }
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ProfileDetailVC.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
       // self.textfield.inputAccessoryView = doneToolbar
        self.descriptionTextView.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        //self.textfield.resignFirstResponder()
        self.descriptionTextView.resignFirstResponder()
    }




    
   @IBAction func editProfileAction(_ sender: Any)
   {
        if(self.isBeingEdited)
        {
            self.isBeingEdited = false;
            
            locationTextfield.isUserInteractionEnabled = false
//            locationTextfield.borderStyle = UITextBorderStyle.none
            locationTextfield.layer.borderColor = UIColor.clear.cgColor;
//            locationTextfield.layer.borderWidth = 1.0;
            
            emailTextfield.isUserInteractionEnabled = false
            emailTextfield.layer.borderColor = UIColor.clear.cgColor;
            
            mobilenoTextfield.isUserInteractionEnabled = false
            mobilenoTextfield.layer.borderColor = UIColor.clear.cgColor;
            
            descriptionTextView.isUserInteractionEnabled = false
            descriptionTextView.layer.borderColor = UIColor.clear.cgColor;
            
            self.callEditProfile();
        }
        else
        {
            self.isBeingEdited = true;
            
            locationTextfield.isUserInteractionEnabled = true
            locationTextfield.layer.borderColor = UIColor.white.cgColor;
            locationTextfield.layer.borderWidth = 1.0;
            locationTextfield.delegate = self
            locationTextfield.tag = 1
            locationTextfield.returnKeyType = UIReturnKeyType.next
            
            emailTextfield.isUserInteractionEnabled = true
            emailTextfield.layer.borderColor = UIColor.white.cgColor;
            emailTextfield.layer.borderWidth = 1.0;
            emailTextfield.delegate = self
            emailTextfield.tag = 2
            emailTextfield.returnKeyType = UIReturnKeyType.next
            
            mobilenoTextfield.isUserInteractionEnabled = true
            mobilenoTextfield.layer.borderColor = UIColor.white.cgColor;
            mobilenoTextfield.layer.borderWidth = 1.0;
            mobilenoTextfield.delegate = self
            mobilenoTextfield.tag = 3
            mobilenoTextfield.returnKeyType = UIReturnKeyType.next
            
            descriptionTextView.isUserInteractionEnabled = true
            descriptionTextView.layer.borderColor = UIColor.white.cgColor;
            descriptionTextView.layer.borderWidth = 1.0;
            descriptionTextView.returnKeyType = UIReturnKeyType.next
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
        
    {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField
        {
            nextField.becomeFirstResponder()
            
        }
        else
        {
            textField.resignFirstResponder()
            return true;
        }
        
        return false
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return true
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //TODO: Vineet call this from Edit profile page
    
    private func callEditProfile()
    {
        ProgressView.shared.showProgressView(self.view);
        
        let editProfileDO = EditProfileDO();
        editProfileDO.prepareRequestWithParameters(parameters: self.locationTextfield.text as AnyObject?,
                                                   self.mobilenoTextfield.text as AnyObject?,
                                                   UserRegistrationHelper.getInstance().getEmailID() as AnyObject?,
                                                   self.descriptionTextView.text as AnyObject?)
        editProfileDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            ProgressView.shared.hideProgressView()
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    if(baseDO.serverErrorBO!.errorText! == "token mismatch")
                    {
                        let alertController = UIUtil.standardUIAlertController(title: "Alert", message: AppConstants.TEXT_1, preferredStyle: .alert)
                        self.present(alertController, animated: true, completion: nil);
                        
                        App_UI_Helper.appDelegate().logout();
                    }
                }
            }
            else
            {
                // Response received successfully.
                
                print("...Response received successfully...");
                
                if let loginBO = (editProfileDO.response as! LoginResponse).loginBO
                {
                    
                    UserRegistrationHelper.getInstance().clearUserRegistrationInfo();
                    UserRegistrationHelper.getInstance().setUserRegistrationInfo(name: loginBO.name!,
                                                                                     emailID: loginBO.mail!,
                                                                                     mobileNumber: loginBO.field_phone!,
                                                                                     location: loginBO.field_address!,
                                                                                     csrfToken: loginBO.csrf_token!,
                                                                                     uid: loginBO.uid!);
                    
                }
            }
        }
    }
}


