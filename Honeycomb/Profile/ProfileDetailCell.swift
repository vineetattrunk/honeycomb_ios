//
//  ProfileDetailCell.swift
//  Honeycomb
//
//  Created by iSparsh on 10/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

class ProfileDetailCell: UITableViewCell {

    @IBOutlet weak var loctionThumbnil: UIImageView!
    
    @IBOutlet weak var mobilenoThumbnil: UIImageView!
    @IBOutlet weak var emailThumbnil: UIImageView!
    
    @IBOutlet weak var descriptionThumbnil: UIImageView!
    @IBOutlet weak var locationTextfield: UITextField!
    
    
    @IBOutlet weak var mobilenoTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
