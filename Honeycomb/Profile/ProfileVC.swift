//
//  ProfileVC.swift
//  Honeycomb
//
//  Created by samarjeet on 15/09/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

class ProfileVC: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var profileCellArray = ["PROFILE","HISTORY","LOCATION SETTING","PAYMENT SETUP","LOG OUT","DELETE ACCOUNT"]
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.

//    }
 

}
extension ProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.profileCellArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 0) {
            // Table view cells are reused and should be dequeued using a cell identifier.
            let cellIdentifier = "ProfileVCCell"
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ProfileVCCell  else {
                fatalError("The dequeued cell is not an instance of TableViewCell.")
            }
            
           // cell.configureUIWithData(data:self.productDetail)
            cell.profileType.text = "SELLER"
            cell.profileName.text = UserRegistrationHelper.getInstance().getUserName() != nil ? UserRegistrationHelper.getInstance().getUserName() : "";
            return cell
        }
        else {
            
            // Table view cells are reused and should be dequeued using a cell identifier.
            let cellIdentifier = "cell"
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? UITableViewCell  else {
                fatalError("The dequeued cell is not an instance of TableViewCell.")
            }
            
            cell.textLabel?.text = profileCellArray[indexPath.row - 1] as! String
            cell.backgroundColor = UIColor.black
            cell.textLabel?.textColor = .white
            return cell
            }
       
       // return cell
      
    }
    
  
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 1
        {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileDetailVC") as! ProfileDetailVC
                self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if(indexPath.row == 5)
        {
            
            App_UI_Helper.appDelegate().window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
            
            UserRegistrationHelper.getInstance().clearUserRegistrationInfo();
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if(indexPath.row == 0){
                
                return 150
            }else{
                
                return  50
            }
            
        }
        //
        //var indexPath = tableView.indexPathForSelectedRow
//        if indexPath.row == 1{
//        self.performSegue(withIdentifier: "segue_ProfileDetail", sender: self)
//        }
        
        //getting the current cell from the index path
        //let currentCell = tableView.cellForRow(at: indexPath!)! as! PitchTableViewCell
        
        // getting the text of that cell
        // let currentItem = nbArray[indexPath!.row]["nid"]
        // print("Myvalue\(currentItem!)")
        
        //self.navigationController?.pushViewController(detailViewController, animated: true)
        
    }
    
}




