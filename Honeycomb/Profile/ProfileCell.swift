//
//  ProfileCell.swift
//  Honeycomb
//
//  Created by iSparsh on 10/11/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {
    @IBOutlet weak var profileThumbnil: UIImageView!
    @IBOutlet weak var profileType: UILabel!
    @IBOutlet weak var profileName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
