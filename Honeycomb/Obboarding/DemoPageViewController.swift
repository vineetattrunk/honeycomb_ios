//
//  DemoPageViewController.swift
//  
//
//  Created by Aurionpro2 on 02/02/16.
//
//

import UIKit

protocol DemoPageDelegate
{
    func demoPageRemoved()
}

class DemoPageViewController: UIViewController, UIScrollViewDelegate{

    var delegate:DemoPageDelegate?
    
  @IBOutlet weak var buttonView: UIView!
  @IBOutlet weak var navigationPageControlView: UIView!
    var demoParentViewController:UIViewController!
    @IBOutlet weak var propLblSkip: UILabel!
    @IBOutlet weak var propPgControl: UIPageControl!
    @IBOutlet weak var propBtnSkip: UIButton!
    
    @IBOutlet weak var propImgLogo: UIImageView!
    
    @IBOutlet weak var propViewDemoHolder: UIView!
    
    @IBOutlet weak var propViewDemoMainHolder: UIView!
    
    @IBOutlet weak var propSVDemo: UIScrollView!
    
    @IBOutlet weak var seondScreen: UIImageView!
    @IBOutlet weak var thirdScreen: UIImageView!
    
    
    
    @IBOutlet weak var fourthScreen: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.propViewDemoMainHolder.translatesAutoresizingMaskIntoConstraints = true
        self.propViewDemoHolder.translatesAutoresizingMaskIntoConstraints = true
        self.propViewDemoMainHolder.frame = CGRect(x:0,y: 0,width: SCREEN_WIDTH * 4 ,height:self.propViewDemoMainHolder.frame.height)
        self.propViewDemoHolder.frame = CGRect(x:SCREEN_WIDTH, y:0,width: SCREEN_WIDTH, height:self.propViewDemoMainHolder.frame.height)
        
        self.propSVDemo.contentSize = CGSize(width:SCREEN_WIDTH * 4, height:SCREEN_HEIGHT)
        
        self.propSVDemo.isPagingEnabled = true
        
        self.propViewDemoHolder.alpha = 0
        self.propImgLogo.alpha = 0
        
        self.propImgLogo.frame = CGRect(x:(SCREEN_WIDTH/2) - (self.propImgLogo.frame.width/2), y:self.propImgLogo.frame.origin.y, width:self.propImgLogo.frame.width, height:self.propImgLogo.frame.height)
        
        
        propSVDemo.delegate = self
        
        
        thirdScreen.frame = CGRect(x:seondScreen.frame.width,y: 0, width:SCREEN_WIDTH,height: SCREEN_HEIGHT)
        
        
    }
    
    
    @IBAction func actBtnSkip(sender: UIButton) {
        
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        
        delegate?.demoPageRemoved()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.propImgLogo.translatesAutoresizingMaskIntoConstraints = true
        self.propViewDemoHolder.translatesAutoresizingMaskIntoConstraints = true
//        self.propImgLogo.frame = CGRectMake( -SCREEN_WIDTH, (SCREEN_HEIGHT/2) - (self.propImgLogo.frame.height / 2), self.propImgLogo.frame.width, self.propImgLogo.frame.height)
        
        UIView.animate(withDuration: 2.0, delay: 0.5, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            
                self.propImgLogo.alpha = 1
            
            }) { (Bool) -> Void in
                
                self.propViewDemoHolder.alpha = 1
                
                UIView.animate(withDuration: 0.7, delay: 0.5, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
                    self.propImgLogo.frame = CGRect(x: -SCREEN_WIDTH,y: self.propImgLogo.frame.origin.y, width:self.propImgLogo.frame.width, height:self.propImgLogo.frame.height)
                    
                        self.propImgLogo.alpha = 0
                    self.propViewDemoHolder.frame = CGRect(x:0, y:0, width:SCREEN_WIDTH, height:SCREEN_HEIGHT)
                    
                        if let temp = self.demoParentViewController as? LoginViewController
                        {
                           // temp.propImgPoweredBy.alpha = 0
                            //temp.versionLabel.alpha = 0
                        }
                    

                    }) { (Bool) -> Void in
                        
                        self.propImgLogo.alpha = 0
                        
                        
                }
        }
        
      if IS_IPHONE_5 {
        
//        self.navigationPageControlView.translatesAutoresizingMaskIntoConstraints = true
//        self.navigationPageControlView.frame = CGRectMake(self.navigationPageControlView.frame.origin.x, self.navigationPageControlView.frame.origin.y - 100, self.navigationPageControlView.frame.width, self.navigationPageControlView.frame.height)
        
        
        self.buttonView.translatesAutoresizingMaskIntoConstraints = true
        self.buttonView.frame = CGRect(x:self.buttonView.frame.origin.x, y:self.buttonView.frame.origin.y + 10, width:self.buttonView.frame.width, height:self.buttonView.frame.height)
        
        self.propPgControl.translatesAutoresizingMaskIntoConstraints = true
        self.propPgControl.frame = CGRect(x:self.propPgControl.frame.origin.x, y:self.propPgControl.frame.origin.y + 10, width:self.propPgControl.frame.width, height:self.propPgControl.frame.height)
        

        
      }else if IS_IPHONE_4_OR_LESS {
        
        self.buttonView.translatesAutoresizingMaskIntoConstraints = true
        self.buttonView.frame = CGRect(x:self.buttonView.frame.origin.x, y:SCREEN_HEIGHT - self.buttonView.frame.height + 13, width:self.buttonView.frame.width,height: self.buttonView.frame.height)
        
        self.propPgControl.translatesAutoresizingMaskIntoConstraints = true
        self.propPgControl.frame = CGRect(x:self.propPgControl.frame.origin.x,y: SCREEN_HEIGHT - self.propPgControl.frame.height + 4, width:self.propPgControl.frame.width, height:self.propPgControl.frame.height)
        
      }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onPageChange(sender: UIPageControl) {
        
        if sender.currentPage == 0
        {
           // self.propSVDemo.setContentOffset(CGPoint(width:0,height:0), animated: true)
            self.propSVDemo.setContentOffset(CGPoint(x:0,y:0), animated: true)
        }
        else if sender.currentPage == 1
        {
            self.propSVDemo.setContentOffset(CGPoint(x:SCREEN_WIDTH,y: 0), animated: true)
        }
        else if sender.currentPage == 2
        {
            self.propSVDemo.setContentOffset(CGPoint(x:SCREEN_WIDTH * 2,y: 0), animated: true)
        }
        else if sender.currentPage == 3
        {
            self.propSVDemo.setContentOffset(CGPoint(x:SCREEN_WIDTH * 3,y: 0), animated: true)
        }

        
        if sender.currentPage == 3
        {
            propLblSkip.text = "DONE"
        }
        else
        {
            propLblSkip.text = "SKIP"
        }
        
        
    }
    

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if self.propSVDemo.contentOffset.x == 0
        {
            propPgControl.currentPage = 0
        }
        else if self.propSVDemo.contentOffset.x == SCREEN_WIDTH
        {
            propPgControl.currentPage = 1
        }
        else if self.propSVDemo.contentOffset.x == (SCREEN_WIDTH * 2)
        {
            propPgControl.currentPage = 2
        }
        else if self.propSVDemo.contentOffset.x == (SCREEN_WIDTH * 3)
        {
            propPgControl.currentPage = 3
        }
        if propPgControl.currentPage == 3
        {
            propLblSkip.text = "DONE"
        }
        else
        {
            propLblSkip.text = "SKIP"
        }
        
    }
    

    
}
