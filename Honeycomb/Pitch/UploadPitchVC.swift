//
//  UploadPitchVC.swift
//  Honeycomb
//
//  Created by iSparsh on 10/16/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit
import AlamofireImage

class UploadPitchVC: BaseViewController, UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    var field_video_posters: String!;
    var field_video_json:String!;
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var inputPitchtitle: InputView!
    @IBOutlet weak var inputtags: InputView!
    @IBOutlet weak var inputViewDescription: InputView!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        addSlideMenuButton()
        
//        ProgressView.shared.showProgressView(view)
//        ProgressView.shared.hideProgressView()
    }

    
    @IBAction func clearTextfields(_ sender: UIBarButtonItem)
    {
        inputPitchtitle.textFieldView.text = nil
        inputtags.textFieldView.text = nil
        inputViewDescription.textFieldView.text = nil
    }
    
    @IBAction func submitAction(_ sender: Any)
    {
        self.callAddPitch(type: "pitch_video",
                          title: self.inputPitchtitle.textFieldView.text!,
                          field_location_name:"",
                          field_geoLocation_lat:0.0,
                          field_geoLocation_lng:0.0,
                          field_video_tags:self.inputtags.textFieldView.text!,
                          field_video_details:self.inputViewDescription.textFieldView.text!,
                          field_video_posters: self.field_video_posters,
                          field_video_json: self.field_video_json)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UICollectionViewDataSource
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : PitchVCCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PitchVCCell
        
//        if let imageURL = (self.myUploadsBOsArray![indexPath.row].field_video_posters)
//        {
//            if(!self.myUploadsBOsArray![indexPath.row].field_video_posters!.isEmpty)
//            {
//                cell.thumbnilImageView.af_setImage(withURL: URL(string: imageURL)!)
//            }
//        }
//        else
//        {
//            cell.thumbnilimage.image = UIImage(named:"HoneyComb_icon");
//        }
        cell.thumbnilimage.af_setImage(withURL: URL(string: field_video_posters)!)
        
        
        
       return cell
        
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0.0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0.0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
            
            return CGSize(width: UIScreen.main.bounds.size.width/2.0 , height: UIScreen.main.bounds.size.width/2.0 )
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
    }
    
    //MARK: Private Methods (Vineet)
    
    private func callAddPitch(type:String, title:String, field_location_name:String?, field_geoLocation_lat:Double?, field_geoLocation_lng:Double?, field_video_tags:String, field_video_details:String, field_video_posters:String, field_video_json:String)
    {
        let addPitchDO = AddPitchDO();
        addPitchDO.prepareRequestWithParameters(parameters: type as AnyObject?,
                                                title as AnyObject?,
                                                field_location_name as AnyObject?,
                                                field_geoLocation_lat as AnyObject?,
                                                field_geoLocation_lng as AnyObject?,
                                                field_video_tags as AnyObject?,
                                                field_video_details as AnyObject?,
                                                field_video_posters as AnyObject?,
                                                field_video_json as AnyObject)
        addPitchDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                    
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    if(baseDO.serverErrorBO!.errorText! == "token mismatch")
                    {
                        let alertController = UIUtil.standardUIAlertController(title: "Alert", message: AppConstants.TEXT_1, preferredStyle: .alert)
                        self.present(alertController, animated: true, completion: nil);
                        
                        App_UI_Helper.appDelegate().logout();
                    }
                }
            }
            else
            {
                // Response received successfully.
                
                if let videoUploadBO = (addPitchDO.response as! VideoUploadResponse).videoUploadBO
                {
                    let alert = UIAlertController(title: "SUBMISSION COMPLETE", message: "It usually takes a minute to process the video completely.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "MY UPLOADS", style: .default) { (action) -> Void in
                        
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "MyUploadVC") as! MyUploadVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
