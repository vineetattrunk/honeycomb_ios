//
//  FavoritesVC.swift
//  Honeycomb
//
//  Created by samarjeet on 15/09/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit
import Cloudinary
import MobileCoreServices
import Alamofire
import SwiftyJSON

class PitchVC: BaseViewController
{
   
    @IBOutlet weak var tableView: UITableView!
    
    var nbArray = [AnyObject]()
    var publicid:String?
    var videoUrl:String?
    var convertedString:String?
    
    private var field_video_posters: String!;
    private var field_video_json:String!;
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        addSlideMenuButton()
        self.tableView.tableFooterView = UIView();

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func uploadfromGallery(_ sender: Any)
    {
        galleryVideo()
    }
    
    @IBAction func recordfromCamera(_ sender: Any)
    {
        self.startCameraFromViewController(self, withDelegate: self)
    }
    func galleryVideo()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            
            let videoPicker = UIImagePickerController()
            videoPicker.delegate = self
            videoPicker.sourceType = .photoLibrary
            videoPicker.mediaTypes = [kUTTypeMovie as String]
            videoPicker.allowsEditing = true
            
            self.present(videoPicker, animated: true, completion: nil)
        }
    }
    
    func startCameraFromViewController(_ viewController: UIViewController, withDelegate delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            return false
        }
        
        let cameraController = UIImagePickerController()
        cameraController.sourceType = .camera
        cameraController.allowsEditing = true
        cameraController.mediaTypes = [kUTTypeMovie as NSString as String]
        cameraController.allowsEditing = true
        cameraController.delegate = delegate
        
        present(cameraController, animated: true, completion: nil)
        return true
    }
    
    @objc func video(_ videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject)
    {
        if(error == nil)
        {
            // Video is saved locally successfully.
            
            print("Video saved Locally successfully...");
            
        }
        else
        {
            self.present(UIUtil.standardUIAlertController(title: "Error", message: "Failed to cache video locally. Please retry.", preferredStyle: .alert), animated: true, completion: nil)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension PitchVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PitchCell", for: indexPath) as! PitchTableViewCell
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        // return self.nbArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let indexPath = tableView.indexPathForSelectedRow
        
        //getting the current cell from the index path
        let currentCell = tableView.cellForRow(at: indexPath!)! as! PitchTableViewCell
    }
    
}

extension PitchVC: UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        dismiss(animated: true, completion: nil)
        // Handle a movie capture
        if mediaType == kUTTypeMovie
        {
            guard let path = (info[UIImagePickerControllerMediaURL] as? URL)?.path else { return }
            
            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path)
            {
                print("Path - \(path)");
                
                //////--------Cloudinary API---------------------------///////
                
                var videoThumbnaiURL :String?
                var videoData: Data?
                do
                {
                    videoData = try NSData(contentsOfFile: (path), options: NSData.ReadingOptions.alwaysMapped) as Data
                    print(videoData!)
                }
                catch
                {
                    
                }
               ProgressView.shared.showProgressView(view)
                let config = CLDConfiguration(cloudinaryUrl: "cloudinary://619327614426169:cVux-ozY4bvVi_Dksuj1OmMrI0U@trunk")
                let cloudinary = CLDCloudinary(configuration: config!)
                
                let params = CLDUploadRequestParams().setResourceType(.video)
                cloudinary.createUploader().signedUpload(data: videoData!, params: params as? CLDUploadRequestParams).responseRaw({ (result, error) in
                    
                    if let res = result as? [String:AnyObject]
                    {
                        //completionHandler(CLDUploadResult(json: res), nil)
                        
                        print("Result - \(res)");
                        
                          ProgressView.shared.hideProgressView()
                        
//                        let jdata = try! JSONSerialization.data(withJSONObject: res,options: .prettyPrinted)
//                        let jsonStr = NSString(data: jdata, encoding: String.Encoding.utf8.rawValue)! as String
//                        print("jsonStr - \(jsonStr)");
                        
                        self.publicid = res["public_id"] as? String
//                        print("Here is pid \(self.publicid)")
                        
                        self.videoUrl = res["secure_url"] as? String
//                        print("Here is videoUrl \(self.videoUrl)")
                        
                        videoThumbnaiURL = self.videoUrl!.replacingOccurrences(of: ".mov", with: ".jpg")
                        print("videoThumbnaiURL! - \(videoThumbnaiURL!)")
                        
                        // Converting the response object received from Cloudinary into JSON object
                        let jsonObj = JSON(res)
                        
                        // Generate the string representation of the JSON value
                        let jsonString = jsonObj.rawString()!
                        let escapedAddress = jsonString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                        
                        //--------********Video Send API-------****************
                        
                        let videoFileName = (path as NSString).lastPathComponent
                        
//                        self.callAddPitch(type: "pitch_video",
//                                             title: videoFileName,
//                                             field_location_name:"LocationName",
//                                             field_geoLocation_lat:0.0,
//                                             field_geoLocation_lng:0.0,
//                                             field_video_tags:"Tags",
//                                             field_video_details:"Details",
//                                             field_video_posters: videoThumbnaiURL!,
//                                             field_video_json: escapedAddress)
                        
                        self.field_video_posters = videoThumbnaiURL!;
                        self.field_video_json = escapedAddress;
                    }
                    
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let uploadPitchVC = storyboard.instantiateViewController(withIdentifier: "UploadPitchVC") as! UploadPitchVC
                    uploadPitchVC.field_video_posters = self.field_video_posters;
                    uploadPitchVC.field_video_json = self.field_video_json;
                    
                    self.navigationController?.pushViewController(uploadPitchVC, animated: true)
                })
                
                UISaveVideoAtPathToSavedPhotosAlbum(path, self, #selector(NBRequestListView.video(_:didFinishSavingWithError:contextInfo:)), nil)
            }
        }
    }
    
    //MARK: Private Methods (Vineet)
    
    private func callAddPitch(type:String, title:String, field_location_name:String?, field_geoLocation_lat:Double?, field_geoLocation_lng:Double?, field_video_tags:String, field_video_details:String, field_video_posters:String, field_video_json:String)
    {
        let addPitchDO = AddPitchDO();
        addPitchDO.prepareRequestWithParameters(parameters: type as AnyObject?,
                                                   title as AnyObject?,
                                                   field_location_name as AnyObject?,
                                                   field_geoLocation_lat as AnyObject?,
                                                   field_geoLocation_lng as AnyObject?,
                                                   field_video_tags as AnyObject?,
                                                   field_video_details as AnyObject?,
                                                   field_video_posters as AnyObject?,
                                                   field_video_json as AnyObject)
        addPitchDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                    
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    if(baseDO.serverErrorBO!.errorText! == "token mismatch")
                    {
                        let alertController = UIUtil.standardUIAlertController(title: "Alert", message: AppConstants.TEXT_1, preferredStyle: .alert)
                        self.present(alertController, animated: true, completion: nil);
                        
                        App_UI_Helper.appDelegate().logout();
                    }
                }
            }
            else
            {
                // Response received successfully.
                
                if let videoUploadBO = (addPitchDO.response as! VideoUploadResponse).videoUploadBO
                {
                    if(videoUploadBO.returnMsg != "token mismatch")
                    {
                        
                        //TODO: Ask Pawan what needs to be done here...
                    }
                }
            }
        }
    }
}


extension PitchVC: UINavigationControllerDelegate {
}



