//
//  LoginLoginInteractor.swift
//  VIPER Sample
//
//  Created by Samar Dubey on 12/06/2016.
//  Copyright © 2016 Samar Dubey. All rights reserved.
//
import Foundation


class LoginInteractor: LoginInteractorInput {

    weak var output: LoginInteractorOutput!
    let authService : AuthService!
    var Authtoken:String?
    
    init(authService:AuthService) {
        self.authService = authService
    }
    
    func performLoginAPICall(_ username:String, pass:String) {
        
        // BYPass the Validationpart  by adding loginDidComplete out side do try catself.output.loginDidComplete()
        //self.output.loginDidComplete()
//        self.authService.login(User(username:username,password:pass)) { (data, error) -> (Void) in
//            if let data = data {
//                do {
//                    let json = try JSONSerialization.jsonObject(with: data, options: [])
//                    print("This Is My Data \(json)")
//                    self.output.loginDidComplete()
//                } catch {
//                    print(error)
//                }
//            }pawan@123.com
//        }
//
       
        self.authService.login(User(username:username, password: pass)) { (data, error) -> (Void) in

            print(username)
            let genericError = NSError(domain: "Network/Server Error", code: 100, userInfo: nil)

            do {
                if let data = data,
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] {
                    print(json)
                    self.Authtoken = json["csrf_token"] as! String
                    let currentUser = User.decode(json: json)
                    
                    print(currentUser)

                    if currentUser.isValid() {


                        // set here local current user here

                        self.output.loginDidComplete()
                                UserDefaults.standard.set(true, forKey: "UserLoggedIn")
                                UserDefaults.standard.synchronize()
                        
//
//                        NSUserDefaults.standardUserDefaults().setValue(email, forKey: KToken)
//
//                        NSUserDefaults.standardUserDefaults().synchronize()
                        UserDefaults.standard.set(self.Authtoken, forKey: "csrf_token")
                        
                        UserDefaults.standard.synchronize()
                        
                        print(self.Authtoken!)
                        
                        
                        
//                        if let headers = json["csrf_token"] as? String{
//                            self.Authtoken = headers["csrf_token"]
//                            UserDefaults.standard.set(self.Authtoken, forKey: "csrf_token")
//                            UserDefaults.standard.synchronize()
//                        }

                    } else {

                        let code:Int? = json["errorCode"]?.intValue
                        //self.output.loginDidFailWithError(NSError(domain: "Invalid LOGIN Error", code: code!, userInfo: nil))

                    }

                } else {
                    self.output.loginDidFailWithError(error ?? genericError)
                }
            } catch {
                self.output.loginDidFailWithError(genericError)
            }


        }


  }
}
