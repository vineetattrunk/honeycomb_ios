//
//  LoginLoginInteractorInput.swift
//  VIPER Sample
//
//  Created by Samar Dubey on 12/06/2016.
//  Copyright © 2016 ComingWaves. All rights reserved.
//

import Foundation

protocol LoginInteractorInput {

    func performLoginAPICall(_ username:String, pass:String)
}
