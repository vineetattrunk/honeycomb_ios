//
//  AuthService.swift
//  MVVM Sample
//
//  Created by Samar on 10/06/2016.
//  Copyright © 2016 Samar. All rights reserved.
//

import Foundation

protocol AuthService {
    
    func login(_ user:User, CompletionHandler:@escaping (_ data:Data?, _ error:NSError?) -> (Void))
}
