//
//  AuthService.swift
//  MVVM Sample
//
//  Created by Samar on 10/06/2016.
//  Copyright © 2016 Samar. All rights reserved.
//

import Foundation

class RemoteAuthService : NSObject, AuthService {
    
    let host:String = "http://34.202.232.126/honeycomb/tokenized_auth/user/"

    fileprivate func requestWithBody(_ path: String, method: String, body: Data) -> NSMutableURLRequest {
        let request : NSMutableURLRequest = NSMutableURLRequest()
        request.url = URL(string: host + path)
        print(request.url!)
        request.httpMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        if method == "POST" {
                request.httpBody = body
            
        }

        return request
    }

    
    func login(_ user:User, CompletionHandler:@escaping (_ data:Data?, _ error:NSError?) -> (Void)){
        
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: User.encode(object: user), options: .prettyPrinted)
            let request = self.requestWithBody("login?_format=json", method: "POST", body: jsonData)
            print(request)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
                (
                data, response, error) in
                CompletionHandler(data,nil)
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    print("error")
                    return
                }

//                if let data = data {
//                    do {
//                        let json = try JSONSerialization.jsonObject(with: data, options: [])
//                        print(json)
//                        CompletionHandler(data,nil)
//
//                    } catch {
//                        print(error)
//                    }
//                }
                
                
            })
            // Start the task on a background thread
            task.resume()
           } catch {
                    CompletionHandler(nil, NSError(domain: "invalid Json", code: 1, userInfo: ["user": user]))
                   }
    }
   
}
