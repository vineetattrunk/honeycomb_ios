//
//  User.swift
//  MVVM Sample
//
//  Created by Samar on 10/06/2016.
//  Copyright © 2016 Samar. All rights reserved.
//

import Foundation

enum UserError: Error {
    case InvalidDecodeData
}

final class User: NSObject  {
    
    var username: String
    var password: String
//    var csrfToken:String
//    var userAddress:String
//    var userDescription:String
//    var userDevice_token:String
//    var userFullname:String
//    var userPhone:String
//    var userMail:String
//    var userRoles :String
//    var userUid :String
//    var userPicture:String

    
    override init() {
        username = ""
        password = ""
//        csrfToken = ""
//        userAddress = ""
//        userDescription = ""
//        userDevice_token = ""
//        userFullname = ""
//        userPhone = ""
//        userMail = ""
//        userRoles = ""
//        userUid = "0"
//        userPicture = ""
    }
    
    init(username: String, password: String) {
        self.username = username
        self.password = password
    }
//    init(username: String, password: String,csrfToken:String,userAddress:String,userDescription:String,userDevice_token:String,userFullname:String,userPhone:String,userMail:String,userRoles :String,userUid :String,userPicture:String) {
//        self.username = username
//        self.password = password
//        self.csrfToken = csrfToken
//        self.userAddress = userAddress
//        self.userDescription = userDescription
//        self.userDevice_token = userDevice_token
//        self.userFullname = userFullname
//        self.userPhone = userPhone
//        self.userMail = userMail
//        self.userRoles = userRoles
//        self.userUid = userUid
//        self.userPicture = userPicture
//    }
//
    
    
  
    
    func isValid() -> Bool {
        return username.characters.count > 0
    }

    

    class func decode(json: [String:AnyObject]) -> User {
        return User(
            username: json["name"]?.description ?? "",
            password: json["pass"]?.description ?? ""
//            csrfToken: json["csrf_token"]?.description ?? "",
//            userAddress: json["field_address"]?.description ?? "",
//            userDescription: json["field_description"]?.description ?? "",
//            userDevice_token: json["field_device_token"]?.description ?? "",
//            userFullname: json["field_name"]?.description ?? "",
//            userPhone: json["field_phone"]?.description ?? "",
//            userMail: json["mail"]?.description ?? "",
//            userRoles: json["roles"]?.description ?? "",
//            userUid : json["uid"]?.description ?? "",
//            userPicture: json["uid"]?.description ?? ""
        )
    }
    
    class func encode(object: User) -> AnyObject {
        
        var user = [String: AnyObject]()
        
        user["name"] = object.username as AnyObject
        user["pass"] = object.password as AnyObject
//        user["mail"] = object.userMail as AnyObject
//        user["field_address"] = object.userAddress as AnyObject
//        user["field_device_token"] = object.userDevice_token as AnyObject
//        user["field_phone"] = object.userPhone as AnyObject
//        user["field_name"] = object.userFullname as AnyObject
//
        print(user)

        return user as AnyObject
    }
    
}


