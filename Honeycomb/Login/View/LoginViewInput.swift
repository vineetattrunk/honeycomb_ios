//
//  LoginLoginViewInput.swift
//  VIPER Sample
//
//  Created by Samar Dubey on 12/06/2016.
//  Copyright © 2016 ComingWaves. All rights reserved.
//

protocol LoginViewInput: class {

    /**
        @author Samar Dubey
        Setup initial state of the view
    */

    func setupInitialState()
    func showErrorMessage(_ message:String)
    
}
