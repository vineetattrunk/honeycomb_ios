//
//  LoginLoginViewController.swift
//  VIPER Sample
//
//  Created by Samar Dubey on 12/06/2016.
//  Copyright © 2016 ComingWaves. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, LoginViewInput, UITextFieldDelegate {

    var output: LoginViewOutput!

    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var errorMessageLabel: UILabel!
    @IBOutlet var loginButton: UIButton!
    
    @IBOutlet weak var singupButton: UIButton!
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usernameTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: .editingChanged)
        usernameTextField.becomeFirstResponder()
        
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Username",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: AppConstants.COLOR_1]);
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: AppConstants.COLOR_1]);
        
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self
        
        
        output.viewIsReady()
        
    }
    
    @objc func textFieldDidChange(_ textField:UITextField) {
        loginButton.isEnabled = self.output.isValidLoginForm(usernameTextField.text, pass: passwordTextField.text)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // errorMessageLabel.alpha = 0

    }

    @IBAction func forgetPWButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let forgotpwVC = storyboard.instantiateViewController(withIdentifier: "ForgotPwVC") as! ForgotPwVC
        self.navigationController?.pushViewController(forgotpwVC, animated: true)
    }
    @IBAction func handleLogin(_ sender: AnyObject)
    {
        self.callLogin();
    }

    // MARK: LoginViewInput
    func setupInitialState() {
        
    }

    
    // Dismiss keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    @IBAction func handleSignup(_ sender: Any) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func showErrorMessage(_ message: String)
    {
        DispatchQueue.main.async(execute: { () -> Void in
            //self.errorMessageLabel.alpha = 0
            //self.errorMessageLabel.text = message
            
            UIView.animate(withDuration: 0.3, animations: {
               // self.errorMessageLabel.alpha = 1
            }) 
            })
    }
    
    //MARK: Private Methods (Vineet)
    
    private func callLogin()
    {
        let loginDO = LoginDO();
        loginDO.prepareRequestWithParameters(parameters: usernameTextField.text as AnyObject?,
                                             passwordTextField.text as AnyObject?)
        loginDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                    
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    let errorTextToDisplay = baseDO.serverErrorBO!.errorText!;
                    
                    let alertController = UIAlertController(title: "Something's Wrong!", message: errorTextToDisplay, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "Ok!", style: .default, handler: nil)
                    alertController.addAction(defaultAction);
                    
                    self.present(alertController, animated: true, completion: nil);
                }
            }
            else
            {
                // Response received successfully.
                
                if let loginBO = (loginDO.response as! LoginResponse).loginBO
                {
                    
                    if(loginBO.returnMsg == nil)
                    {
                        print("\n\n\n\n CSRF_TOKEN - \(loginBO.csrf_token!)");
                        
                        for item in loginBO.roles!
                        {
                            print("\ntarget_id - \(item.target_id!)");
                            
                            UserRegistrationHelper.getInstance().setUserRegistrationInfo(name: loginBO.name!,
                                                                                         emailID: loginBO.mail!,
                                                                                         mobileNumber: loginBO.field_phone!,
                                                                                         location: loginBO.field_address!,
                                                                                         csrfToken: loginBO.csrf_token!,
                                                                                         uid: loginBO.uid!);
                            if(item.target_id ==  "buyer")
                            {
                                self.callChangeUserRole()
                            }
                        }
                        
                        //                    self.output.performLogin(self.usernameTextField.text ?? "", pass: self.passwordTextField.text ?? "");         // Old implementation
                        
                        
                        if (AppIntroHelper.getInstance().isPostLoginAppIntroShownInfo() != nil && AppIntroHelper.getInstance().isPostLoginAppIntroShownInfo()!)
                        {
                            App_UI_Helper.appDelegate().window?.rootViewController = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController()
                        }
                        else
                        {
                            let appIntroVC = AppIntroViewController(appIntro_Type: .PostLogin)
                            let navController=UINavigationController(rootViewController: appIntroVC);
                            App_UI_Helper.appDelegate().window?.rootViewController = navController;
                            
                            AppIntroHelper.getInstance().setPostLoginAppIntroShownInfo(bool: true);
                        }                        
                    }
                    else
                    {
                        let alertController = UIAlertController(title: "Something's Wrong!", message: loginBO.returnMsg!, preferredStyle: .alert)
                        
                        let defaultAction = UIAlertAction(title: "Ok!", style: .default, handler: nil)
                        alertController.addAction(defaultAction);
                        
                        self.present(alertController, animated: true, completion: nil);
                    }
                }
            }
        }
    }
    
    private func callChangeUserRole()
    {
        let changeUserRoleDO = ChangeUserRoleDO();
        changeUserRoleDO.prepareRequestWithParameters(parameters: nil)
        changeUserRoleDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                    
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    let errorTextToDisplay = baseDO.serverErrorBO!.errorText!;
                    
                    let alertController = UIAlertController(title: "Something's Wrong!", message: errorTextToDisplay, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "Ok!", style: .default, handler: nil)
                    alertController.addAction(defaultAction);
                    
                    self.present(alertController, animated: true, completion: nil);
                }
            }
            else
            {
                // Response received successfully.
                
                if let loginBO = (changeUserRoleDO.response as! LoginResponse).loginBO
                {
                    if(UserRegistrationHelper.getInstance().isUserRegistrationInfo_Stored())
                    {
                        UserRegistrationHelper.getInstance().updateCSRFToken(newCSRFToken: loginBO.csrf_token!);
                    }
                    else
                    {
                        UserRegistrationHelper.getInstance().setUserRegistrationInfo(name: loginBO.name!,
                                                                                     emailID: loginBO.mail!,
                                                                                     mobileNumber: loginBO.field_phone!,
                                                                                     location: loginBO.field_address!,
                                                                                     csrfToken: loginBO.csrf_token!,
                                                                                     uid: loginBO.uid!);
                    }
                }
            }
        }
    }
       
}
