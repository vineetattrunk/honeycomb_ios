//
//  LoginLoginViewOutput.swift
//  VIPER Sample
//
//  Created by Samar Dubey on 12/06/2016.
//  Copyright © 2016 ComingWaves. All rights reserved.
//

protocol LoginViewOutput {
    
    func viewIsReady()
    func performLogin(_ username:String, pass:String)
    func isValidLoginForm(_ username:String?, pass:String?) -> Bool
}
