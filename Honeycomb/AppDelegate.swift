//
//  AppDelegate.swift
//  Honeycomb
//
//  Created by samarjeet on 13/09/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import MapKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    var navigationController:UINavigationController?
    
    // Adding test comment
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        // Test...
        
        CurrentLocationHelper.getInstance().startUpdatingLocation();
        
        print("\n\nCSRFToken - \(UserRegistrationHelper.getInstance().getCSRFToken())\n\n")
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        if (AppIntroHelper.getInstance().isPreLoginAppIntroShownInfo() != nil && AppIntroHelper.getInstance().isPreLoginAppIntroShownInfo()!)
        {
            if(UserRegistrationHelper.getInstance().isUserRegistrationInfo_Stored())
            {
                window?.rootViewController = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController()
            }
            else
            {
                window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
            }
            //window?.rootViewController = HomeController()
            //window?.rootViewController = UINavigationController(rootViewController: HomeController(collectionViewLayout: layout))
        }
        else
        {
            let appIntroVC = AppIntroViewController(appIntro_Type: .PreLogin)
            self.navigationController=UINavigationController(rootViewController: appIntroVC);
            self.window?.rootViewController = navigationController;
            
            AppIntroHelper.getInstance().setPreLoginAppIntroShownInfo(bool: true);
            
           // window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        }
        
        window?.backgroundColor = UIColor.white
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Honeycomb")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext ()
    {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    
    //MARK: - Public Methods
    
    func logout()
    {
        UserRegistrationHelper.getInstance().clearUserRegistrationInfo();
        
        /**
             Uncomment below two lines if you have to show the preLoginAppIntro screens and PostLoginAppIntro screens once user logs out;
        */
        
//        AppIntroHelper.getInstance().clearPreLoginAppIntroShownInfo();
//        AppIntroHelper.getInstance().clearPostLoginAppIntroInfo();
        
        self.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
    }    
}

extension UIColor
{
    class var electricViolet: UIColor
    {
        return UIColor(red: 144.0 / 255.0, green: 18.0 / 255.0, blue: 254.0 / 255.0, alpha: 1.0)
    }
    
    class var amethystSmoke: UIColor
    {
        return UIColor(red: 169.0 / 255.0, green: 159.0 / 255.0, blue: 177.0 / 255.0, alpha: 1.0)
    }
}


