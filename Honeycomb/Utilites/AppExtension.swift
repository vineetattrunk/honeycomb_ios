//  *R
//  AppExtension.swift
//  corporatebanking
//
//  Created by Aurionpro2 on 25/02/15.
//  Copyright (c) 2015 Aurionpro. All rights reserved.
//

import UIKit

var SCREEN_WIDTH  = UIScreen.main.bounds.width
var SCREEN_HEIGHT  = UIScreen.main.bounds.height
var SCREEN_MAX_LENGTH = max(SCREEN_WIDTH, SCREEN_HEIGHT)

var IS_IPHONE_4_OR_LESS = (SCREEN_MAX_LENGTH < 568.0) ? true : false
var IS_IPHONE_5 = (SCREEN_MAX_LENGTH == 568.0) ? true : false
var IS_IPHONE_6 =  (SCREEN_MAX_LENGTH == 667.0) ? true : false
var IS_IPHONE_6P =  (SCREEN_MAX_LENGTH == 736.0) ? true : false

///*************************** Extension for viewController *********************************

// MARK: Adjust UILabel font size according to device screen size Extention

extension UILabel {
    /// This method set Font size according to phone model
    ///
    /// - AppHelper.swift

    @IBInspectable var adjustFontSize: Bool{
        set {
            if newValue {
                let currentFont = self.font
                var sizeScale: CGFloat = 1
                
                if IS_IPHONE_4_OR_LESS {
                    sizeScale = 0.8 //not Tested yet
                }
                else if IS_IPHONE_5 {
                    sizeScale = 1.0
                }
                else if IS_IPHONE_6 {
                    sizeScale = 1.25
                }
                else if IS_IPHONE_6P {
                    sizeScale = 1.35
                }
                
                self.font = currentFont?.withSize((currentFont?.pointSize)! * sizeScale)
            }        }
        
        get {
            return false
        }
    }
    
    @IBInspectable var adjustTokenFontSize: Bool{
        set {
            if newValue {
                let currentFont = self.font
                var sizeScale: CGFloat = 1
                
                if IS_IPHONE_4_OR_LESS {
                    sizeScale = 1.0 //not Tested yet
                }
                else if IS_IPHONE_5 {
                    sizeScale = 1.0
                }
                else if IS_IPHONE_6 {
                    sizeScale = 1.0
                }
                else if IS_IPHONE_6P {
                    sizeScale = 1.35
                }
                
                self.font = currentFont?.withSize((currentFont?.pointSize)! * sizeScale)
            }        }
        
        get {
            return false
        }
    }
    
    
}
//MARK: Adjust UITextView Font size according to device screen size Extension
extension UITextView {
    /// This method set Font size according to phone model
    ///
    /// - AppHelper.swift
    
    @IBInspectable var adjustFontSize: Bool{
        set {
            if newValue {
                var currentFont = self.font
                var sizeScale: CGFloat = 1
                
                if IS_IPHONE_4_OR_LESS {
                    sizeScale = 0.8 //not Tested yet
                }
                else if IS_IPHONE_5 {
                    sizeScale = 0.8
                }
                else if IS_IPHONE_6 {
                    sizeScale = 1.0
                }
                else if IS_IPHONE_6P {
                    sizeScale = 1.2
                }
                
                self.font = currentFont!.withSize(currentFont!.pointSize * sizeScale)
            }        }
        
        get {
            return false
        }
    }
}



// MARK: Adjust UITextField font size according to device screen size Extention

extension UITextField {

    /// This method set Font size according to phone model
    ///
    /// - AppHelper.swift
    
    @IBInspectable var adjustFontSize: Bool{
        set {
            if newValue {
                var currentFont = self.font
                var sizeScale: CGFloat = 1
                
                if IS_IPHONE_4_OR_LESS {
                    sizeScale = 0.8 //not Tested yet
                }
                else if IS_IPHONE_5 {
                    sizeScale = 0.8
                }
                else if IS_IPHONE_6 {
                    sizeScale = 1.0
                }
                else if IS_IPHONE_6P {
                    sizeScale = 1.2
                }

                self.font = currentFont!.withSize(currentFont!.pointSize * sizeScale)
            }
        }
    
        get {
            return false
        }
    }
}

extension String  {
    
//    
//    struct MD5Digester {
//        // return MD5 digest of string provided
//        static func digest(string: String) -> String? {
//            
//            guard let data = string.data(using: String.Encoding.utf8) else { return nil }
//            
//            var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
//            
//            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
//            
//            return (0..<Int(CC_MD5_DIGEST_LENGTH)).reduce("") { $0 + String(format: "%02x", digest[$1]) }
//        }
//    }
//    
//    var md5: String! {
//        let str = self.cString(usingEncoding: NSUTF8StringEncoding)
//        let strLen = CC_LONG(self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
//        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
//        let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLen)
//        
//        CC_MD5(str!, strLen, result)
//        
//        let hash = NSMutableString()
//        for i in 0..<digestLen {
//            hash.appendFormat("%02x", result[i])
//        }
//        
//        result.dealloc(digestLen)
//        
//        return String(format: hash as String)
//    }
//    
//    
//    func MD5() -> String {
//        return (self as NSString).dataUsingEncoding(NSUTF8StringEncoding.rawValue)!.MD5().hexedString()
//    }
//    
//    func SHA1() -> String {
//        return (self as NSString).dataUsingEncoding(NSUTF8StringEncoding.rawValue)!.SHA1().hexedString()
//    }
}


extension NSData {
//    func hexedString() -> String {
//        var string = String()
//        for i in UnsafeBufferPointer<UInt8>(start: UnsafeMutablePointer<UInt8>(bytes), count: length) {
//            string += Int(i).hexedString()
//        }
//        return string
//    }
//
//    func MD5() -> NSData {
//        let result = NSMutableData(length: Int(CC_MD5_DIGEST_LENGTH))!
//        CC_MD5(bytes, CC_LONG(length), UnsafeMutablePointer<UInt8>(result.mutableBytes))
//        return NSData(data: result)
//    }
//
//    func SHA1() -> NSData {
//        let result = NSMutableData(length: Int(CC_SHA1_DIGEST_LENGTH))!
//        CC_SHA1(bytes, CC_LONG(length), UnsafeMutablePointer<UInt8>(result.mutableBytes))
//        return NSData(data: result)
//    }
}

extension Int {
    func hexedString() -> String {
        return NSString(format:"%02x", self) as String
    }
}
