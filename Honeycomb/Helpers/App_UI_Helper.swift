//
//  App_UI_Helper.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 05/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

/**
     This class is a helper for various things.
 */

class App_UI_Helper
{
    static var myInstance:App_UI_Helper!;
    static var pushNotificationsToken:String="";
    
    class func getInstance()->App_UI_Helper
    {
        if(myInstance == nil)
        {
            myInstance=App_UI_Helper();
        }
        
        return myInstance;
    }
    
    class func appDelegate()->AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate;
    }
    
    class func isValidEmailAddress(emailAddress:String)->Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@",emailRegEx);
        return emailTest.evaluate(with: emailAddress);
    }
    
    class func takeSnapshot(view: UIView) -> UIImage
    {
        var image : UIImage
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext();
        
        return image
    }
    
    class func callNumber(phoneNumber:String)
    {
        let trimmedPhoneNumberString = phoneNumber.replacingOccurrences(of: " ", with: "")
        
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(trimmedPhoneNumberString)")
        {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL as URL))
            {
                application.open(phoneCallURL as URL, options: [:], completionHandler: nil)
            }
        }
    }
    
    class func openGoogleMapsWithDirectionBetween(sourcePoint:CLLocationCoordinate2D, destinationPoint:CLLocationCoordinate2D)
    {
        if let googleMapsURL:NSURL = NSURL(string:"comgooglemaps://")!
        {
            let application:UIApplication = UIApplication.shared
            
            let urlString:NSURL = NSURL(string: "comgooglemaps://?saddr=\(sourcePoint.latitude),\(sourcePoint.longitude)&daddr=\(destinationPoint.latitude),\(destinationPoint.longitude)&directionsmode=driving")!;
            
            if (application.canOpenURL(googleMapsURL as URL))
            {
                application.open(urlString as URL, options: [:], completionHandler: nil)
            }
            else
            {
                let url = NSURL(string:"http://maps.google.com/maps?saddr=\(sourcePoint.latitude),\(sourcePoint.longitude)&daddr=\(destinationPoint.latitude),\(destinationPoint.longitude)")!
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil);
            }
        }
    }
    
    class func openTwitterAppOrWebsite(screenName:String)
    {
        if let twitterAppTestURL:NSURL = NSURL(string:"twitter://")
        {
            let application:UIApplication = UIApplication.shared
            
            let urlString:NSURL = NSURL(string: "twitter://user?screen_name=\(screenName)")!;
            
            if (application.canOpenURL(twitterAppTestURL as URL))
            {
                application.open(urlString as URL, options: [:], completionHandler: nil)    //openURL(urlString as URL);
            }
            else
            {
                let url = NSURL(string:"https://twitter.com/\(screenName)?lang=en")!
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil);
            }
        }
    }
    
    class func operatingSystemName()->String
    {
        return UIDevice.current.systemName;
    }
    
    class func operatingSystemVersion()->String
    {
        return UIDevice.current.systemVersion;
    }
    
    class func appVersion()->String
    {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
    }
    
    class func appBundleId()->String
    {
        return Bundle.main.bundleIdentifier!;
    }
    
    class func appName()->String
    {
        return Bundle.main.infoDictionary!["CFBundleName"] as! String;
    }
    
    class func convertString_To_UpperCase(string:String)->String
    {
        return string.uppercased();
    }
    
    class func bearingInDegress(oldLatitude:Double,oldLongitude:Double,newLatitude:Double,newLongitude:Double)->Double
    {
        let dLon:Double=(newLongitude-oldLongitude);
        let y:Double=sin(dLon) * cos(newLatitude);
        let x=cos(oldLatitude) * sin(newLatitude) - sin(oldLatitude) * cos(newLatitude) * cos(dLon)
        var bearing:Double=atan2(y,x);
        
        bearing=bearing * 180 / M_PI;
        
        return bearing;
    }
    
    class func startShareTheApp(fromViewController:UIViewController)
    {
        let uiActivityViewController=UIActivityViewController(activityItems:[AppConstants.SHARE_APP_DETAILS_STRING],applicationActivities:nil);
        fromViewController.present(uiActivityViewController,animated:true,completion:nil);
    }
    
    class func startShareInfo(fromViewController:UIViewController, info:String)
    {
        let uiActivityViewController=UIActivityViewController(activityItems:[info], applicationActivities:nil);
        fromViewController.present(uiActivityViewController,animated:true,completion:nil);
    }
    
    class func openURLInExternalBrowser(url:String)
    {
        UIApplication.shared.open(NSURL(string:url)! as URL, options: [:], completionHandler: nil)
    }
    
    class func readStringDataFromFile(fileName:String)->String
    {
        let path = Bundle.main.path(forResource: fileName, ofType: "txt")
        
        let contents:NSString
        
        do
        {
            contents = try NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue)
        }
        catch _
        {
            contents = ""
        }
        
        return contents as String
    }
    
    class func openAppWithURL(URLString:String)
    {
        UIApplication.shared.open(NSURL(string: URLString)! as URL, options: [:], completionHandler: nil);
    }
    
    class func getDistanceBetween(location1:CLLocationCoordinate2D,location2:CLLocationCoordinate2D) -> Double
    {
        let theta = location1.longitude - location2.longitude;
        
        var dist = sin(deg2rad(deg: location1.latitude)) * sin(deg2rad(deg: location2.latitude)) + cos(deg2rad(deg: location1.latitude)) * cos(deg2rad(deg: location2.latitude)) * cos(deg2rad(deg: theta));
        
        dist = acos(dist);
        dist = rad2deg(rad: dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1609.344;
        
        return dist;
    }
    
    /**
        This method will do a hardReset i.e. remove ALL the keys from UserDefaults
     */
    
    class func clearUserDefaults()
    {
        //** This will clear all the data from UserDefaults
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }
    
    // MARK: GeoDataSource Methods
    
    ///  This function converts decimal degrees to radians
    class func deg2rad(deg:Double) -> Double
    {
        return deg * Double.pi / 180            // Converted M_PI to Double.pi
    }
    
    ///  This function converts radians to decimal degrees
    class func rad2deg(rad:Double) -> Double
    {
        return rad * 180.0 / Double.pi          // Converted M_PI to Double.pi
    }
}
