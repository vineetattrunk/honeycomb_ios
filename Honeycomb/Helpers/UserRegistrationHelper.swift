//
//  UserRegistrationHelper.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 05/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class UserRegistrationHelper: NSObject
{
    static var myInstance:UserRegistrationHelper!;
    private var userRegistrationInfo:[String:String] = [:];
    
    class func getInstance()->UserRegistrationHelper
    {
        if(myInstance == nil)
        {
            myInstance=UserRegistrationHelper();
        }
        
        return myInstance;
    }
    
    //MARK: Public Methods
    
    /**
         This method returns whether UserRegistrationInfo is stored in UserDefaults or not.
     */
    
    func isUserRegistrationInfo_Stored()-> Bool
    {
        if(UserDefaults.standard.object(forKey: "UserRegistrationInfo") != nil)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     This method sets UserRegistrationInfo in UserDefaults (Key->UserRegistrationInfo)
     */
    
    func setUserRegistrationInfo(name:String, emailID:String, mobileNumber:String, location:String, csrfToken:String, uid:String)
    {
        if(!isUserRegistrationInfo_Stored())
        {
            print("\nStoring UserRegistrationInfo Dictionary in UserDefaults...");
            
            self.userRegistrationInfo = ["name":name,
                                         "emailID":emailID,
                                         "mobileNumber":mobileNumber,
                                         "location":location,
                                         "csrfToken":csrfToken,
                                         "uid":uid];
            
            print("UserRegistrationInfo - \(self.userRegistrationInfo)");
            
            UserDefaults.standard.set(self.userRegistrationInfo, forKey: "UserRegistrationInfo");
            UserDefaults.standard.synchronize();
        }
    }
    
    /**
         This method gets getCSRFToken from UserDefaults
     */
    
    func getCSRFToken() -> String?
    {
        if(isUserRegistrationInfo_Stored())
        {
            var userRegistrationInfo = UserDefaults.standard.object(forKey: "UserRegistrationInfo") as! [String:String];
            let accessToken  = (userRegistrationInfo["csrfToken"]!);
            
            return accessToken;
        }
        else
        {
            return nil;
        }
    }
    
    /**
     This method gets uid from UserDefaults
     */
    
    func getUid() -> String?
    {
        if(isUserRegistrationInfo_Stored())
        {
            var userRegistrationInfo = UserDefaults.standard.object(forKey: "UserRegistrationInfo") as! [String:String];
            let uid  = (userRegistrationInfo["uid"]!);
            
            return uid;
        }
        else
        {
            return nil;
        }
    }
    
    /**
         This method gets username from UserDefaults
     */
    
    func getUserName() -> String?
    {
        if(isUserRegistrationInfo_Stored())
        {
            var userRegistrationInfo = UserDefaults.standard.object(forKey: "UserRegistrationInfo") as! [String:String];
            let username  = (userRegistrationInfo["name"]!);
            
            return username;
        }
        else
        {
            return nil;
        }
    }
    
    /**
        This method gets emailID from UserDefaults
     */
    
    func getEmailID() -> String?
    {
        if(isUserRegistrationInfo_Stored())
        {
            var userRegistrationInfo = UserDefaults.standard.object(forKey: "UserRegistrationInfo") as! [String:String];
            let emailID  = (userRegistrationInfo["emailID"]!);
            
            return emailID;
        }
        else
        {
            return nil;
        }
    }
    
    /**
         This method gets mobileNumber from UserDefaults
     */
    
    func getMobileNumber() -> String?
    {
        if(isUserRegistrationInfo_Stored())
        {
            var userRegistrationInfo = UserDefaults.standard.object(forKey: "UserRegistrationInfo") as! [String:String];
            let mobileNumber  = (userRegistrationInfo["mobileNumber"]!);
            
            return mobileNumber;
        }
        else
        {
            return nil;
        }
    }
    
    
    /**
        This method gets user's location that is recorded at the time of Registration & stored locally in UserDefaults
     */
    
    func getLocation() -> String?
    {
        if(isUserRegistrationInfo_Stored())
        {
            var userRegistrationInfo = UserDefaults.standard.object(forKey: "UserRegistrationInfo") as! [String:String];
            let location  = (userRegistrationInfo["location"]!);
            
            return location;
        }
        else
        {
            return nil;
        }
    }
    
    
    /**
         This method updates CSRFToken when the user changes role from buyer to seller UserDefaults (Key->csrfToken)
     */
    
    func updateCSRFToken(newCSRFToken:String!)
    {
        if(isUserRegistrationInfo_Stored())
        {
            let oldCSRFToken = getCSRFToken();
            
            self.userRegistrationInfo = UserDefaults.standard.object(forKey: "UserRegistrationInfo") as! [String:String];
            
            self.userRegistrationInfo.updateValue(newCSRFToken, forKey: "csrfToken");
            
            UserDefaults.standard.set(self.userRegistrationInfo, forKey: "UserRegistrationInfo");
            UserDefaults.standard.synchronize();
            
            print("\n\n Updated old CSRFToken from - \(oldCSRFToken) to new Token - \(getCSRFToken())");
        }
    }
    
    
    /**
         This method clears UserRegistrationInfo from UserDefaults (Key->UserRegistrationInfo)
     */
    
    func clearUserRegistrationInfo()
    {
        if(UserDefaults.standard.object(forKey: "UserRegistrationInfo") != nil)
        {
            UserDefaults.standard.removeObject(forKey: "UserRegistrationInfo")
            UserDefaults.standard.synchronize()
        }
    }
}
