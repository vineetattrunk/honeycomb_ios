//
//  CurrentLocationHelper.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 12/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import CoreLocation

class CurrentLocationHelper:NSObject, CLLocationManagerDelegate
{
    var latestLocation:CLLocation!;
    var locationManager:CLLocationManager!;
    var current_CityName:String?;
    var current_CountryName:String?;
    
    static var myInstance:CurrentLocationHelper!;
    
    class func getInstance()->CurrentLocationHelper
    {
        if(myInstance == nil)
        {
            myInstance=CurrentLocationHelper();
        }
        
        return myInstance;
    }
    
    //MARK: CLLocationManagerDelegate Methods
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location:CLLocation = locations.last!
        self.latestLocation = location
    }
    
    //MARK: Public Methods
    
    func startUpdatingLocation()
    {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if #available(iOS 8.0, *)
        {
            locationManager.requestWhenInUseAuthorization()
        }
        else
        {
            // Fallback on earlier versions
        }
        
        locationManager.startUpdatingLocation()
    }
    
    func getCurrentLocationName_From_LatLong(lat:CLLocationDegrees, long:CLLocationDegrees)
    {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat, longitude: long)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            
            guard let addressDict = placemarks?[0].addressDictionary else {
                
                return
            }
            
            // Print each key-value pair in a new row
            addressDict.forEach { print($0) }
            
            // Print fully formatted address
            if let formattedAddress = addressDict["FormattedAddressLines"] as? [String] {
                print(formattedAddress.joined(separator: ", "))
            }
            
            // Access each element manually
            if let locationName = addressDict["Name"] as? String {
                print(locationName)                
            }
            
            if let street = addressDict["Thoroughfare"] as? String {
                print(street)
            }
            
            if let city = addressDict["City"] as? String {
                print(city)
                self.current_CityName = city;
            }
            
            if let zip = addressDict["ZIP"] as? String {
                print(zip)
            }
            
            if let country = addressDict["Country"] as? String {
                print(country)
                self.current_CountryName = country;
            }
        })
    }
    
}
