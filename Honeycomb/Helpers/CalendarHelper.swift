//
//  CalendarHelper.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 05/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

/*
import Foundation

class CalendarHelper
{
    class func dateToMilliSeconds(date:NSDate) -> Int64
    {
        let nowDouble = date.timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
    
    
    /**
        This method returns date in string format of local timeZone.
    */
    
    class func localStringFromDate(date:NSDate?)->String?
    {
        if(date == nil)
        {
            return nil;
        }
        
        let dateFormatter=DateFormatter()
        dateFormatter.calendar=Calendar(identifier:.gregorian)
        dateFormatter.timeZone=TimeZone.ReferenceType.local;
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let dateString=dateFormatter.string(from: date! as Date)
        
        return dateString
    }
    
    /**
        This method returns date in string that is formatted as per web service's requirement.
     */
    
    class func dateStringToBeSentOverTheNetwork(date:NSDate?)->String?
    {
        if(date == nil)
        {
            return nil;
        }
        
        let dateFormatter=DateFormatter()
        //dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:ssZZZZZ";
        let dateString=dateFormatter.string(from: date! as Date)
        
        return dateString
    }
    
    class func utcStringToDate(dateString:String?)->NSDate?
    {
        if(dateString == nil)
        {
            return nil;
        }
        
        var dateFormatter=DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        dateFormatter.timeZone=TimeZone.ReferenceType.default
        let date:NSDate?=dateFormatter.date(from: dateString!)! as NSDate;
        
        if(date == nil)
        {
            dateFormatter=DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
            dateFormatter.timeZone=TimeZone.ReferenceType.default
            var date:NSDate?=dateFormatter.date(from: dateString!)! as NSDate;
        }
        
        return date;
    }
    
    class func weirdStringToDate(dateString:String?)->NSDate?
    {
        if(dateString == nil)
        {
            return nil;
        }
        
        let dateFormatter=DateFormatter()
        dateFormatter.dateFormat = "HH:mm EEE dd MMM yy"
        dateFormatter.timeZone=TimeZone.ReferenceType.default
        let date:NSDate?=dateFormatter.date(from: dateString!)! as NSDate;
        
        return date;
    }
    
    class func timeSpanString(date1:NSDate,date2:NSDate)->String
    {
        var timeSpanString="";
        
        let totalMillis=CalendarHelper.dateToMilliSeconds(date: date1) - CalendarHelper.dateToMilliSeconds(date: date2);
        let hours:Int64 = (totalMillis/1000) % 60;
        let minutes:Int64 = ((totalMillis/1000) / 60) % 60;
        let seconds:Int64 = (totalMillis/1000) / 3600;
        
        timeSpanString = String(hours)+":"+String(minutes)+":"+String(seconds);
        
        return timeSpanString;
    }
    
    class func dateByAddingDaysToDate(daysToBeAdded:Int,date:NSDate)->NSDate
    {
        let dateComponents=NSCalendar.currentCalendar.components([NSCalendar.Unit.Year, NSCalendar.Unit.Month, NSCalendar.Unit.Day, NSCalendar.Unit.Hour,NSCalendar.Unit.Minute, NSCalendar.Unit.Weekday], fromDate: date);
        dateComponents.day=dateComponents.day + daysToBeAdded;
        return NSCalendar.currentCalendar().dateFromComponents(dateComponents)!;
    }
    
    class func areTheTwoDatesEqual_InTermsOf_Day_Month_Year(date1:NSDate,date2:NSDate)->Bool
    {
        let dateComponents1=NSCalendar.currentCalendar().components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day,NSCalendarUnit.Hour,NSCalendarUnit.Minute,NSCalendarUnit.Weekday], fromDate: date1);
        let dateComponents2=NSCalendar.currentCalendar().components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day,NSCalendarUnit.Hour,NSCalendarUnit.Minute,NSCalendarUnit.Weekday], fromDate: date2);
        
        return dateComponents1.day==dateComponents2.day&&dateComponents1.month==dateComponents2.month&&dateComponents1.year==dateComponents2.year;
    }
    
    class func dateBySettingItsTimeTo_000000(date:NSDate)->NSDate?
    {
        let dateComponents1=NSCalendar.currentCalendar().components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day,NSCalendarUnit.Hour,NSCalendarUnit.Minute,NSCalendarUnit.Weekday], fromDate: date);
        let dateComponents2=NSDateComponents();
        dateComponents2.day=dateComponents1.day;
        dateComponents2.month=dateComponents1.month;
        dateComponents2.year=dateComponents1.year;
        dateComponents2.hour=0;
        dateComponents2.minute=0;
        dateComponents2.second=0;
        
        return NSCalendar.currentCalendar().dateFromComponents(dateComponents2);
    }
    
    class func dateBySettingItsTimeTo_235959(date:NSDate)->NSDate?
    {
        let dateComponents1=NSCalendar.currentCalendar().components([NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day,NSCalendarUnit.Hour,NSCalendarUnit.Minute,NSCalendarUnit.Weekday], fromDate: date);
        let dateComponents2=NSDateComponents();
        dateComponents2.day=dateComponents1.day;
        dateComponents2.month=dateComponents1.month;
        dateComponents2.year=dateComponents1.year;
        dateComponents2.hour=23;
        dateComponents2.minute=59;
        dateComponents2.second=59;
        
        return NSCalendar.currentCalendar().dateFromComponents(dateComponents2);
    }
    
    class func minutesBy_Minusing_TwoDates(date1:NSDate,date2:NSDate)->Int64
    {
        let totalMillis=CalendarHelper.dateToMilliSeconds(date1) - CalendarHelper.dateToMilliSeconds(date2);
        let minutes:Int64 = ((totalMillis/1000) / 60) % 60;
        
        return minutes;
    }
    
    class func doesThisDateFallInBetween(dateToBeCompared:NSDate,date1:NSDate,date2:NSDate)->Bool
    {
        let result1:NSComparisonResult=dateToBeCompared.compare(date1);
        let result2:NSComparisonResult=dateToBeCompared.compare(date2);
        return ((result1 == NSComparisonResult.OrderedSame || result1 == NSComparisonResult.OrderedDescending) && (result2 == NSComparisonResult.OrderedSame || result2==NSComparisonResult.OrderedAscending));
    }
    
    class func isThisDateLessThanOrEqualToOtherDate(date1:NSDate,date2:NSDate)->Bool
    {
        let result:NSComparisonResult=date1.compare(date2);
        return (result==NSComparisonResult.OrderedSame||result==NSComparisonResult.OrderedAscending);
    }
    
    class func isThisDateGreaterThanOrEqualToOtherDate(date1:NSDate,date2:NSDate)->Bool
    {
        let result:NSComparisonResult=date1.compare(date2);
        return (result==NSComparisonResult.OrderedSame||result==NSComparisonResult.OrderedDescending);
    }
}
 
 */
