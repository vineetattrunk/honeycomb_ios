//
//  AppIntroHelper.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 01/11/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class AppIntroHelper: NSObject
{
    static var myInstance:AppIntroHelper!;
    
    class func getInstance()->AppIntroHelper
    {
        if(myInstance == nil)
        {
            myInstance=AppIntroHelper();
        }
        return myInstance;
    }
    
    //MARK: Public Methods
    //MARK: -- PreLogin Methods
    
    /**
         This method sets preLoginAppIntroShownInfo in UserDefaults (Key->preLoginAppIntroShownInfo)
     */
    
    func setPreLoginAppIntroShownInfo(bool:Bool)
    {
        UserDefaults.standard.set(bool, forKey: "preLoginAppIntroShownInfo");
        UserDefaults.standard.synchronize();
    }
    
    /**
         This method gets isPreLoginAppIntroShownInfo from UserDefaults
     */
    
    func isPreLoginAppIntroShownInfo() -> Bool?
    {
        let isPreLoginAppIntroShownInfo = UserDefaults.standard.object(forKey: "preLoginAppIntroShownInfo") as? Bool;
        return isPreLoginAppIntroShownInfo;
    }
    
    /**
         This method updates preLoginAppIntroShownInfo in UserDefaults (Key->preLoginAppIntroShownInfo)
     */
    
    func updatePreLoginAppIntroShownInfo(bool:Bool)
    {
            UserDefaults.standard.set(bool, forKey: "preLoginAppIntroShownInfo");
            UserDefaults.standard.synchronize();
            
            print("\n\n preLoginAppIntroShownInfo - \(UserDefaults.standard.set(bool, forKey: "preLoginAppIntroShownInfo"))");
    }
    
    /**
         This method clears preLoginAppIntroShownInfo from UserDefaults (Key->preLoginAppIntroShownInfo)
     */
    
    func clearPreLoginAppIntroShownInfo()
    {
        if(UserDefaults.standard.object(forKey: "preLoginAppIntroShownInfo") != nil)
        {
            UserDefaults.standard.removeObject(forKey: "preLoginAppIntroShownInfo")
            UserDefaults.standard.synchronize()
        }
    }
    
    //MARK: -- PostLogin Methods
    
    /**
         This method sets postLoginAppIntroShownInfo in UserDefaults (Key->postLoginAppIntroShownInfo)
     */
    
    func setPostLoginAppIntroShownInfo(bool:Bool)
    {
        UserDefaults.standard.set(bool, forKey: "postLoginAppIntroShownInfo");
        UserDefaults.standard.synchronize();
    }
    
    /**
     This method gets ispostLoginAppIntroShownInfo from UserDefaults
     */
    
    func isPostLoginAppIntroShownInfo() -> Bool?
    {
        let isPostLoginAppIntroShownInfo = UserDefaults.standard.object(forKey: "postLoginAppIntroShownInfo") as? Bool;
        return isPostLoginAppIntroShownInfo;
    }
    
    /**
     This method updates postLoginAppIntroShownInfo in UserDefaults (Key->postLoginAppIntroShownInfo)
     */
    
    func updatepostLoginAppIntroShownInfo(bool:Bool)
    {
        UserDefaults.standard.set(bool, forKey: "postLoginAppIntroShownInfo");
        UserDefaults.standard.synchronize();
        
        print("\n\n postLoginAppIntroShownInfo - \(UserDefaults.standard.set(bool, forKey: "postLoginAppIntroShownInfo"))");
    }
    
    /**
     This method clears postLoginAppIntroShownInfo from UserDefaults (Key->postLoginAppIntroShownInfo)
     */
    
    func clearPostLoginAppIntroInfo()
    {
        if(UserDefaults.standard.object(forKey: "postLoginAppIntroShownInfo") != nil)
        {
            UserDefaults.standard.removeObject(forKey: "postLoginAppIntroShownInfo")
            UserDefaults.standard.synchronize()
        }
    }
}
