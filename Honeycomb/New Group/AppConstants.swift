//
//  AppConstants.swift
//  KCPassengerApp
//
//  Created by Vineet S.  on 25/01/17.
//  Copyright © 2017 AEIOU Networks LLP. All rights reserved.
//

import Foundation
import UIKit

class AppConstants:NSObject
{
    static let GOOGLE_API_KEY:String = "AIzaSyA0Fw1OoVTdXUXekMj_h97QPwy9k_1kaZw";
    
    static let APP_VERSION:String=(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String);
    static let DEVICE_MODEL = UIDevice.current.model;
    static let DEVICE_SYSTEM_VERSION = UIDevice.current.systemVersion
    
    static let SHARE_APP_DETAILS_STRING:String="Hi, I am using 'Kulo-Cabs', You must try it too...\n\n";
    
    static let STATUS_BAR_HEIGHT = UIApplication.shared.statusBarFrame.size.height
    
    static let INVALID_INDEX:Int = -1;
    
    //MARK: Padding Constants
    static let PADDING_1  = CGFloat(1.0)
    static let PADDING_2  = CGFloat(2.0)
    static let PADDING_3  = CGFloat(3.0)
    static let PADDING_4  = CGFloat(4.0)
    static let PADDING_5  = CGFloat(5.0)
    static let PADDING_6  = CGFloat(6.0)
    static let PADDING_7  = CGFloat(7.0)
    static let PADDING_8  = CGFloat(8.0)
    static let PADDING_9  = CGFloat(9.0)
    static let PADDING_10 = CGFloat(10.0)
    static let PADDING_15 = CGFloat(15.0)
    static let PADDING_20 = CGFloat(20.0)
    static let PADDING_25 = CGFloat(25.0)
    static let PADDING_30 = CGFloat(30.0)
    static let PADDING_35 = CGFloat(35.0)
    static let PADDING_40 = CGFloat(40.0)
    static let PADDING_45 = CGFloat(45.0)
    static let PADDING_50 = CGFloat(50.0)
    static let PADDING_55 = CGFloat(55.0)
    static let PADDING_60 = CGFloat(60.0)
    static let PADDING_75 = CGFloat(75.0)
    static let PADDING_80 = CGFloat(80.0)
    static let PADDING_90 = CGFloat(90.0)
    static let PADDING_100 = CGFloat(100.0)
    static let PADDING_110 = CGFloat(110.0)
    static let PADDING_120 = CGFloat(120.0)
    static let PADDING_150 = CGFloat(150.0)
    static let PADDING_200 = CGFloat(200.0)
    static let PADDING_220 = CGFloat(220.0)
    static let PADDING_1000 = CGFloat(1000.0)
    
    //MARK: Colors
    static let COLOR_1:UIColor=UIColor(red:0.0/255.0,green:0.0/255.0,blue:0.0/255.0,alpha:0.8);        // Translucent Color
    static let COLOR_2:UIColor=UIColor(red:193.0/255.0,green:25.0/255.0,blue:33.0/255.0,alpha:1.0);      // Splash_BottomLine_Red
    static let COLOR_3:UIColor=UIColor(red:13.0/255.0,green:57.0/255.0,blue:128.0/255.0,alpha:1.0);     // Splash_BottomLine_Blue
    static let COLOR_4:UIColor=UIColor(red:95.0/255.0,green:95.0/255.0,blue:95.0/255.0,alpha:1.0);     // T&C_Buttons_GrayColor
    static let COLOR_5:UIColor=UIColor(red:226.0/255.0,green:230.0/255.0,blue:236.0/255.0,alpha:1.0);
    static let COLOR_6:UIColor=UIColor(red:0.0/255.0,green:90.0/255.0,blue:11/255.0,alpha:1.0);         // Green color
    static let COLOR_7:UIColor=UIColor(red:64.0/255.0,green:22.0/255.0,blue:42.0/255.0,alpha:1.0);         //Pink shade
    static let COLOR_8:UIColor=UIColor(red:21.0/255.0,green:21.0/255.0,blue:21.0/255.0,alpha:1.0);      // DarkGreyColor for stationsSchedule cells
    static let COLOR_9:UIColor=UIColor(red:32.0/255.0,green:32.0/255.0,blue:32.0/255.0,alpha:1.0);      // LightGreyColor for stationsSchedule
    static let COLOR_10:UIColor=UIColor(red:56.0/255.0,green:56.0/255.0,blue:56.0/255.0,alpha:1.0);      // LightGreyColor for PF dot color
    static let COLOR_11:UIColor=UIColor(red:160.0/255.0,green:160.0/255.0,blue:160.0/255.0,alpha:1.0);   // LightGreyColor for TicketFareHeaderView
    static let COLOR_12:UIColor=UIColor(red:37.0/255.0,green:50.0/255.0,blue:55.0/255.0,alpha:1.0);     // GreenishGrey for TicketFareView rows
    static let COLOR_13:UIColor=UIColor(red:204.0/255.0,green:204.0/255.0,blue:204.0/255.0,alpha:1.0);   // TextColor for AboutTableView
    static let COLOR_14:UIColor=UIColor(red:64.0/255.0,green:64.0/255.0,blue:64.0/255.0,alpha:1.0);     // LightGreyColor for TrainLinesVC Headers
    static let COLOR_15:UIColor=UIColor(red:112.0/255.0,green:112.0/255.0,blue:112.0/255.0,alpha:1.0);     // LightGreyColor for TrainLinesVC HeaderLabels Bgcolor
    static let COLOR_16:UIColor=UIColor(red:54.0/255.0,green:11.0/255.0,blue:11.0/255.0,alpha:1.0);     // Dark Red color
    static let COLOR_17:UIColor=UIColor(red:189.0/255.0,green:189.0/255.0,blue:189.0/255.0,alpha:1.0);  // LightGreyColor of Share Button in AlertsVC
    static let COLOR_18:UIColor=UIColor(red:86.0/255.0,green:87.0/255.0,blue:88.0/255.0,alpha:1.0);  // DarkGreyColor of Source/Destination text
    static let COLOR_19:UIColor=UIColor(red:48.0/255.0,green:48.0/255.0,blue:48.0/255.0,alpha:1.0);  // LightGreyColor of PlatformNumberLabel in trainScheduleVC
    static let COLOR_20:UIColor=UIColor(red:0.0/255.0,green:175.0/255.0,blue:0.0/255.0,alpha:1.0);      // Green color used to indicate 'slow' train
    static let COLOR_21:UIColor=UIColor(red:173.0/255.0,green:173.0/255.0,blue:173.0/255.0,alpha:1.0);     // LightGreyColor of StationScheduleVC detailTextLabel
    static let COLOR_22:UIColor=UIColor(red:255.0/255.0,green:0.0/255.0,blue:0.0/255.0,alpha:1.0);      // FastTrainSpeedIndicator_Red
    static let COLOR_23:UIColor=UIColor(red:0.0/255.0,green:254.0/255.0,blue:82.0/255.0,alpha:1.0);      // Green color used to indicate 'slow' train
    static let COLOR_24:UIColor=UIColor(red:245.0/255.0,green:245.0/255.0,blue:245.0/255.0,alpha:1.0);      // TabBarTintColor F5F5F5
    static let COLOR_25:UIColor=UIColor(red:244.0/255.0,green:244.0/255.0,blue:244.0/255.0,alpha:1.0);
    static let COLOR_26:UIColor=UIColor(red:59.0/255.0,green:55.0/255.0,blue:52.0/255.0,alpha:0.8);
    static let COLOR_27:UIColor=UIColor(red:237.0/255.0,green:237.0/255.0,blue:237.0/255.0,alpha:1.0);
    static let COLOR_28:UIColor=UIColor(red:229.0/255.0,green:48.0/255.0,blue:44.0/255.0,alpha:1.0);        //RedBorderedCircleIcon
    static let COLOR_29:UIColor=UIColor(red:218.0/255.0,green:218.0/255.0,blue:218.0/255.0,alpha:1.0);
    static let COLOR_30:UIColor=UIColor(red:192.0/255.0,green:192.0/255.0,blue:192.0/255.0,alpha:1.0);
    static let COLOR_31:UIColor=UIColor(red:127.0/255.0,green:127.0/255.0,blue:127.0/255.0,alpha:1.0);
    static let COLOR_32:UIColor=UIColor(red:13.0/255.0,green:57.0/255.0,blue:112.0/255.0,alpha:1.0);    //BlueColor of TrainExtraInfo in AtoBView
    
    //KULO Colors
    static let COLOR_33:UIColor=UIColor(red:0.0/255.0,green:151.0/255.0,blue:165.0/255.0,alpha:1.0); // PageControl Active Page color
    static let COLOR_34:UIColor=UIColor(red:30.0/255.0,green:30.0/255.0,blue:30.0/255.0,alpha:1.0); // DarkGrayColor of menuDrawer options
    static let COLOR_35:UIColor=UIColor(red:115.0/255.0,green:115.0/255.0,blue:115.0/255.0,alpha:1.0); // LightGrayColor of SearchAddressTableCell SecondLabel
    static let COLOR_36:UIColor=UIColor(red:250.0/255.0,green:250.0/255.0,blue:250.0/255.0,alpha:1.0); // OffWhiteColor of SearchAddressVC Background
    static let COLOR_37:UIColor=UIColor(red:241.0/255.0,green:241.0/255.0,blue:241.0/255.0,alpha:1.0); // LightYellowishColor of SearchAddressTableCell
    
    
    
    //MARK: Fonts
    static let APPLE_SD_GOTHIC_REGULAR = "AppleSDGothicNeo-Regular"    // Vineet - Change the constant Name
    static let APPLE_SD_GOTHIC_BOLD = "AppleSDGothicNeo-Bold"  // Vineet - Change the constant Name
    
    //MARK: System Info
    static let iOSVersion = NSString(string: UIDevice.current.systemVersion).doubleValue
    static let iOS8 = iOSVersion >= 8
    static let iOS7 = iOSVersion >= 7 && iOSVersion < 8
    
    //MARK: Enums
    enum LineViewOrientation
    {
        case Horizontal
        case Vertical
    }
    
    enum ButtonStates
    {
        case Normal
        case Selected
        case Disabled
    }
    
    enum TableViewDataSortingOrder
    {
        case Ascending
        case Descending
    }
    
    enum DateAndTimeManager_TimeFormat
    {
        case hours_minutes  // am/pm format
        case Hours_Minutes  // 24 hrs format
        case Hours_Minutes_Seconds
    }
}
