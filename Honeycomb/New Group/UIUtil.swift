//
//  UIUtil.swift
//  KCPassengerApp
//
//  Created by Vineet S.  on 25/01/17.
//  Copyright © 2017 AEIOU Networks LLP. All rights reserved.
//

import Foundation
import UIKit

class UIUtil
{
    class func standardLabel(text:String,x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat,numberOfLines:Int,fontName:String?,fontSize:Int)->UILabel
    {
        let label:UILabel=UILabel(frame: CGRect(x: x, y: y, width: width, height: height));
        label.backgroundColor=UIColor.clear;
        label.textColor=UIColor.black;
        
        if(fontName == nil)
        {
            label.font=UIFont(name:AppConstants.APPLE_SD_GOTHIC_BOLD,size:CGFloat(fontSize));
        }
        else
        {
            label.font=UIFont(name:fontName!,size:CGFloat(fontSize));
        }
        
        label.text=text;
        
        if(numberOfLines<=0)
        {
            label.numberOfLines=0;
            label.sizeToFit();
        }
        else
        {
            label.numberOfLines=numberOfLines;
        }
        
        return label;
    }
    
    class func standardTextField(text:String?,placeHolderText:String?,x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat, fontName:String?,fontSize:CGFloat,delegate:UITextFieldDelegate?)->UITextField
    {
        
        let textField:UITextField=UITextField(frame:CGRect(x: x,y: y,width: width,height: height));
        textField.text=text;
        textField.font=UIFont(name:(fontName != nil ? fontName! : AppConstants.APPLE_SD_GOTHIC_REGULAR),size:fontSize);
        textField.textColor=UIColor.black;
        textField.autocorrectionType=UITextAutocorrectionType.no;
        textField.placeholder=placeHolderText;
        textField.backgroundColor=UIColor.clear;
        textField.borderStyle=UITextBorderStyle.roundedRect;
        textField.delegate=delegate;
        
        return textField;
    }
    
    class func standardImageView(imageName:String,x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat)->UIImageView
    {
        let image=UIImage(named:imageName)
        let imageWidth=image?.size.width
        let imageHeight=image?.size.height
        
        let imageView=UIImageView()
        
        if((width<=0 && height<=0))
        {
            imageView.frame=CGRect(x: x, y: y, width: imageWidth!, height: imageHeight!)
        }
        else if(width<=0)
        {
            imageView.frame=CGRect(x: x, y: y, width: imageWidth!, height: height)
        }
        else if(height<=0)
        {
            imageView.frame=CGRect(x: x, y: y, width: width, height: imageHeight!)
        }
        else
        {
            imageView.frame=CGRect(x: x, y: y, width: width, height: height)
        }
        
        imageView.image=image
        
        return imageView
    }
    
    class func standardImageView_With_Image(image:UIImage,x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat)->UIImageView
    {
        let imageWidth=image.size.width
        let imageHeight=image.size.height
        
        let imageView=UIImageView()
        
        if((width<=0 && height<=0))
        {
            imageView.frame=CGRect(x: x, y: y, width: imageWidth, height: imageHeight)
        }
        else if(width<=0)
        {
            imageView.frame=CGRect(x: x, y: y, width: imageWidth, height: height)
        }
        else if(height<=0)
        {
            imageView.frame=CGRect(x: x, y: y, width: width, height: imageHeight)
        }
        else
        {
            imageView.frame=CGRect(x: x, y: y, width: width, height: height)
        }
        
        imageView.image=image
        
        return imageView
    }
    
    class func lineView(orientation:AppConstants.LineViewOrientation, color:UIColor?, x:CGFloat,y:CGFloat, length:CGFloat,thickness:CGFloat)->UIView
    {
        let lineView=UIView()
        
        if(orientation==AppConstants.LineViewOrientation.Horizontal)
        {
            lineView.frame=CGRect(x: x, y: y, width: length, height: thickness)
        }
        else if(orientation==AppConstants.LineViewOrientation.Vertical)
        {
            lineView.frame=CGRect(x: x, y: y, width: thickness, height: length)
        }
        
        if(color == nil)
        {
            lineView.backgroundColor=UIColor(red: 64.0/255.0, green: 64.0/255.0, blue: 64.0/255.0, alpha:1.0)
        }
        else
        {
            lineView.backgroundColor=color!
        }
        
        return lineView
    }
    
    class func standardButton(title:String,x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat,bgcolor:UIColor?,fontName:String?, fontSize:Int,titleColor:UIColor?, enableRoundedCorners:Bool?, target:AnyObject?, action:Selector?)->UIButton
    {
        
        let button=UIButton(frame: CGRect(x: x,y: y,width: width,height: height))
        button.setTitle(title, for: UIControlState.normal)
        
        if(enableRoundedCorners == nil || enableRoundedCorners == true)
        {
            button.layer.cornerRadius=CGFloat(4)
        }
        
        if(target != nil && action != nil)
        {
            button.addTarget(target!, action: action!, for: UIControlEvents.touchUpInside)
        }
        
        if(fontName==nil)
        {
            button.titleLabel!.font=UIFont(name:AppConstants.APPLE_SD_GOTHIC_REGULAR, size:CGFloat(fontSize));
        }
        else
        {
            button.titleLabel!.font=UIFont(name:fontName!,size:CGFloat(fontSize));
        }
        
        if(bgcolor==nil)
        {
            button.backgroundColor=UIColor.white
        }
        else
        {
            button.backgroundColor=bgcolor!
        }
        
        if(titleColor == nil)
        {
            button.setTitleColor(UIColor.darkGray, for: UIControlState.normal)
        }
        else
        {
            button.setTitleColor(titleColor, for: UIControlState.normal)
        }
        
        return button
    }
    
    /*
    class func standardUIAlertView(title:String?,text:String?,delegate:UIAlertViewDelegate?,cancelButtonTitle:String,alertViewTag:Int?)->Void
    {
        let alertView:UIAlertView=UIAlertView(title:title,message:text,delegate:delegate,cancelButtonTitle:cancelButtonTitle);
        if(alertViewTag != nil)
        {
            alertView.tag=alertViewTag!;
        }
        alertView.show();
    }
    */
    
    class func standardUIAlertController(title: String, message: String, preferredStyle: UIAlertControllerStyle?)->UIAlertController
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle != nil ? preferredStyle! : .alert)
        
        // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        {
            (result : UIAlertAction) -> Void in
            print("OK")
        }
        
        alertController.addAction(okAction);
        
        return alertController;
    }
    
    class func standardUISwitch(x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat,onTintColor:UIColor?)->UISwitch
    {
        let switchButton:UISwitch=UISwitch(frame: CGRect(x: x, y: y, width: width, height: height))
        
        if(onTintColor == nil)
        {
            switchButton.onTintColor=AppConstants.COLOR_2
        }
        else
        {
            switchButton.onTintColor=onTintColor
        }
        
        return switchButton
    }
}
