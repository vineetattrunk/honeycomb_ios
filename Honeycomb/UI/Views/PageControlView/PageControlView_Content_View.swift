//
//  PageControlView_Content_View.swift
//  KCPassengerApp
//
//  Created by Vineet S.  on 26/01/17.
//  Copyright © 2017 AEIOU Networks LLP. All rights reserved.
//

import Foundation
import UIKit

class PageControlView_Content_View:UIView
{
    private var pageControlView_Content_Model:PageControlView_Content_Model!;
    
    init(frame:CGRect, pageControlView_Content_Model:PageControlView_Content_Model)
    {
        super.init(frame: frame);
        self.pageControlView_Content_Model = pageControlView_Content_Model;
        self.prepareViews();
        
//        self.layer.borderWidth=1.3;
//        self.layer.borderColor=UIColor.green.cgColor;
    }

    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Public Methods
    
    func refresh()
    {
        self.removeSubViews();
        self.prepareViews();
    }
    
    func whatsYourHeight()->CGFloat
    {
        return self.frame.size.height;
    }
    
    //MARK: Private Methods
    
    private func prepareViews()
    {
        // Adding ImageView
        let imageView:UIImageView = UIUtil.standardImageView(imageName: self.pageControlView_Content_Model.imageName!, x: 0, y: 0, width:0, height: 0);
        self.addSubview(imageView);
         imageView.frame.origin.x = (self.frame.size.width - imageView.frame.size.width)/2; // Aligning it to horizontal center
        
        // Adding PageTitle Label
        let pageTitleLabel:UILabel = UIUtil.standardLabel(text: self.pageControlView_Content_Model.pageTitle!, x: 0, y: imageView.frame.origin.y + imageView.frame.size.height + AppConstants.PADDING_5, width: 0, height: 0, numberOfLines: 0, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD, fontSize: 20);
        pageTitleLabel.textAlignment = NSTextAlignment.center
        self.addSubview(pageTitleLabel)
        pageTitleLabel.frame.origin.x = (self.frame.size.width - pageTitleLabel.frame.size.width)/2; // Aligning it to horizontal center

        // Adding PageDescription Label
        let pageDescriptionLabel:UILabel = UIUtil.standardLabel(text: self.pageControlView_Content_Model.pageDescription!,
                                                                x: 0, y: pageTitleLabel.frame.origin.y + pageTitleLabel.frame.size.height + AppConstants.PADDING_5,
                                                                width: 0, height: 0,
                                                                numberOfLines: 0, fontName: AppConstants.APPLE_SD_GOTHIC_REGULAR, fontSize: 18);
        pageDescriptionLabel.textAlignment = NSTextAlignment.center
        self.addSubview(pageDescriptionLabel);
        pageDescriptionLabel.frame.origin.x = (self.frame.size.width - pageDescriptionLabel.frame.size.width)/2; // Aligning it to horizontal center
        
        // Resizing height of "PageControlView_Content_View"
        self.frame.size.height = pageDescriptionLabel.frame.origin.y + pageDescriptionLabel.frame.size.height + AppConstants.PADDING_10;
    }
    
    private func removeSubViews()
    {
        for view in self.subviews
        {
            view.removeFromSuperview();
        }
    }
    
}
