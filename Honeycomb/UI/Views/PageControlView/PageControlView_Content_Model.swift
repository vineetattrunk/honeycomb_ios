//
//  PageControlView_Content_Model.swift
//  KCPassengerApp
//
//  Created by Vineet S.  on 26/01/17.
//  Copyright © 2017 AEIOU Networks LLP. All rights reserved.
//

import Foundation

class PageControlView_Content_Model
{
    var imageName:String?;
    var pageTitle:String?;
    var pageDescription:String?;
    
    init(imageName:String?, pageTitle:String?, pageDescription:String?)
    {
        self.imageName = imageName;
        self.pageTitle = pageTitle;
        self.pageDescription = pageDescription;
    }
}
