//
//  BasePageControlView.swift
//  KCPassengerApp
//
//  Created by Vineet S.  on 26/01/17.
//  Copyright © 2017 AEIOU Networks LLP. All rights reserved.
//

import Foundation
import UIKit

class BasePageControlView:UIView
{
    override init(frame: CGRect)
    {
        super.init(frame:frame);
        self.backgroundColor = UIColor.black
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}
