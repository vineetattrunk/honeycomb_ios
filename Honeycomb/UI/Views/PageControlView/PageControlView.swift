//
//  PageControlView.swift
//  KCPassengerApp
//
//  Created by Vineet S.  on 26/01/17.
//  Copyright © 2017 AEIOU Networks LLP. All rights reserved.
//

import Foundation
import UIKit

protocol PageControlView_DataSource
{
    func pageControlView_numberOfPages(pageControlView:PageControlView)->Int;
    func pageControlView_viewForPageAtIndexPath(pageControlView:PageControlView, viewForPageAtIndex index: Int)->UIView;
}

protocol PageControlView_Delegate
{
    func pageControlView_currentPageIndexDisplayed(pageControlView:PageControlView, currentPageIndex:Int);
}

class PageControlView:BasePageControlView, UIScrollViewDelegate
{
    enum PageControlPosition
    {
        case Top
        case Bottom
    }
    
    var dataSource:PageControlView_DataSource?;
    var delegate:PageControlView_Delegate?;
    var pageControl_Position:PageControlPosition = PageControlPosition.Bottom;
    var toAutoResizeHeight:Bool=false;
    
    private var scrollView:UIScrollView!;
    private var pageControl:UIPageControl!;
    private var numberOfPages:Int=0;
    private var pageIndex_Of_PageControl_While_Decelerating:Int=0;
    private var contentViewsArray:Array<UIView>=[];
    private var currentPage:Int=0;
    
    override init(frame: CGRect)
    {
        super.init(frame: frame);
        self.backgroundColor = UIColor.black
       // setupPageControl()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: ScrollView delegate Methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = self.bounds.width;
        let pageFraction = Float(self.scrollView.contentOffset.x / pageWidth);
        
        self.pageControl.currentPage = Int(roundf(pageFraction));
        currentPage=self.pageControl.currentPage;
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView)
    {
        self.pageIndex_Of_PageControl_While_Decelerating = self.pageControl.currentPage;
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        delegate?.pageControlView_currentPageIndexDisplayed(pageControlView: self, currentPageIndex: self.pageControl.currentPage)
        currentPage=self.pageControl.currentPage;
    }
    
    //MARK: Public Methods
    
    func refresh()
    {
        self.removeSubViews();
        self.prepareViews();
    }
    
    func enableOrDisablePageMovement(toEnable:Bool)
    {
        scrollView.isScrollEnabled=toEnable;
    }
    
    func changeHeight(height:CGFloat)->Void
    {
        self.frame.size.height=height;
        reposition_PageControl_And_ScrollView();
    }
    
    func changePageTo(page:Int)
    {
        let previousScrollEnabled:Bool=scrollView.isScrollEnabled;
        enableOrDisablePageMovement(toEnable: true);
        pageControl.currentPage=page;
        changePage();
        enableOrDisablePageMovement(toEnable: previousScrollEnabled);
    }
    
    func currentPageDisplayed()->Int
    {
        return currentPage;
    }
    
    @objc func changePage()
    {
        if(scrollView.isScrollEnabled)
        {
            currentPage=pageControl.currentPage;
            let x = CGFloat(currentPage) * scrollView.frame.size.width;
            scrollView.setContentOffset(CGPoint(x: x, y: 0), animated: true);
            pageControl.updateCurrentPageDisplay();
        }
    }
    
    func hidePageControl()
    {
        self.pageControl.isHidden = true;
    }
    
    func showPageControl()
    {
        self.pageControl.isHidden = false;
    }
    
    //MARK: Private Methods
    
    private func prepareViews()
    {
        self.numberOfPages = (dataSource?.pageControlView_numberOfPages(pageControlView: self))!;
        
        self.prepareAndAdd_PageControl();
        self.prepareAndAdd_ScrollView();
        
        self.get_Content_Views();
        self.reposition_PageControl_And_ScrollView();
    }
    
    private func get_Content_Views()
    {
        for i in 0..<self.numberOfPages
        {
            contentViewsArray.append((dataSource?.pageControlView_viewForPageAtIndexPath(pageControlView: self, viewForPageAtIndex: i))!);
        }
        
        var maxHeight=self.contentViewsArray[0].frame.size.height; // First views height as maxHeight
        
        // Adding the views in the contentViewsArray on scrollView
        for (index,item) in self.contentViewsArray.enumerated()
        {
            item.frame.origin.x=item.frame.size.width*CGFloat(index);
            self.scrollView.addSubview(item);
            
            if(maxHeight < item.frame.size.height)
            {
                maxHeight = item.frame.size.height
            }
        }
        
        if(toAutoResizeHeight)
        {
            self.frame.size.height = maxHeight + AppConstants.PADDING_30;
        }
    }
    
    private func reposition_PageControl_And_ScrollView()
    {
        // Repositioning the Y of pageControl
        self.pageControl.frame.origin.y = self.frame.size.height-20;
        
        // Resize scrollView & its content size
        scrollView.frame.size.height = self.frame.size.height - self.pageControl.frame.size.height - AppConstants.PADDING_20;
        scrollView.contentSize=CGSize(width: self.frame.size.width*CGFloat(self.contentViewsArray.count),
                                      height: self.scrollView.frame.size.height);
    }
    
    private func prepareAndAdd_ScrollView()
    {
        if(self.scrollView == nil)
        {
            scrollView = UIScrollView(frame: CGRect(x: 0,y: 0,
                                                    width: self.frame.size.width,
                                                    height: self.frame.size.height - self.pageControl.frame.size.height - AppConstants.PADDING_20));
            scrollView.delegate=self;
            scrollView.isPagingEnabled=true;
            scrollView.contentSize=CGSize(width: self.frame.size.width*CGFloat(self.numberOfPages), height: self.scrollView.frame.size.height);
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true);
            scrollView.showsHorizontalScrollIndicator=false;
            scrollView.showsVerticalScrollIndicator=false;
            scrollView.backgroundColor=UIColor.clear;
            
            self.addSubview(scrollView);
        }
    }
    fileprivate func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.white
        appearance.backgroundColor = UIColor.black
        
    }
    
    private func prepareAndAdd_PageControl()
    {
        if(self.pageControl == nil)
        {
            pageControl = UIPageControl(frame: CGRect(x: 0, y: self.frame.size.height, width: self.frame.size.width, height: 10));
       
            let currentPageIndicatorTintColor = AppConstants.COLOR_1;
//            pageControl.backgroundColor = UIColor.red
            
            pageControl.currentPageIndicatorTintColor = currentPageIndicatorTintColor;
            pageControl.numberOfPages=self.numberOfPages;
            pageControl.tintColor = AppConstants.COLOR_1;
            pageControl.pageIndicatorTintColor = UIColor.lightGray;
            pageControl.addTarget(self, action: #selector(PageControlView.changePage), for: UIControlEvents.valueChanged);
            pageControl.defersCurrentPageDisplay=true;
            
            self.addSubview(pageControl)
        }
    }
    
    private func removeSubViews()
    {
        for view in self.subviews
        {
            view.removeFromSuperview();
        }
    }
}
