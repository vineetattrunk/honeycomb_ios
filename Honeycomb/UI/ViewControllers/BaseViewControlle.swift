//
//  BaseViewController.swift
//  POCs
//
//  Created by Vineet on 4/6/17.
//  Copyright © 2017 Vineet. All rights reserved.
//

import Foundation
import UIKit

class BaseViewControlle: UIViewController
{
    var isUISetUpDone:Bool=false;
    var scrollView:UIScrollView?;
    var currentTFBeingEdited:UITextField?;
    var isDisplayedForTips:Bool=false;
    
    private let FGI_CALL_CENTRE_NUMBER:String = "1800 220 233";
    
    override func loadView()
    {
        self.scrollView=UIScrollView(frame:CGRect.zero);
        self.view=scrollView;
    }
    
    override func viewDidLoad()
    {
        self.edgesForExtendedLayout=[];
        //self.view.backgroundColor=UIColor.white;
        self.view.backgroundColor = UIColor.red
        
        // Adding a hamburgerMenu button in the navbarItem.
        
        let notificationsButton = UIButton(type: .custom)
        notificationsButton.setImage(UIImage(named: "ic_bell_red"), for: .normal)
        notificationsButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        notificationsButton.addTarget(self, action: #selector(BaseViewControlle.notificationsBarButtonTapped), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: notificationsButton)
        
        let phoneButton = UIButton(type: .custom)
        phoneButton.setImage(UIImage(named: "ic_phone_red"), for: .normal)
        phoneButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        phoneButton.addTarget(self, action: #selector(BaseViewControlle.callBarButtonTapped), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: phoneButton)
        
        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
    }
    
    func registerFor_This_ViewController_Entering_ForeGround_State()
    {
//        NotificationCenter.default.addObserver(self, selector:#selector(BaseViewController.viewControllerEnteredForeGroundState), name:
//            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    func deregisterFor_This_ViewController_Entering_ForeGround_State()
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil);
    }
    
    func viewControllerEnteredForeGroundState()
    {
    }
    
    /****  NavigationDrawer related code
     
     func showNavigationDrawerBarButtonItem()
     {
     let navigationDrawerBarButtonItem = UIBarButtonItem(image: UIImage(named: "hamBurgerIcon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(BaseViewController.openNavigationDrawer))
     navigationItem.leftBarButtonItem = navigationDrawerBarButtonItem;
     }
     
     func showProfileBarButtonItem()
     {
     let profileBarButtonItem = UIBarButtonItem(image: UIImage(named: "userProfileIcon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(BaseViewController.showProfileViewController))
     navigationItem.rightBarButtonItem = profileBarButtonItem;
     }
     
     func openNavigationDrawer()
     {
     NavigationDrawerManager.getInstance().openNavigationDrawer();
     }
     
     func closeNavigationDrawer()
     {
     NavigationDrawerManager.getInstance().closeNavigationDrawer();
     }
     
     func showProfileViewController()
     {
     NavigationDrawerManager.getInstance().makeThisNavigationControllerAsRoot(UINavigationController(rootViewController:ProfileViewController()));
     }
     */
    
    func viewWidth()->CGFloat
    {
        return self.view.frame.size.width;
    }
    
    func viewHeight()->CGFloat
    {
        return self.view.frame.size.height;
    }
    
    //MARK: Actions
    
    @objc func callBarButtonTapped()
    {
        print("CallBarButtonTapped...")
//        App_UI_Helper.callNumber(phoneNumber: FGI_CALL_CENTRE_NUMBER);
    }
    
    @objc func notificationsBarButtonTapped()
    {
        print("NotificationsBarButtonTapped...")
        
//        self.navigationController!.pushViewController(NotificationsViewController(), animated: true)
    }
    
    deinit
    {
        deregisterFor_This_ViewController_Entering_ForeGround_State();
    }
}

