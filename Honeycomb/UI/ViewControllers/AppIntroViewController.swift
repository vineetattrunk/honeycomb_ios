//
//  AppIntroViewController.swift
//  KCPassengerApp
//
//  Created by Vineet S.  on 27/01/17.
//  Copyright © 2017 AEIOU Networks LLP. All rights reserved.
//

import Foundation
import UIKit

enum AppIntro_Type
{
    case PreLogin
    case PostLogin
}

class AppIntroViewController: BaseViewControlle, PageControlView_DataSource, PageControlView_Delegate
{
    private var pageControlView_And_Login_And_nextButtons_ContainerView:UIView!
    private var pageControlView:PageControlView!
    private var skipButton:UIButton!
    private var nextButton:UIButton!
    private var numberOfPageControlViews:Int = 7;
    private var appIntro_Type:AppIntro_Type = .PreLogin;
    
    init(appIntro_Type:AppIntro_Type)
    {
        super.init(nibName: nil, bundle: nil);
        
        self.appIntro_Type = appIntro_Type;
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black
    }
    
    override func viewWillAppear(_ animated:Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBar.isHidden=true
    }
    
    override func viewWillLayoutSubviews()
    {
        if(!self.isUISetUpDone)
        {
            self.view.backgroundColor = AppConstants.COLOR_2;
            self.navigationController!.navigationBar.barStyle=UIBarStyle.default
            self.navigationController!.navigationBar.barTintColor=UIColor.black;
            
            self.numberOfPageControlViews = self.appIntro_Type == .PreLogin ? 7 : 5;
            
            self.prepareAndAdd_PageControlView_And_Login_And_nextButtons_ContainerView();
            
            if(self.appIntro_Type == .PostLogin)
            {
                self.pageControlView.hidePageControl();
                self.view.backgroundColor = .black
            }
            
            self.isUISetUpDone=true;
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: PageControlDataSource Methods
    
    func pageControlView_numberOfPages(pageControlView: PageControlView) -> Int
    {
        return numberOfPageControlViews;
    }
    
    func pageControlView_viewForPageAtIndexPath(pageControlView: PageControlView, viewForPageAtIndex index: Int) -> UIView
    {
        if(self.appIntro_Type == .PreLogin)
        {
            if(index == 0)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = AppConstants.COLOR_2;
                
                // Adding skipButton
                self.skipButton = UIUtil.standardButton(title: "SKIP", x: containerView.frame.size.width - AppConstants.PADDING_75 - AppConstants.PADDING_10,
                                                        y: AppConstants.PADDING_20,
                                                        width: AppConstants.PADDING_75,
                                                        height: AppConstants.PADDING_45,
                                                        bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                        fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:true,
                                                        target: self, action: #selector(AppIntroViewController.skipButtonTapped));
                
                containerView.addSubview(self.skipButton!);
                
                // Adding imageView
                let imageView = UIUtil.standardImageView(imageName: "ic_logo_onboarding",
                                                         x: 0, y: self.skipButton.frame.origin.y + self.skipButton.frame.size.height + AppConstants.PADDING_20,
                                                         width: 0, height: 0);
                
                imageView.frame.origin.x = (containerView.frame.size.width - imageView.frame.size.width)/2;
                containerView.addSubview(imageView);
                
                
                // Adding PageTitle Label
                let takeTourLabel:UILabel = UIUtil.standardLabel(text: "TAKE A QUICK TOUR", x: 0, y: containerView.frame.size.height,
                                                                  width: 0, height: 0, numberOfLines: 0, fontName: AppConstants.HELVETICA_NEUE_BOLD, fontSize: 16);
                takeTourLabel.textColor = AppConstants.COLOR_1;
                takeTourLabel.textAlignment = NSTextAlignment.center
                containerView.addSubview(takeTourLabel)
                
                takeTourLabel.frame.origin.x = (containerView.frame.size.width - takeTourLabel.frame.size.width)/2; // Aligning it to horizontal center
                takeTourLabel.frame.origin.y = (containerView.frame.size.height - takeTourLabel.frame.size.height) - AppConstants.PADDING_10; // Aligning it to horizontal center
                
                let labelContainerView:UIView = UIView(frame: CGRect(x:AppConstants.PADDING_10,
                                                                                 y: imageView.frame.origin.y + imageView.frame.size.height + AppConstants.PADDING_50,
                                                                                 width: containerView.frame.size.width - 2*AppConstants.PADDING_10,
                                                                                 height: containerView.frame.size.height - AppConstants.PADDING_30));
    //            labelContainerView.backgroundColor = .red;
                containerView.addSubview(labelContainerView)
                
                let welcomeLabel:UILabel = UIUtil.standardLabel(text: "Welcome to Honeycomb", x: 0, y: 0,
                                                                 width: 0, height: 0, numberOfLines: 0, fontName: AppConstants.HELVETICA_NEUE_BOLD, fontSize: 16);
                welcomeLabel.textColor = AppConstants.COLOR_1;
                welcomeLabel.textAlignment = NSTextAlignment.center
                labelContainerView.addSubview(welcomeLabel)
                
                
                let subTitleLabel:UILabel = UIUtil.standardLabel(text: "Get paid to capture local news", x: 0, y: welcomeLabel.frame.origin.y + welcomeLabel.frame.size.height + AppConstants.PADDING_5,
                                                                width: 0, height: 0, numberOfLines: 0, fontName: AppConstants.HELVETICA_NEUE_BOLD, fontSize: 16);
                subTitleLabel.textColor = .white;
                subTitleLabel.textAlignment = NSTextAlignment.center
                labelContainerView.addSubview(subTitleLabel)
                
                
                welcomeLabel.frame.origin.x = (labelContainerView.frame.size.width - welcomeLabel.frame.size.width)/2; // Aligning it to horizontal center
                subTitleLabel.frame.origin.x = (labelContainerView.frame.size.width - subTitleLabel.frame.size.width)/2; // Aligning it to horizontal center
                
                labelContainerView.frame.origin.x = (containerView.frame.size.width - labelContainerView.frame.size.width)/2; // Aligning it to horizontal center
                labelContainerView.frame.size.height = subTitleLabel.frame.origin.y + subTitleLabel.frame.size.height;
    //            labelContainerView.frame.origin.y = (containerView.frame.size.height - labelContainerView.frame.size.height)/2; // Aligning it to horizontal center
                
                return containerView;
            }
            else if(index == 1)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = AppConstants.COLOR_2;
                
                // Adding PageTitle Label
                let pageTitleLabel:UILabel = UIUtil.standardLabel(text: "See list of nearby requests", x: 0, y: containerView.frame.size.height,
                                                                  width: 0, height: 0, numberOfLines: 0, fontName: AppConstants.HELVETICA_NEUE_BOLD, fontSize: 16);
                pageTitleLabel.textColor = AppConstants.COLOR_1;
                pageTitleLabel.textAlignment = NSTextAlignment.center
                containerView.addSubview(pageTitleLabel)
                
                pageTitleLabel.frame.origin.x = (containerView.frame.size.width - pageTitleLabel.frame.size.width)/2; // Aligning it to horizontal center
                pageTitleLabel.frame.origin.y = (containerView.frame.size.height - pageTitleLabel.frame.size.height) - AppConstants.PADDING_10; // Aligning it to horizontal center
                
                // Adding ImageView
                let imageView = UIImageView(frame: CGRect(x: AppConstants.PADDING_30, y: AppConstants.PADDING_30, width: containerView.frame.size.width - 2*AppConstants.PADDING_30, height: containerView.frame.size.height - pageTitleLabel.frame.size.height - AppConstants.PADDING_20 - AppConstants.PADDING_30))
                imageView.image = UIImage(named:"onBoarding_1");
                containerView.addSubview(imageView)
                
                return containerView;
            }
            else if(index == 2)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = AppConstants.COLOR_2;
                
                // Adding PageTitle Label
                let pageTitleLabel:UILabel = UIUtil.standardLabel(text: "See time limit, distance and price", x: 0, y: containerView.frame.size.height,
                                                                  width: 0, height: 0, numberOfLines: 0, fontName: AppConstants.HELVETICA_NEUE_BOLD, fontSize: 16);
                pageTitleLabel.textColor = AppConstants.COLOR_1;
                pageTitleLabel.textAlignment = NSTextAlignment.center
                containerView.addSubview(pageTitleLabel)
                
                pageTitleLabel.frame.origin.x = (containerView.frame.size.width - pageTitleLabel.frame.size.width)/2; // Aligning it to horizontal center
                pageTitleLabel.frame.origin.y = (containerView.frame.size.height - pageTitleLabel.frame.size.height) - AppConstants.PADDING_10; // Aligning it to horizontal center
                
                // Adding ImageView
                let imageView = UIImageView(frame: CGRect(x: AppConstants.PADDING_30, y: AppConstants.PADDING_30, width: containerView.frame.size.width - 2*AppConstants.PADDING_30, height: containerView.frame.size.height - pageTitleLabel.frame.size.height - AppConstants.PADDING_20 - AppConstants.PADDING_30))
                imageView.image = UIImage(named:"onBoarding_2");
                containerView.addSubview(imageView)
                
                return containerView;
            }
            else if(index == 3)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = AppConstants.COLOR_2;
                
                // Adding PageTitle Label
                let pageTitleLabel:UILabel = UIUtil.standardLabel(text: "See request details", x: 0, y: containerView.frame.size.height,
                                                                  width: 0, height: 0, numberOfLines: 0, fontName: AppConstants.HELVETICA_NEUE_BOLD, fontSize: 16);
                pageTitleLabel.textColor = AppConstants.COLOR_1;
                pageTitleLabel.textAlignment = NSTextAlignment.center
                containerView.addSubview(pageTitleLabel)
                
                pageTitleLabel.frame.origin.x = (containerView.frame.size.width - pageTitleLabel.frame.size.width)/2; // Aligning it to horizontal center
                pageTitleLabel.frame.origin.y = (containerView.frame.size.height - pageTitleLabel.frame.size.height) - AppConstants.PADDING_10; // Aligning it to horizontal center
                
                // Adding ImageView
                let imageView = UIImageView(frame: CGRect(x: AppConstants.PADDING_30, y: AppConstants.PADDING_30, width: containerView.frame.size.width - 2*AppConstants.PADDING_30, height: containerView.frame.size.height - pageTitleLabel.frame.size.height - AppConstants.PADDING_20 - AppConstants.PADDING_30))
                imageView.image = UIImage(named:"onBoarding_3");
                containerView.addSubview(imageView)
                
                return containerView;
            }
            else if(index == 4)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = AppConstants.COLOR_2;
                
                // Adding PageTitle Label
                let pageTitleLabel:UILabel = UIUtil.standardLabel(text: "Edit your video using cropping tool", x: 0, y: containerView.frame.size.height,
                                                                  width: 0, height: 0, numberOfLines: 0, fontName: AppConstants.HELVETICA_NEUE_BOLD, fontSize: 16);
                pageTitleLabel.textColor = AppConstants.COLOR_1;
                pageTitleLabel.textAlignment = NSTextAlignment.center
                containerView.addSubview(pageTitleLabel)
                
                pageTitleLabel.frame.origin.x = (containerView.frame.size.width - pageTitleLabel.frame.size.width)/2; // Aligning it to horizontal center
                pageTitleLabel.frame.origin.y = (containerView.frame.size.height - pageTitleLabel.frame.size.height) - AppConstants.PADDING_10; // Aligning it to horizontal center
                
                // Adding ImageView
                let imageView = UIImageView(frame: CGRect(x: AppConstants.PADDING_30, y: AppConstants.PADDING_30, width: containerView.frame.size.width - 2*AppConstants.PADDING_30, height: containerView.frame.size.height - pageTitleLabel.frame.size.height - AppConstants.PADDING_20 - AppConstants.PADDING_30))
                imageView.image = UIImage(named:"onBoarding_4");
                containerView.addSubview(imageView)
                
                return containerView;
                
            }
            else if(index == 5)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = AppConstants.COLOR_2;
                
                // Adding PageTitle Label
                let pageTitleLabel:UILabel = UIUtil.standardLabel(text: "See something great? Upload \nand record it.", x: 0, y: containerView.frame.size.height,
                                                                  width: 0, height: 0, numberOfLines: 0, fontName: AppConstants.HELVETICA_NEUE_BOLD, fontSize: 16);
                pageTitleLabel.textColor = AppConstants.COLOR_1;
                pageTitleLabel.textAlignment = NSTextAlignment.center
                containerView.addSubview(pageTitleLabel)
                
                pageTitleLabel.frame.origin.x = (containerView.frame.size.width - pageTitleLabel.frame.size.width)/2; // Aligning it to horizontal center
                pageTitleLabel.frame.origin.y = (containerView.frame.size.height - pageTitleLabel.frame.size.height) - AppConstants.PADDING_10; // Aligning it to horizontal center
                
                // Adding ImageView
                let imageView = UIImageView(frame: CGRect(x: AppConstants.PADDING_30, y: AppConstants.PADDING_30, width: containerView.frame.size.width - 2*AppConstants.PADDING_30, height: containerView.frame.size.height - pageTitleLabel.frame.size.height - AppConstants.PADDING_20 - AppConstants.PADDING_30))
                imageView.image = UIImage(named:"onBoarding_5");
                containerView.addSubview(imageView)
                
                return containerView;
            }
            else if(index == 6)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = AppConstants.COLOR_2;
                
                
                // Adding imageView
                let imageView = UIUtil.standardImageView(imageName: "ic_logo_onboarding",
                                                         x: 0, y: AppConstants.PADDING_100,
                                                         width: 0, height: 0);
                
                imageView.frame.origin.x = (containerView.frame.size.width - imageView.frame.size.width)/2;
                imageView.frame.origin.y = (containerView.frame.size.height - imageView.frame.size.height)/2;
                containerView.addSubview(imageView);
                
                
                // Adding skipButton
                self.skipButton = UIUtil.standardButton(title: "Continue to Honeycomb >>",
                                                        x: 0,
                                                        y: imageView.frame.origin.y + imageView.frame.size.height + AppConstants.PADDING_20,
                                                        width: containerView.frame.size.width,
                                                        height: AppConstants.PADDING_45,
                                                        bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                        fontSize: 16, titleColor: AppConstants.COLOR_1, enableRoundedCorners:true,
                                                        target: self, action: #selector(AppIntroViewController.skipButtonTapped));
                
                self.skipButton.frame.origin.x = (containerView.frame.size.width - self.skipButton.frame.size.width)/2;
                containerView.addSubview(self.skipButton!);
                
                let subTitleLabel:UILabel = UIUtil.standardLabel(text: "Get paid to capture local news", x: 0, y: skipButton.frame.origin.y + skipButton.frame.size.height + AppConstants.PADDING_5,
                                                                 width: 0, height: 0, numberOfLines: 0, fontName: AppConstants.HELVETICA_NEUE_BOLD, fontSize: 16);
                subTitleLabel.textColor = .white;
                subTitleLabel.textAlignment = NSTextAlignment.center
                containerView.addSubview(subTitleLabel)
                
                subTitleLabel.frame.origin.x = (containerView.frame.size.width - subTitleLabel.frame.size.width)/2; // Aligning it to horizontal center
                
                return containerView;
            }
            else
            {
                return UIView();
            }
        }
        else if(self.appIntro_Type == .PostLogin)
        {
            if(index == 0)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = .black;
                
                // Adding imageView
                let imageView = UIUtil.standardImageView(imageName: "post_ob_1",
                                                         x: 0,
                                                         y: 0,
                                                         width: containerView.frame.size.width,
                                                         height: containerView.frame.size.height);
                imageView.contentMode = .scaleAspectFill;
                containerView.addSubview(imageView);
                
                // Adding skipButton
                let leftButton = UIUtil.standardButton(title: "", x: 0,
                                                        y: 0,
                                                        width: containerView.frame.size.width/2,
                                                        height: containerView.frame.size.height,
                                                        bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                        fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:false,
                                                        target: self, action: #selector(AppIntroViewController.previousButtonTapped));
                
                containerView.addSubview(leftButton);
                
                let rightButton = UIUtil.standardButton(title: "", x: leftButton.frame.origin.x + leftButton.frame.size.width,
                                                       y: 0,
                                                       width: leftButton.frame.size.width,
                                                       height: leftButton.frame.size.height,
                                                       bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                       fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:false,
                                                       target: self, action: #selector(AppIntroViewController.nextButtonTapped));
                
                containerView.addSubview(rightButton);
                
                return containerView;
            }
            else if(index == 1)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = .black;
                
                // Adding imageView
                let imageView = UIUtil.standardImageView(imageName: "post_ob_2",
                                                         x: 0,
                                                         y: 0,
                                                         width: containerView.frame.size.width,
                                                         height: containerView.frame.size.height);
                imageView.contentMode = .scaleAspectFill;                
                containerView.addSubview(imageView);
                
                // Adding skipButton
                let leftButton = UIUtil.standardButton(title: "", x: 0,
                                                       y: 0,
                                                       width: containerView.frame.size.width/2,
                                                       height: containerView.frame.size.height,
                                                       bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                       fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:false,
                                                       target: self, action: #selector(AppIntroViewController.previousButtonTapped));
                
                containerView.addSubview(leftButton);
                
                let rightButton = UIUtil.standardButton(title: "", x: leftButton.frame.origin.x + leftButton.frame.size.width,
                                                        y: 0,
                                                        width: leftButton.frame.size.width,
                                                        height: leftButton.frame.size.height,
                                                        bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                        fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:false,
                                                        target: self, action: #selector(AppIntroViewController.nextButtonTapped));
                
                containerView.addSubview(rightButton);
                
                return containerView;
            }
            else if(index == 2)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = .black;
                
                // Adding imageView
                let imageView = UIUtil.standardImageView(imageName: "post_ob_3",
                                                         x: 0,
                                                         y: 0,
                                                         width: containerView.frame.size.width,
                                                         height: containerView.frame.size.height);
                imageView.contentMode = .scaleAspectFill;
                containerView.addSubview(imageView);
                
                // Adding skipButton
                let leftButton = UIUtil.standardButton(title: "", x: 0,
                                                       y: 0,
                                                       width: containerView.frame.size.width/2,
                                                       height: containerView.frame.size.height,
                                                       bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                       fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:false,
                                                       target: self, action: #selector(AppIntroViewController.previousButtonTapped));
                
                containerView.addSubview(leftButton);
                
                let rightButton = UIUtil.standardButton(title: "", x: leftButton.frame.origin.x + leftButton.frame.size.width,
                                                        y: 0,
                                                        width: leftButton.frame.size.width,
                                                        height: leftButton.frame.size.height,
                                                        bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                        fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:false,
                                                        target: self, action: #selector(AppIntroViewController.nextButtonTapped));
                
                containerView.addSubview(rightButton);
                
                return containerView;
            }
            else if(index == 3)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = .black;
                
                // Adding imageView
                let imageView = UIUtil.standardImageView(imageName: "post_ob_4",
                                                         x: 0,
                                                         y: 0,
                                                         width: containerView.frame.size.width,
                                                         height: containerView.frame.size.height);
                imageView.contentMode = .scaleAspectFill;
                containerView.addSubview(imageView);
                
                // Adding skipButton
                let leftButton = UIUtil.standardButton(title: "", x: 0,
                                                       y: 0,
                                                       width: containerView.frame.size.width/2,
                                                       height: containerView.frame.size.height,
                                                       bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                       fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:false,
                                                       target: self, action: #selector(AppIntroViewController.previousButtonTapped));
                
                containerView.addSubview(leftButton);
                
                let rightButton = UIUtil.standardButton(title: "", x: leftButton.frame.origin.x + leftButton.frame.size.width,
                                                        y: 0,
                                                        width: leftButton.frame.size.width,
                                                        height: leftButton.frame.size.height,
                                                        bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                        fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:false,
                                                        target: self, action: #selector(AppIntroViewController.nextButtonTapped));
                
                containerView.addSubview(rightButton);
                
                return containerView;
            }
            else if(index == 4)
            {
                let containerView:UIScrollView = UIScrollView(frame: CGRect(x:0,y: 0, width: self.view.frame.size.width, height: self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height - AppConstants.PADDING_30));
                containerView.backgroundColor = .black;
                
                // Adding imageView
                let imageView = UIUtil.standardImageView(imageName: "post_ob_5",
                                                         x: 0,
                                                         y: 0,
                                                         width: containerView.frame.size.width,
                                                         height: containerView.frame.size.height);
                imageView.contentMode = .scaleAspectFill;
                containerView.addSubview(imageView);
                
                // Adding skipButton
                let leftButton = UIUtil.standardButton(title: "", x: 0,
                                                       y: 0,
                                                       width: containerView.frame.size.width/2,
                                                       height: containerView.frame.size.height,
                                                       bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                       fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:false,
                                                       target: self, action: #selector(AppIntroViewController.previousButtonTapped));
                
                containerView.addSubview(leftButton);
                
                let rightButton = UIUtil.standardButton(title: "", x: leftButton.frame.origin.x + leftButton.frame.size.width,
                                                        y: 0,
                                                        width: leftButton.frame.size.width,
                                                        height: leftButton.frame.size.height,
                                                        bgcolor: UIColor.clear, fontName: AppConstants.APPLE_SD_GOTHIC_BOLD,
                                                        fontSize: 16, titleColor: UIColor.white, enableRoundedCorners:false,
                                                        target: self, action: #selector(AppIntroViewController.nextButtonTapped));
                
                containerView.addSubview(rightButton);
                
                return containerView;
            }
            else
            {
                return UIView();
            }
        }
        else
        {
            return UIView();
        }
    }
    
    //MARK: PageControlDelegate Methods
    
    func pageControlView_currentPageIndexDisplayed(pageControlView: PageControlView, currentPageIndex: Int)
    {
        //print("\nCurrent PageIndex is : \(currentPageIndex)");
    }
    
    //MARK: Private Methods
    
    private func prepareAndAdd_PageControlView_And_Login_And_nextButtons_ContainerView()
    {
        let availableHeight_For_PageControlView_And_Login_And_nextButtons_ContainerView = AppConstants.iOS11 ? self.view.frame.size.height - AppConstants.PADDING_20 - AppConstants.PADDING_10 : self.view.frame.size.height - AppConstants.PADDING_10;
        
        // Adding "pageControlView_And_Login_And_nextButtons_ContainerView"
        self.pageControlView_And_Login_And_nextButtons_ContainerView = UIView(frame: CGRect(x:0, y:0,
                                                                                            width: self.view.frame.size.width,
                                                                                            height:availableHeight_For_PageControlView_And_Login_And_nextButtons_ContainerView));
        self.pageControlView_And_Login_And_nextButtons_ContainerView.backgroundColor=UIColor.white;
        
        self.view.addSubview(pageControlView_And_Login_And_nextButtons_ContainerView);
        
        // Adding pageControlView
        
        self.pageControlView = PageControlView(frame: CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.pageControlView_And_Login_And_nextButtons_ContainerView.frame.size.height));
        self.pageControlView.backgroundColor = self.appIntro_Type == .PreLogin ? AppConstants.COLOR_2 : .black
        pageControlView.dataSource=self;
        pageControlView.delegate=self;
        pageControlView.refresh();
        
//        self.pageControlView.layer.borderWidth = 2.0;
//        self.pageControlView.layer.borderColor = UIColor.green.cgColor
        
        self.pageControlView_And_Login_And_nextButtons_ContainerView.addSubview(pageControlView);
    }
    
    
    //MARK: Internal Methods
    
    @objc func previousButtonTapped()
    {
        if(self.pageControlView.currentPageDisplayed() > 0)
        {
            self.pageControlView.changePageTo(page: self.pageControlView.currentPageDisplayed() - 1)
        }
    }
    
    @objc func nextButtonTapped()
    {
        if(self.pageControlView.currentPageDisplayed() < numberOfPageControlViews - 1)
        {
            self.pageControlView.changePageTo(page: self.pageControlView.currentPageDisplayed() + 1)
        }
        else
        {
            self.skipButtonTapped();
        }
    }
    
    @objc func skipButtonTapped()
    {
        if(self.appIntro_Type == .PreLogin)
        {
            App_UI_Helper.appDelegate().window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        }
        else if(self.appIntro_Type == .PostLogin)
        {
            App_UI_Helper.appDelegate().window?.rootViewController = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController()
        }
    }
}
