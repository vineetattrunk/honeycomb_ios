//
//  NBRequestDetailVC.swift
//  Honeycomb
//
//  Created by samarjeet on 15/09/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit
import Cloudinary
import MobileCoreServices
import Alamofire
import SwiftyJSON
import SwiftDate

class NBRequestDetailVC: UIViewController {
    @IBOutlet weak var TitleLabel: UILabel!
  
    @IBOutlet weak var Detailtext: UITextView!
    
    
    @IBOutlet weak var directionButton: UIButton!
    
    
    @IBOutlet weak var deadlineLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    
    @IBOutlet weak var locationLable: UILabel!
    
    var nbArray = [AnyObject]()
    var publicid:String?
    var videoUrl:String?
    var convertedString:String?
    
    var passedValue:String = ""
    fileprivate var nbdetailDic = Dictionary<String,AnyObject>()
    fileprivate var nbdetailArray = Array<String>()
    
    //let url = " http://34.202.232.126/honeycomb/node/429?_format=json&time=1499686885216"
    override func viewWillAppear(_ animated: Bool) {
        print("Detail\(passedValue)")
    }
    @IBAction func uploadfromGallery(_ sender: Any)
    {
        galleryVideo()
    }
    
    @IBAction func recordfromCamera(_ sender: Any)
    {
        self.startCameraFromViewController(self, withDelegate: self)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tableView.dataSource = self
        //self.tableView.delegate = self
        self.directionButton.layer.borderWidth = 2
        self.directionButton.layer.borderColor = AppConstants.COLOR_1.cgColor
       
        
                ProgressView.shared.showProgressView(view)
      
        print("Detail\(passedValue)")
        
        Alamofire.request("http://34.202.232.126/honeycomb/node/\(passedValue)?_format=json&time=1499686885216").responseJSON { response in
            debugPrint(response)
            ProgressView.shared.hideProgressView()
            if let json = response.result.value {
                print("JSON: \(json)")
                self.nbdetailDic = json as! Dictionary<String,AnyObject>
                self.TitleLabel.text = (self.nbdetailDic["title"] as? String)
                self.TitleLabel.textColor = AppConstants.COLOR_1;
                
                let createdDate = DateInRegion(string:(self.nbdetailDic["field_expiry_date"] as! String), format: .iso8601(options: .withInternetDateTime), fromRegion: Region.Local(autoUpdate: true));
                print("Date - \(self.nbdetailDic["field_expiry_date"] as! String)")
                let createdDateString = createdDate?.string(custom: "MM.dd.yy");
                let timeString = createdDate?.string(custom: "hh:mm a");
                
                if(createdDateString != nil && timeString != nil)
                {
                    self.deadlineLabel.text = createdDateString! + " at " + timeString!;
                }
                else
                {
                    self.deadlineLabel.text = "-";
                }
                
                self.priceLabel.text = "$\(self.nbdetailDic["field_amount"] as! String)"
                self.locationLable.text = (self.nbdetailDic["field_location_name"] as? String)
                
                self.Detailtext.text = self.nbdetailDic["field_details"] as? String
               
            }
            
        }

        self.automaticallyAdjustsScrollViewInsets = true
        // Set a header for the table view
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func galleryVideo()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            
            let videoPicker = UIImagePickerController()
            videoPicker.delegate = self
            videoPicker.sourceType = .photoLibrary
            videoPicker.mediaTypes = [kUTTypeMovie as String]
            videoPicker.allowsEditing = true
            
            self.present(videoPicker, animated: true, completion: nil)
        }
    }
    
    func startCameraFromViewController(_ viewController: UIViewController, withDelegate delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            return false
        }
        
        let cameraController = UIImagePickerController()
        cameraController.sourceType = .camera
        cameraController.allowsEditing = true
        cameraController.mediaTypes = [kUTTypeMovie as NSString as String]
        cameraController.allowsEditing = true
        cameraController.delegate = delegate
        
        present(cameraController, animated: true, completion: nil)
        return true
    }
    @objc func video(_ videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        var title = "Success"
        var message = "Video was saved"
        if let _ = error {
            title = "Error"
            message = "Video failed to save"
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func confirmationMSG()
    {
        let alert = UIAlertController(title: "Request Confirmation", message: "Are you sure you want to upload video", preferredStyle: .alert)
        let action = UIAlertAction(title: "Confirm", style:.default) { (action) -> Void in
            
            App_UI_Helper.appDelegate().logout();
        }
        let cancelAction = UIAlertAction(title: "NO", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped");
        }
        alert.addAction(cancelAction)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NBRequestDetailVC: UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
            let mediaType = info[UIImagePickerControllerMediaType] as! NSString
            
            dismiss(animated: true, completion: nil)
            // Handle a movie capture
            if mediaType == kUTTypeMovie
            {
                guard let path = (info[UIImagePickerControllerMediaURL] as? URL)?.path else { return }
                
                if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path)
                {
                    print("Path - \(path)");
                    
                    //////--------Cloudinary API---------------------------///////
                    
                    var videoThumbnaiURL :String?
                    var videoData: Data?
                    do
                    {
                        videoData = try NSData(contentsOfFile: (path), options: NSData.ReadingOptions.alwaysMapped) as Data
                        print(videoData!)
                    }
                    catch
                    {
                        
                    }
                    
                    let config = CLDConfiguration(cloudinaryUrl: "cloudinary://619327614426169:cVux-ozY4bvVi_Dksuj1OmMrI0U@trunk")
                    let cloudinary = CLDCloudinary(configuration: config!)
                    
                    let params = CLDUploadRequestParams().setResourceType(.video)
                    cloudinary.createUploader().signedUpload(data: videoData!, params: params as? CLDUploadRequestParams).responseRaw({ (result, error) in
                        
                        if let res = result as? [String:AnyObject]
                        {
                            //completionHandler(CLDUploadResult(json: res), nil)
                            
                            print("Result - \(res)");
                            

                            
                            self.publicid = res["public_id"] as? String
//                            print("Here is pid \(self.publicid)")
                            
                            self.videoUrl = res["secure_url"] as? String
//                            print("Here is videoUrl \(self.videoUrl)")
                            
                            videoThumbnaiURL = self.videoUrl!.replacingOccurrences(of: ".mov", with: ".jpg")
                            print("videoThumbnaiURL! - \(videoThumbnaiURL!)")
                            
                            // Converting the response object received from Cloudinary into JSON object
                            let jsonObj = JSON(res)
                            
                            // Generate the string representation of the JSON value
                            let jsonString = jsonObj.rawString()!
                            let escapedAddress = jsonString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                            
                            //--------********Video Send API-------****************
                            
                            let videoFileName = (path as NSString).lastPathComponent
                            
                            self.callVideoUpload(xcrfToken: UserRegistrationHelper.getInstance().getCSRFToken() != nil ? UserRegistrationHelper.getInstance().getCSRFToken()! : "",
                                                 type: "video",
                                                 title: videoFileName,
                                                 field_video_request_reference: "0",
                                                 field_video_posters: videoThumbnaiURL!,
                                                 field_video_json: escapedAddress)
                        }
                    })
                    
                    let alert = UIAlertController(title: "Request Confirmation", message: "Are you sure you want to upload video!", preferredStyle: .alert)
                    let action = UIAlertAction(title: "Confirm", style:.default) { (action) -> Void in
                        
                        UISaveVideoAtPathToSavedPhotosAlbum(path, self, #selector(NBRequestListView.video(_:didFinishSavingWithError:contextInfo:)), nil)
                    }
                    let cancelAction = UIAlertAction(title: "NO", style: .cancel) { (action:UIAlertAction!) in
                        print("Cancel button tapped");
                    }
                    alert.addAction(cancelAction)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    
                   
                }
            }
        }
    
    //MARK: Private Methods (Vineet)
    
    private func callVideoUpload(xcrfToken:String, type:String, title:String, field_video_request_reference:String, field_video_posters:String, field_video_json:String)
    {
        let videoUploadDO = VideoUploadDO();
        videoUploadDO.prepareRequestWithParameters(parameters: xcrfToken as AnyObject?,
                                             type as AnyObject?,
                                             title as AnyObject?,
                                             field_video_request_reference as AnyObject?,
                                             field_video_posters as AnyObject?,
                                             field_video_json as AnyObject)
        videoUploadDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                    
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    if(baseDO.serverErrorBO!.errorText! == "token mismatch")
                    {
                        let alertController = UIUtil.standardUIAlertController(title: "Alert", message: AppConstants.TEXT_1, preferredStyle: .alert)
                        self.present(alertController, animated: true, completion: nil);
                        
                        App_UI_Helper.appDelegate().logout();
                    }
                }
            }
            else
            {
                // Response received successfully.
                
                if let videoUploadBO = (videoUploadDO.response as! VideoUploadResponse).videoUploadBO
                {
                    if(videoUploadBO.returnMsg != "token mismatch")
                    {
                        
                        //TODO: Ask Pawan what needs to be done here...
                    }
                }
            }
        }
    }
}


extension NBRequestDetailVC: UINavigationControllerDelegate {
}
