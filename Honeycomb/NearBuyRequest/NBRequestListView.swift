//
//  NBRequestListView.swift
//  Honeycomb
//
//  Created by Samarjeet on 22/08/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit
import Cloudinary
import MobileCoreServices
import CoreLocation
import Alamofire
import SwiftDate

class NBRequestListView:BaseViewController
{
    @IBOutlet weak var tableView: UITableView!
    
//    var nbArray = [AnyObject]()
    var publicid:String?
    var videoUrl:String?
    var convertedString:String?
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
 
    var nearbyRequestsBOsArray:Array<NearbyRequestsBO>?;
    var currentItem: String?
    var nid:Int = 0
    let NBRequestIdentifier = "DetailNBRequestSegue"
    var xyz:String?
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
                
        ProgressView.shared.showProgressView(view)
        
        addSlideMenuButton()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView();
        
        if(CurrentLocationHelper.getInstance().latestLocation != nil)
        {
            self.callNearbyRequests();
        }
        else
        {
            self.callAllNearbyRequests();
        }
    }
    
    override func viewWillAppear(_ _animated: Bool)
    {
        super.viewWillAppear(_animated)
        
        self.tableView.reloadData()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if(segue.identifier == "DetailNBRequestSegue")
        {
            let indexpath = self.tableView.indexPathForSelectedRow!
            let destVC = segue.destination as! NBRequestDetailVC
            currentItem =  self.nearbyRequestsBOsArray![indexpath.row].nid! // nbArray[Indexpath!.row]["nid"] as! String    //TODO: Vineet - Find a better way for this.
            destVC.passedValue = currentItem!
        }
    }
    
    func galleryVideo()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            
            let videoPicker = UIImagePickerController()
            videoPicker.delegate = self
            videoPicker.sourceType = .photoLibrary
            videoPicker.mediaTypes = [kUTTypeMovie as String]
            videoPicker.allowsEditing = true
            
            self.present(videoPicker, animated: true, completion: nil)
        }
    }
    
    func startCameraFromViewController(_ viewController: UIViewController, withDelegate delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) -> Bool
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false
        {
            return false
        }
        
        let cameraController = UIImagePickerController()
        cameraController.sourceType = .camera
        cameraController.allowsEditing = true
        cameraController.mediaTypes = [kUTTypeMovie as NSString as String]
        cameraController.allowsEditing = true
        cameraController.delegate = delegate
        
        present(cameraController, animated: true, completion: nil)
        return true
    }
    
    @objc func video(_ videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject)
    {
        var title = "Success"
        var message = "Video was saved"
        if let _ = error {
            title = "Error"
            message = "Video failed to save"
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    func cellStarButtonTapped(cell: NBRequestTableViewCell) {
        let indexPath = self.tableView.indexPath(for: cell)
        print(indexPath!.row)
        print("inside call")
//        let phoneNumber = self.teamPlayer[indexPath!.row]["mobile"] as! String
//        if let url = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
//            if #available(iOS 10, *) {
//                UIApplication.shared.open(url)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
       // }
    }
    
    
    //MARK: IBActions
    
    @IBAction func uploadfromGallery(_ sender: Any)
    {
        galleryVideo()
    }
    
    @IBAction func recordfromCamera(_ sender: Any)
    {
        self.startCameraFromViewController(self, withDelegate: self)
    }
    
    //MARK: Private Methods (Vineet)
    
    private func callNearbyRequests()
    {
        let currentTimestamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))     // Current epoch time
        let currentLocationCoordinates = CurrentLocationHelper.getInstance().latestLocation.coordinate;
        
        let nearbyRequestsDO = NearbyRequestsDO();
        nearbyRequestsDO.prepareRequestWithParameters(parameters: (currentLocationCoordinates.latitude as AnyObject?),
                                                      (currentLocationCoordinates.longitude as AnyObject?),
                                                      (AppConstants.COVERAGE_MILES as AnyObject?),
                                                      currentTimestamp as AnyObject?)
        nearbyRequestsDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            ProgressView.shared.hideProgressView()
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    if(baseDO.serverErrorBO!.errorText! == "token mismatch")
                    {
                        let alertController = UIUtil.standardUIAlertController(title: "Alert", message: AppConstants.TEXT_1, preferredStyle: .alert)
                        self.present(alertController, animated: true, completion: nil);
                        
                        App_UI_Helper.appDelegate().logout();
                    }
                }
            }
            else
            {
                // Response received successfully.
                
                if((nearbyRequestsDO.response as! NearbyRequestsResponse).nearbyRequestsBOsArray != nil && (nearbyRequestsDO.response as! NearbyRequestsResponse).nearbyRequestsBOsArray!.count > 0)
                {
                    self.nearbyRequestsBOsArray = (nearbyRequestsDO.response as! NearbyRequestsResponse).nearbyRequestsBOsArray!
                    
                    self.tableView.reloadData();
                    
                    self.navigationItem.title = "NEARBY REQUESTS (\(self.nearbyRequestsBOsArray!.count))"
                }
            }
        }
    }
    
    private func callAllNearbyRequests()
    {
        let currentTimestamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))     // Current epoch time
        
        let allNearbyRequestsDO = AllNearbyRequestsDO();
        allNearbyRequestsDO.prepareRequestWithParameters(parameters: currentTimestamp as AnyObject?)
        allNearbyRequestsDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            ProgressView.shared.hideProgressView()
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    if(baseDO.serverErrorBO!.errorText! == "token mismatch")
                    {
                        let alertController = UIUtil.standardUIAlertController(title: "Alert", message: AppConstants.TEXT_1, preferredStyle: .alert)
                        self.present(alertController, animated: true, completion: nil);
                        
                        App_UI_Helper.appDelegate().logout();
                    }
                }
            }
            else
            {
                // Response received successfully.
                
                if((allNearbyRequestsDO.response as! NearbyRequestsResponse).nearbyRequestsBOsArray != nil && (allNearbyRequestsDO.response as! NearbyRequestsResponse).nearbyRequestsBOsArray!.count > 0)
                {
                    self.nearbyRequestsBOsArray = (allNearbyRequestsDO.response as! NearbyRequestsResponse).nearbyRequestsBOsArray!
                    self.tableView.reloadData();
                    self.navigationItem.title = "NEARBY REQUESTS (\(self.nearbyRequestsBOsArray!.count))"
                }
            }
        }
    }
}

extension NBRequestListView: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(self.nearbyRequestsBOsArray != nil)
        {
            let expirydateString = self.nearbyRequestsBOsArray![indexPath.row].field_expiry_date!
            let expiryDate = DateInRegion(string: expirydateString, format: .iso8601(options: .withInternetDateTime), fromRegion: Region.Local(autoUpdate: true))
            
            let todaysDate = DateInRegion();
            
            let isRequestExpired = todaysDate.isBefore(date: expiryDate!, orEqual: true, granularity: .hour);
            
            if(isRequestExpired)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NBRequestCell", for: indexPath) as! NBRequestTableViewCell
                
                cell.titlelabel.text = "$\(self.nearbyRequestsBOsArray![indexPath.row].field_amount!)"  + " " +  self.nearbyRequestsBOsArray![indexPath.row].title!
                cell.detailLabel.text = self.nearbyRequestsBOsArray![indexPath.row].field_details!
                cell.priceLabel.text = "$\(self.nearbyRequestsBOsArray![indexPath.row].field_amount!)"
                cell.distanceLabel.text = "50 Miles"
            
                let timeInterval = (expiryDate! - todaysDate)
                let daysLeft = ((expiryDate! - todaysDate)/86400).rounded()     // 60*60*24
                
                if(daysLeft <= 1)
                {
                    cell.dayleftLabel.text =  "\((timeInterval/3600).rounded()) Hours Left"
                }
                else
                {
                    cell.dayleftLabel.text =  "\(daysLeft) Days Left"
                }
                
//                let date = Date()
//                let dateFormatter = DateFormatter()
//                let timeZone = TimeZone.autoupdatingCurrent.identifier as String
//                dateFormatter.timeZone = TimeZone(identifier: timeZone)
//                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//                let currentdate = dateFormatter.string(from: date)
//
//                let start = dateFormatter.date(from: currentdate)!
//                let end = dateFormatter.date(from: expirydate)!
//                print("StartTime\(start)   EndTime\(end)")
//
//                let diff = Date.daysBetween(start: start, end: end)
//
//                cell.dayleftLabel.text =  "\(diff) Days Left"
                
                return cell
            }
            else
            {
                return UITableViewCell();
            }
        }
        else if(self.nearbyRequestsBOsArray == nil || (self.nearbyRequestsBOsArray!.count <= 0))
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NBRequestCell", for: indexPath) as! NBRequestTableViewCell
            return cell
        }
        else
        {
            return UITableViewCell();
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(self.nearbyRequestsBOsArray != nil)
        {
            return self.nearbyRequestsBOsArray!.count
        }
        else if(self.nearbyRequestsBOsArray == nil)
        {
            return 1
        }
        
        return 0;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let indexPath = tableView.indexPathForSelectedRow

        //getting the current cell from the index path
        let currentCell = tableView.cellForRow(at: indexPath!)! as! NBRequestTableViewCell
        
       // getting the text of that cell
        let currentItem = self.nearbyRequestsBOsArray![indexPath!.row].nid!
        print("Myvalue\(currentItem)")
        
         //self.navigationController?.pushViewController(detailViewController, animated: true)

    }
}

// MARK: - UIImagePickerControllerDelegate

extension NBRequestListView: UIImagePickerControllerDelegate
{

}

extension NBRequestListView: UINavigationControllerDelegate
{
    
}

extension String
{
    func dropLast(_ n: Int = 1) -> String
    {
        return String(characters.dropLast(n))
    }
    
    var dropLast: String
    {
        return dropLast()
    }
}

extension Date
{
    func daysBetween(date: Date) -> Int {
        return Date.daysBetween(start: self, end: date)
    }
    
    static func daysBetween(start: Date, end: Date) -> Int {
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
        
        let a = calendar.dateComponents([.day], from: date1, to: date2)
        return a.value(for: .day)!
    }
}

//extension Array {
//    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
//        var dict = [Key:Element]()
//        for element in self {
//            dict[selectKey(element)] = element
//        }
//        return dict
//    }
//}

extension Date
{
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

//extension Date {
//
//    func differenceInDaysWithDate(date: Date) -> Int {
//        let calendar = Calendar.current
//
//        let date1 = calendar.startOfDay(for: self)
//        let date2 = calendar.startOfDay(for: date)
//
//        let components = calendar.dateComponents([.day], from: date1, to: date2)
//        return components.day ?? 0
//    }
//}

//extension Dictionary {
//
//    var json: String {
//        let invalidJson = "Not a valid JSON"
//        do {
//            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
//            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
//        } catch {
//            return invalidJson
//        }
//    }
//
//    func printJson() {
//        print(json)
//    }
//
//}
//
