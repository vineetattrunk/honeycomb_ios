//
//  Endpoints.swift
//  iOS-Viper-Architecture
//
//  Created by Samarjeet on 22/08/17.
//  Copyright © 2017 Trunk. All rights reserved.
//
//

import Foundation

struct API {
      static let baseurl = "http://34.202.232.126/honeycomb/nearby-requests/all?_format=json&time=1503408515503"
      // static let  baseurl = "http://34.202.232.126/honeycomb/nearby-requests/19.0632311,72.9979443%3C=50.0miles?_format=json&time=1503408557454"
//    var originalString = "test/test=42"
//    var customAllowedSet =  CharacterSet(charactersIn: "=\"#%/<>?@\\^`{|}").inverted
//   // var customAllowedSet =  CharacterSet(charactersInString:"=\"#%/<>?@\\^`{|}").invertedSet
//    var escapedString =  urlStr.addingPercentEncoding(withAllowedCharacters: self.customAllowedSet)
//
//    println("escapedString: \(escapedString)")
//    static let baseurl : String = urlStr.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
}


protocol Endpoint {
    var path: String { get }
    var url: String { get }
}

enum Endpoints {
    
    enum Posts: Endpoint {
        case fetch
        
        public var path: String {
            switch self {
            case .fetch: return "/nearby-requests/all?_format=json&time=1503408515503"
            }
        }
        
        public var url: String {
            switch self {
            case .fetch: return "\(API.baseurl)\(path)"
               
            }
        }
    }
}
