//
//  NBRequestTableViewCell.swift
//  Honeycomb
//
//  Created by Samarjeet on 22/08/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

protocol CustomCellDelegate {
    func cellStarButtonTapped(cell: NBRequestTableViewCell)
    
}
class NBRequestTableViewCell: UITableViewCell {
    var delegate: CustomCellDelegate?
    
    @IBOutlet weak var nbRequestImageView: UIImageView!
    
    var nid:Int?
    var nidLabel:UILabel?
    @IBOutlet weak var NumberBgView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var dayleftLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nextArrowImage: UIImageView!
    
    @IBAction func callButton(_ sender: Any) {
        delegate?.cellStarButtonTapped(cell: self)
    }


   
}
