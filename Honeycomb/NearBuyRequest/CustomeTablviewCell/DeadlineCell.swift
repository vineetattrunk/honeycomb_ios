//
//  DeadlineCell.swift
//  Honeycomb
//
//  Created by Samarjeet on 26/08/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

class DeadlineCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var valueLabel: UILabel?
    
//    var item: Deadline?  {
//        didSet {
//            titleLabel?.text = item?.key
//            valueLabel?.text = item?.value
//        }
//    }
    
//    var nib:UINib {
//        return UINib(nibName:DeadlineCell.identifier, bundle: nil)
//    }
//    
//    static var identifier: String {
//        return String(describing: self)
//    }
}
