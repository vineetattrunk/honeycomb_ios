//
//  LocationCell.swift
//  Honeycomb
//
//  Created by Samarjeet on 26/08/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var valueLabel: UILabel?
    
//    var item: Location?  {
//        didSet {
//            titleLabel?.text = item?.key
//            valueLabel?.text = item?.value
//        }
//    }
//    
//    static var nib:UINib {
//        return UINib(nibName: LocationCell.identifier, bundle: nil)
//    }
//    
//    static var identifier: String {
//        return String(describing: self)
//    }
}
