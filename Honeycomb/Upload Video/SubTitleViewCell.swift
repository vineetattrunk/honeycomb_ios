//
//  SubTitleViewCell.swift
//  Collection View in a Table View Cell
//
//  Created by iSparsh on 9/28/17.
//  Copyright © 2017 Ash Furrow. All rights reserved.
//

import UIKit

class SubTitleViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var SubTitleViewLabel: UILabel!

}
