import UIKit

class ViewController: UITableViewController{
    var count: Int = 1
    var items:[Int] = [2]
    var pendingImageString:[String]!

    let model = generateRandomData()
    var storedOffsets = [Int: CGFloat]()
    
    // cancel button action
    func buttonClicked(sender: UIButton?) {
        let tag = sender?.tag
        // remove object from array and reload collectionview
    }
    
   

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return model.count
        return 5
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleViewCellID", for: indexPath) as! TitleViewCell
            cell.textLabel?.text = "Title"
            
            // Set up cell.label
            return cell
        } else if indexPath.row == 1 {
           let cell = tableView.dequeueReusableCell(withIdentifier: "SubTitleViewCellID", for: indexPath) as! SubTitleViewCell
            cell.textLabel?.text = "SubTitle"
            // Set up cell.button
            return cell
        }else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubtitleDescriptonICellID", for: indexPath) as!SubtitleDescripton
             cell.textLabel?.text = "SubTitle"
            // Set up cell.button
            return cell
        }else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubtitleDetailCellID", for: indexPath) as! SubtitleDetail
            // Set up cell.button
            return cell
        }else{
         let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
            // Set up cell.textField
            return cell
        }

       // let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        //return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 4
        {
            return 200.0
        }else{
            return 80.0
            
        }
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        guard let tableViewCell = cell as? TableViewCell else { return }

        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }

    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        guard let tableViewCell = cell as? TableViewCell else { return }

        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return model[collectionView.tag].count
//        return pendingImageString.count + 1
        return items.count + 1
        //return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)

            cell.backgroundColor = model[collectionView.tag][indexPath.item]
            // Set up cell.label
            return cell
        } else if indexPath.item == 1 {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
            // Set up cell.button
            return cell
        }
        else{
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
            // Set up cell.button
            return cell

        }
       // return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == self.items.count{
            for indexPath: IndexPath in collectionView.indexPathsForSelectedItems!{
                count = count + 1
                    print(self.count)
                    self.items.append(count)
                    self.tableView.reloadData()
                
                     print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")

            }


        }
        
      //  if indexPath.item == pendingImageString.count{
//            // update your array with new image file
//        }
//
    }
}
