import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var collectionView: UICollectionView!

}

extension TableViewCell {

    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
     // collectionView.scrollToItem(at: <#T##IndexPath#>, at: UICollectionViewScrollPosition., animated: <#T##Bool#>)
       // self.collectionView.scrollToItem(at: IndexPath(item: items.count-1, section: 0), at: [], animated: false)
        collectionView.reloadData()
       
    }

    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
    
}
