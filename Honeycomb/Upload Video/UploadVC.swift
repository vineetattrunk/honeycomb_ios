//
//  UploadVC.swift
//  Honeycomb
//
//  Created by samarjeet on 15/09/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit


class UploadVC: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var count: Int = 1
    var items:[Int] = [2]
    var pendingImageString:[String]!
    
    let model = generateRandomData()
    var storedOffsets = [Int: CGFloat]()
    
   
    
   // services.append(Item(indexPath, value: clickeditem))

    
    override func viewDidLoad(){
        super.viewDidLoad()
        //imageCollectionView!.dataSource = self
       // imageCollectionView!.delegate = self
       addSlideMenuButton()
        //self.view.addSubview(imageCollectionView!)
        // Do any additional setup after loading the view.
    }
    func deleteItem (indexOfItem : Int){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
     
        
       
       // self.imageCollectionView!.reloadData()
        
    }
    func parkItem(itemId:String) {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
       
        
        
       // item.setValue(itemId, forKeyPath: "itemId")
       // items.append(item)
        
       
        
//        self.imageCollectionView!.reloadData()
//        self.imageCollectionView!.scrollToItem(at: IndexPath(item: items.count-1, section: 0), at: [], animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UploadVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return model.count
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleViewCellID", for: indexPath) as! TitleViewCell
            cell.textLabel?.text = "Title"
            
            // Set up cell.label
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubTitleViewCellID", for: indexPath) as! SubTitleViewCell
            cell.textLabel?.text = "SubTitle"
            // Set up cell.button
            return cell
        }else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubtitleDescriptonICellID", for: indexPath) as!SubtitleDescripton
            cell.textLabel?.text = "SubTitle"
            // Set up cell.button
            return cell
        }else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubtitleDetailCellID", for: indexPath) as! SubtitleDetail
            // Set up cell.button
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
            // Set up cell.textField
            return cell
        }
        
        // let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        //return cell
    }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 4
        {
            return 200.0
        }else{
            return 80.0
            
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? TableViewCell else { return }
        
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
     func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? TableViewCell else { return }
        
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
    
}

extension UploadVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return model[collectionView.tag].count
        //        return pendingImageString.count + 1
        return items.count + 1
        //return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
            
            //cell.backgroundColor = model[collectionView.tag][indexPath.item]
            // Set up cell.label
            return cell
        } else if indexPath.item == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
            // Set up cell.button
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
            // Set up cell.button
            return cell
            
        }
        // return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == self.items.count{
            for indexPath: IndexPath in collectionView.indexPathsForSelectedItems!{
                count = count + 1
                print(self.count)
                self.items.append(count)
                self.tableView.reloadData()
                
                print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
                
            }
            
            
        }
        
        //  if indexPath.item == pendingImageString.count{
        //            // update your array with new image file
        //        }
        //
    }
}
