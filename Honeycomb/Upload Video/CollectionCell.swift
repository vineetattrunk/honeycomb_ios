//  CollectionViewCell
//  Honeycomb
//
//  Created by iSparsh on 9/26/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

protocol TapCellDelegate:NSObjectProtocol{
    func buttonTapped(indexPath:IndexPath)
}

class CollectionCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBOutlet weak var topButton: UIButton!
    weak var delegate:TapCellDelegate?
    @IBOutlet weak var itemIdLabel: UILabel!
    
    public var indexPath:IndexPath!
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        if let delegate = self.delegate{
            delegate.buttonTapped(indexPath: indexPath)
        }
    }
}
