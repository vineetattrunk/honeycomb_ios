//
//  NearbyRequestsResponse.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 12/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class NearbyRequestsResponse: BaseResponse
{
    var nearbyRequestsBOsArray:Array<NearbyRequestsBO>?;
}
