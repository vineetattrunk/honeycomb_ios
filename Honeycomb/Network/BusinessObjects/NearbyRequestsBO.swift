//
//  NearbyRequestsBO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 12/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class NearbyRequestsBO:BaseBO
{
    var title:String?;
    var field_details:String?;
    var nid:String?;
    var field_geolocation:String?;  // lat | long
    var field_expiry_date:String?;
    var field_location_name:String?;
    var field_amount:String?;
    var flagged:String?;
    
    init(title:String?, field_details:String?, nid:String?, field_geolocation:String?, field_expiry_date:String?, field_location_name:String?, field_amount:String?, flagged:String?)
    {
        self.title=title;
        self.field_details=field_details;
        self.nid=nid;
        self.field_geolocation=field_geolocation;
        self.field_expiry_date=field_expiry_date;
        self.field_location_name=field_location_name;
        self.field_amount=field_amount;
        self.flagged=flagged;
    }
    
    override init()
    {
        super.init();
    }
}
