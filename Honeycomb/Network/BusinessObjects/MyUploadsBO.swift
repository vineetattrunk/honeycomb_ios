//
//  MyUploadsBO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 11/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class MyUploadsBO: BaseBO
{
    var created_at:String?
    var field_video_posters:String?
    var title:String?
    var nid:String?     // Its a Buyer's request nid
    var uuid:String?
    var vid:String?
    var isPitch:Bool?
    
    init(created_at:String?, field_video_posters:String?, title:String?, nid:String?, uuid:String?, vid:String?, isPitch:Bool?)
    {
        self.created_at = created_at;
        self.field_video_posters = field_video_posters;
        self.title = title;
        self.nid = nid;
        self.uuid = uuid;
        self.vid = vid;
        self.isPitch = isPitch;
    }
}
