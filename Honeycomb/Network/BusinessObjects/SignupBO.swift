//
//  SignupBO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 16/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class SignupBO: BaseBO
{
    var access:Int?;
    var changed:Int64?;
    var created:Int64?;
    var csrf_token:String?;
    var default_langcode:Bool?;
    var field_address:String?;
    var field_description:String?;
    var field_device_token:String?;
    var field_name:String?;
    var field_phone:String?;
    var init_:String?
    var langcode:String?
    var login:String?
    var mail:String?
    var name:String?
    var pass:Array<Any?>?;                        // [null],
    var preferred_admin_langcode:String?
    var preferred_langcode:String?
    var roles:Array<TargetModel>?        //[{ "target_id": "seller", "target_type": "user_role", "target_uuid": "99a058dc-be95-4468-89bd-a346cdf8d679"}],
    var status:Int?
    var timezone:String?
    var uid:String?
    var user_picture:String?
    var uuid:String?
}
