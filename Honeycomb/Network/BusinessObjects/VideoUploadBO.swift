//
//  VideoUploadBO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 11/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class VideoUploadBO: BaseBO
{
    var url:String?;
    var tags:Array<TagsModel>?;
    var secure_url:String?;
    var created_at:String?;
    var resource_type:String?;
    var field_video_posters:String?;
    var nid:String?;
    var title:String?;
    var uid_target_id:String?;      //uid -> target_id
    var uuid:String?;
    var vid:String?;
}

class TagsModel
{
    var tid:String?;
    var name:String?;
    
    init(tid:String?, name:String?)
    {
        self.tid = tid;
        self.name = name;
    }
}
