//
//  ServerErrorBO.swift
//  FGI-MyDrive
//
//  Created by Vineet on 4/27/17.
//  Copyright © 2017 Idealake. All rights reserved.
//

import Foundation

class ServerErrorBO: BaseBO
{
    var isSessionExpired:Bool=false;
    var errorText:String?=nil;
    
    init(errorText:String?)
    {
        self.errorText=errorText;
    }
    
    class func serverErrorBO_From_OneOfTheValidTexts(possibleTexts:AnyObject?...)->ServerErrorBO
    {
        let serverErrorBO:ServerErrorBO=ServerErrorBO(errorText: "Server Error");
        
        for object in possibleTexts
        {
            if(object != nil && (object as? String) != nil)
            {
                serverErrorBO.errorText=(object as? String);
                break;
            }
        }
        
        return serverErrorBO;
    }
}
