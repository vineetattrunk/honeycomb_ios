//
//  StatusReplyBO.swift
//  FGI-MyDrive
//
//  Created by Vineet on 4/27/17.
//  Copyright © 2017 Idealake. All rights reserved.
//

import Foundation

class StatusReplyBO: BaseBO
{
    var theDescription:String?=nil;
    var errors:String?=nil;
    var heading:String?=nil;
    var statusType:String?=nil;
    var hint:String?=nil;
    
    override func parse(parameter:AnyObject)
    {
        if((parameter as? Dictionary<String,AnyObject>) != nil)
        {
            self.theDescription=parameter["Description"] as? String;
            if let errorsDictionary=parameter["Errors"] as? Dictionary<String,AnyObject>
            {
                var keysArray:[String]=Array(errorsDictionary.keys);
                self.errors=keysArray[0];
            }
            else if let errorsArray=parameter["Errors"] as? Array<AnyObject>
            {
                if(errorsArray.count > 0)
                {
                    for i:Int in 0  ..< errorsArray.count 
                    {
                        let errorDictionary:AnyObject=errorsArray[i];
                        if((errorDictionary as? Dictionary<String,AnyObject>) != nil)
                        {
                            self.errors=self.errors == nil ? (errorDictionary["Error"] as? String) : (self.errors! + "\n" + (errorDictionary["Error"] as? String)!);
                        }
                    }
                }
            }
            self.heading=parameter["Heading"] as? String;
            self.statusType=parameter["StatusType"] as? String;
            self.hint=parameter["Hint"] as? String;
        }
    }
    
    func isSessionExpired()->Bool
    {
        return (hint != nil) && (hint! == "SessionExpired");
    }
}
