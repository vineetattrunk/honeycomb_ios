//
//  InsertRegistrationDetailsBO.swift
//  FGI-MyDrive
//
//  Created by Vineet on 4/27/17.
//  Copyright © 2017 Idealake. All rights reserved.
//

import Foundation

class LoginBO: BaseBO
{
    var access:String?
    var changed:String?
    var created:String?
    var csrf_token:String?
    var default_langcode:String?
    var field_address:String?
    var field_description:String?
    var field_device_token:String?
    var field_name:String?
    var field_phone:String?
    var init_:String?
    var langcode:String?
    var login:String?
    var mail:String?
    var name:String?
    var pass:Array<Any?>?;                        // [null],
    var preferred_admin_langcode:String?
    var preferred_langcode:String?
    var roles:Array<TargetModel>?        //[{ "target_id": "seller", "target_type": "user_role", "target_uuid": "99a058dc-be95-4468-89bd-a346cdf8d679"}],
    var status:String?
    var timezone:String?
    var uid:String?
    var user_picture:String?
    var uuid:String?
}

class TargetModel
{
    var target_id:String?;
    var target_type:String?;
    var target_uuid:String?;
    
    init(target_id:String?, target_type:String?, target_uuid:String?)
    {
        self.target_id = target_id;
        self.target_type = target_type;
        self.target_uuid = target_uuid;
    }
}
