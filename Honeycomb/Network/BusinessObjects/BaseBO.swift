//
//  BaseBO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 05/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class BaseBO
{
    var returnCode:String?;
    var returnMsg:String?;
    var data:Dictionary<String,AnyObject>!;
    
    class func prepareBaseBOWithParameters(parameters:AnyObject?...)->AnyObject?
    {
        return nil;
    }
    
    func paramName()->String?
    {
        return nil;
    }
    
    func parameters()->AnyObject?
    {
        return nil;
    }
    
    func parse(parameter:AnyObject)
    {
    }
}
