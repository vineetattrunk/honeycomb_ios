//
//  SignupRequest.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 16/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class SignupRequest:BaseRequest
{
    var name:String!;
    var mail:String!;
    var field_address:String!;
    var field_device_token:String!;     // Firebase token
    var field_name:String!;
    var field_phone:String!;
    var pass:String!;
    
}
