//
//  MyUploadsRequest.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 11/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class MyUploadsRequest: BaseRequest
{
    var uid:String!;
    var epochTime:Int64!;  //in milliseconds
}
