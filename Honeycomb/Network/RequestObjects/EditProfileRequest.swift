//
//  EditProfileRequest.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 24/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class EditProfileRequest: BaseRequest
{
    var field_address:Array<EditProfileRequest_FieldAddressModel> = [];
    var field_phone:Array<EditProfileRequest_FieldPhoneModel> = [];
    var mail:Array<EditProfileRequest_MailModel> = [];
    var field_description:Array<EditProfileRequest_FieldDescriptionModel> = [];
}

class EditProfileRequest_FieldAddressModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}

class EditProfileRequest_FieldPhoneModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}

class EditProfileRequest_MailModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}

class EditProfileRequest_FieldDescriptionModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}
