//
//  NearbyRequestsRequest.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 12/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class NearbyRequestsRequest: BaseRequest
{
    var latitude:Double!;
    var longitude:Double!;
    var coverageMiles:Double!;
    var epochTime:Int64!;  //in milliseconds
    
}
