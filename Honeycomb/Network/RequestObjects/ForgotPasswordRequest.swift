//
//  ForgotPasswordRequest.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 31/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class ForgotPasswordRequest: BaseRequest
{
    var emailID:String!;
}
