//
//  AddPitchRequest.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 17/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class AddPitchRequest: BaseRequest
{
    var type:Array<AddPitchRequest_TypeModel> = [];
    var title:Array<AddPitchRequest_TitleModel> = [];
    var field_location_name:Array<AddPitchRequest_FieldLocationNameModel> = [];
    var field_geolocation:Array<AddPitchRequest_FieldGeoLocationModel> = [];
    
    var field_tags:Array<AddPitchRequest_FieldTagsModel> = [];
    var field_details:Array<AddPitchRequest_FieldDetailsModel> = [];
    
    var field_video_posters:Array<AddPitchRequest_FieldVideoPostersModel> = [];
    var field_video_json:Array<AddPitchRequest_FieldVideoJsonModel> = [];
}

class AddPitchRequest_TypeModel: NSObject
{
    var target_id:String!;
    
    init(target_id:String!)
    {
        self.target_id = target_id;
    }
}

class AddPitchRequest_TitleModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}

class AddPitchRequest_FieldLocationNameModel: NSObject
{
    var value:String?;
    
    init(value:String?)
    {
        self.value = value;
    }
}

class AddPitchRequest_FieldGeoLocationModel: NSObject
{
    var lat:Double?;
    var long:Double?;
    
    init(lat:Double?, long:Double?)
    {
        self.lat = lat;
        self.long = long;
    }
}

class AddPitchRequest_FieldTagsModel: NSObject
{
    var name:String!;
    
    init(name:String!)
    {
        self.name = name;
    }
}

class AddPitchRequest_FieldDetailsModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}

class AddPitchRequest_FieldVideoPostersModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}

class AddPitchRequest_FieldVideoJsonModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}
