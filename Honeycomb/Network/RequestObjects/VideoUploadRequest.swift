//
//  VideoUploadRequest.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 10/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

class VideoUploadRequest: BaseRequest
{
    var type:Array<VideoUploadRequest_TypeModel> = [];
    var title:Array<VideoUploadRequest_TitleModel> = [];
    var field_video_request_reference:Array<VideoUploadRequest_FieldVideoReqRefModel> = [];
    var field_video_posters:Array<VideoUploadRequest_FieldVideoPostersModel> = [];
    var field_video_json:Array<VideoUploadRequest_FieldVideoJsonModel> = [];
}

class VideoUploadRequest_TypeModel: NSObject
{
    var target_id:String!;
    
    init(target_id:String!)
    {
        self.target_id = target_id;
    }
}

class VideoUploadRequest_TitleModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}

class VideoUploadRequest_FieldVideoReqRefModel: NSObject
{
    var target_id:String!;
    
    init(target_id:String!)
    {
        self.target_id = target_id;
    }
}

class VideoUploadRequest_FieldVideoPostersModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}

class VideoUploadRequest_FieldVideoJsonModel: NSObject
{
    var value:String!;
    
    init(value:String!)
    {
        self.value = value;
    }
}
