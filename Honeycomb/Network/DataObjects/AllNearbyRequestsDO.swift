//
//  AllNearbyRequestsDO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 12/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AllNearbyRequestsDO: BaseDO
{
    override func methodType() -> DOBOConstants.HTTPMethodType
    {
        return DOBOConstants.HTTPMethodType.Get;
    }
    
    override func methodName()->String?
    {
        let req = (request as! AllNearbyRequestsRequest);
        let epochTime = req.epochTime
        
        return "nearby-requests/all?_format=json&time=\(epochTime!)"
    }
    
    override func xCSRFToken() -> String?
    {
        return UserRegistrationHelper.getInstance().getCSRFToken();
    }
    
    override func encoding() -> ParameterEncoding
    {
        return JSONEncoding.default;
    }
    
    override func prepareRequestWithParameters(parameters: AnyObject?...)
    {
        if(!parameters.isEmpty)
        {
            let allNearbyRequestsRequest:AllNearbyRequestsRequest = AllNearbyRequestsRequest();
            allNearbyRequestsRequest.epochTime = parameters[0] as? Int64;
            
            request=allNearbyRequestsRequest;
        }
    }
    
    override func parse()
    {
        print("nearbyRequestsResponse - JSON Response \(self.jsonResponse)");
        
        let nearbyRequestsResponse:NearbyRequestsResponse = NearbyRequestsResponse()
        nearbyRequestsResponse.nearbyRequestsBOsArray = [];
        
        if let responseObjDict = self.jsonResponse as? Dictionary<String,AnyObject>
        {
            if((responseObjDict["message"] as! String) == "token mismatch")
            {
                let nearbyRequestsBO = NearbyRequestsBO()
                nearbyRequestsBO.returnMsg = "token mismatch";
                nearbyRequestsResponse.nearbyRequestsBOsArray!.append(nearbyRequestsBO)
            }
        }
        else if let nearbyRequestsResponseObj=self.jsonResponse as? Array<Dictionary<String,AnyObject>>
        {
            let jsonobj = JSON(nearbyRequestsResponseObj).arrayValue

            for item in jsonobj
            {
                print(item["title"].stringValue);

                let nearbyRequestsBO = NearbyRequestsBO(title: item["title"].stringValue,
                                                        field_details: item["field_details"].stringValue,
                                                        nid: item["nid"].stringValue,
                                                        field_geolocation: nil, // item["field_geolocation"].stringValue        //TODO: Vineet Change nil to field_geolocation values
                                                        field_expiry_date: item["field_expiry_date"].stringValue,
                                                        field_location_name: item["field_location_name"].stringValue,
                                                        field_amount: item["field_amount"].stringValue,
                                                        flagged: item["flagged"].stringValue)

                nearbyRequestsResponse.nearbyRequestsBOsArray!.append(nearbyRequestsBO);
            }
            //            myUploadsResponse.myUploadsBO.isPitch = jsonobj["isPitch"].stringValue;       // May be added in the future
        }
    
            response = nearbyRequestsResponse;
    }
}
