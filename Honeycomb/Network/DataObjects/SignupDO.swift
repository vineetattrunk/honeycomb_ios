//
//  SignupDO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 16/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class SignupDO: BaseDO
{
    override func methodType() -> DOBOConstants.HTTPMethodType
    {
        return DOBOConstants.HTTPMethodType.Post;
    }
    
    override func methodName()->String?
    {
        return "tokenized_auth/user/register?_format=json"
    }
    
    override func encoding() -> ParameterEncoding
    {
        return JSONEncoding.default;
    }
    
    override func prepareRequestWithParameters(parameters: AnyObject?...)
    {
        if(!parameters.isEmpty)
        {
            let signupRequest:SignupRequest =  SignupRequest();
            
            signupRequest.name = parameters[0] as? String;
            signupRequest.mail = parameters[1] as? String;
            signupRequest.field_address = parameters[2] as? String;
            signupRequest.field_device_token = parameters[3] as? String;
            signupRequest.field_name = parameters[4] as? String;
            signupRequest.field_phone = parameters[5] as? String;
            signupRequest.pass = parameters[6] as? String;
            
            request=signupRequest;
        }
    }
    
    override func requestParameters() -> Dictionary<String, AnyObject>?
    {
        let signupRequest:SignupRequest = (request as? SignupRequest)!;
        
        var params=Dictionary<String,Dictionary<String,AnyObject>>()
        
        params["name"] = [:];
        params["mail"] = [:];
        params["field_address"] = [:];
        params["field_device_token"] = [:];
        params["field_name"] = [:];
        params["field_phone"] = [:];
        params["pass"] = [:];
        
        
        
        params["name"]!["value"] = (signupRequest.name as String) as AnyObject?;
        params["mail"]!["value"] = (signupRequest.mail as String) as AnyObject?;
        params["field_address"]!["value"] = (signupRequest.field_address as String) as AnyObject?;
        params["field_device_token"]!["value"] = (signupRequest.field_device_token as String) as AnyObject?;
        params["field_name"]!["value"] = (signupRequest.field_name as String) as AnyObject?;
        params["field_phone"]!["value"] = (signupRequest.field_phone as String) as AnyObject?;
        params["pass"]!["value"] = (signupRequest.pass as String) as AnyObject?;
        
        print("params - \(params)");
        
        return params as Dictionary<String, AnyObject>;
    }
    
    override func parse()
    {
        let signupResponse:SignupResponse = SignupResponse()
        signupResponse.signupBO = SignupBO()
        
        //        print("LoginBO - JSON Response \(self.jsonResponse)");
        
        let loginResponseObj=self.jsonResponse as! Dictionary<String,AnyObject>
        
        if let errorMessage = (loginResponseObj["message"] as? String)
        {
            print("errorMessage - \(errorMessage)");
            
            signupResponse.signupBO.returnMsg = errorMessage;
        }
        else
        {
            signupResponse.signupBO.access = loginResponseObj["access"] as? Int;
            signupResponse.signupBO.changed = loginResponseObj["changed"] as? Int64;
            signupResponse.signupBO.created = loginResponseObj["created"] as? Int64;
            signupResponse.signupBO.csrf_token = loginResponseObj["csrf_token"] as? String;
            signupResponse.signupBO.default_langcode = loginResponseObj["default_langcode"] as? Bool;
            
            signupResponse.signupBO.field_address = loginResponseObj["field_address"] as? String;
            signupResponse.signupBO.field_description = loginResponseObj["field_description"] as? String;
            signupResponse.signupBO.field_device_token = loginResponseObj["field_device_token"] as? String;
            signupResponse.signupBO.field_name = loginResponseObj["field_name"] as? String;
            signupResponse.signupBO.field_phone = loginResponseObj["field_phone"] as? String;
            signupResponse.signupBO.init_ = loginResponseObj["init"] as? String;
            signupResponse.signupBO.langcode = loginResponseObj["langcode"] as? String;
            
            signupResponse.signupBO.langcode = loginResponseObj["langcode"] as? String;
            signupResponse.signupBO.login = loginResponseObj["login"] as? String;
            signupResponse.signupBO.mail = loginResponseObj["mail"] as? String;
            signupResponse.signupBO.name = loginResponseObj["name"] as? String;
            
            signupResponse.signupBO.pass = loginResponseObj["pass"] as? Array<Any?>;
            
            signupResponse.signupBO.preferred_admin_langcode = loginResponseObj["preferred_admin_langcode"] as? String;
            signupResponse.signupBO.preferred_langcode = loginResponseObj["preferred_langcode"] as? String;
            
            signupResponse.signupBO.roles = [];
            
            if let roles = loginResponseObj["roles"] as? Array<Dictionary<String,Any>> //loginResponse.loginBO.roles
            {
                for item in roles
                {
                    let targetModel = TargetModel(target_id: item["target_id"] as? String,
                                                  target_type: item["target_type"] as? String,
                                                  target_uuid: item["target_uuid"] as? String)
                    
                    signupResponse.signupBO.roles!.append(targetModel);
                }
            }
            
            signupResponse.signupBO.status = loginResponseObj["status"] as? Int;
            signupResponse.signupBO.timezone = loginResponseObj["timezone"] as? String;
            signupResponse.signupBO.uid = loginResponseObj["uid"] as? String;
            signupResponse.signupBO.user_picture = loginResponseObj["user_picture"] as? String;
            signupResponse.signupBO.uuid = loginResponseObj["uuid"] as? String;
        }
        
        response = signupResponse;
    }
}
