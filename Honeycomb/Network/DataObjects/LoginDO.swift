//
//  InsertRegistrationDetailsDO.swift
//  FGI-MyDrive
//
//  Created by Vineet on 4/27/17.
//  Copyright © 2017 Idealake. All rights reserved.
//

import Foundation
import Alamofire

class LoginDO: BaseDO
{
    override func methodType() -> DOBOConstants.HTTPMethodType
    {
        return DOBOConstants.HTTPMethodType.Post;
    }
    
    override func methodName()->String?
    {
        return "tokenized_auth/user/login?_format=json"
    }
    
    override func encoding() -> ParameterEncoding
    {
        return JSONEncoding.default;
    }
    
    override func prepareRequestWithParameters(parameters: AnyObject?...)
    {
        if(!parameters.isEmpty)
        {
            let loginRequest:LoginRequest = LoginRequest();
            loginRequest.name = parameters[0] as? String;
            loginRequest.pass = parameters[1] as? String;
            
            request=loginRequest;
        }
    }
    
    override func requestParameters() -> Dictionary<String, AnyObject>?
    {
        let loginRequest:LoginRequest = (request as? LoginRequest)!;
        
        var requestParameters=Dictionary<String,AnyObject>()
        
        requestParameters["name"]=(loginRequest.name as String) as AnyObject?;
        requestParameters["pass"]=(loginRequest.pass as String) as AnyObject?;
        
        print(requestParameters);
        
        return requestParameters;
    }
    
    override func parse()
    {
        let loginResponse:LoginResponse = LoginResponse()
        loginResponse.loginBO = LoginBO()
        
//        print("LoginBO - JSON Response \(self.jsonResponse)");
        
        if let loginResponseObj=self.jsonResponse as? Dictionary<String,AnyObject>
        {
            if let returnMessage = loginResponseObj["message"] as? String
            {
                loginResponse.loginBO.returnMsg = returnMessage;
            }
            else
            {
                loginResponse.loginBO.access = loginResponseObj["access"] as? String;
                loginResponse.loginBO.changed = loginResponseObj["changed"] as? String;
                loginResponse.loginBO.created = loginResponseObj["created"] as? String;
                loginResponse.loginBO.csrf_token = loginResponseObj["csrf_token"] as? String;
                loginResponse.loginBO.default_langcode = loginResponseObj["default_langcode"] as? String;
                
                loginResponse.loginBO.field_address = loginResponseObj["field_address"] as? String;
                loginResponse.loginBO.field_description = loginResponseObj["field_description"] as? String;
                loginResponse.loginBO.field_device_token = loginResponseObj["field_device_token"] as? String;
                loginResponse.loginBO.field_name = loginResponseObj["field_name"] as? String;
                loginResponse.loginBO.field_phone = loginResponseObj["field_phone"] as? String;
                loginResponse.loginBO.init_ = loginResponseObj["init"] as? String;
                loginResponse.loginBO.langcode = loginResponseObj["langcode"] as? String;
                
                loginResponse.loginBO.langcode = loginResponseObj["langcode"] as? String;
                loginResponse.loginBO.login = loginResponseObj["login"] as? String;
                loginResponse.loginBO.mail = loginResponseObj["mail"] as? String;
                loginResponse.loginBO.name = loginResponseObj["name"] as? String;
                
                loginResponse.loginBO.pass = loginResponseObj["pass"] as? Array<Any?>;
                
                loginResponse.loginBO.preferred_admin_langcode = loginResponseObj["preferred_admin_langcode"] as? String;
                loginResponse.loginBO.preferred_langcode = loginResponseObj["preferred_langcode"] as? String;
                
                loginResponse.loginBO.roles = [];
                
                if let roles = loginResponseObj["roles"] as? Array<Dictionary<String,Any>> //loginResponse.loginBO.roles
                {
                    for item in roles
                    {
                        let targetModel = TargetModel(target_id: item["target_id"] as? String,
                                                      target_type: item["target_type"] as? String,
                                                      target_uuid: item["target_uuid"] as? String)
                        
                        loginResponse.loginBO.roles!.append(targetModel);
                    }
                }
                
                loginResponse.loginBO.status = loginResponseObj["status"] as? String;
                loginResponse.loginBO.timezone = loginResponseObj["timezone"] as? String;
                loginResponse.loginBO.uid = loginResponseObj["uid"] as? String;
                loginResponse.loginBO.user_picture = loginResponseObj["user_picture"] as? String;
                loginResponse.loginBO.uuid = loginResponseObj["uuid"] as? String;
            }
        }
        
        response = loginResponse;
    }
}
