//
//  AddPitchDO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 17/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AddPitchDO:BaseDO
{
    override func methodType() -> DOBOConstants.HTTPMethodType
    {
        return DOBOConstants.HTTPMethodType.Post;
    }
    
    override func methodName()->String?
    {
        return "entity/node?_format=json"
    }
    
    override func encoding() -> ParameterEncoding
    {
        return JSONEncoding.default;
    }
    
    override func xCSRFToken() -> String?
    {
        return UserRegistrationHelper.getInstance().getCSRFToken();
    }
    
    override func prepareRequestWithParameters(parameters: AnyObject?...)
    {
        if(!parameters.isEmpty)
        {
            let addPitchRequest:AddPitchRequest = AddPitchRequest();
            
            addPitchRequest.type.append(AddPitchRequest_TypeModel(target_id: parameters[0] as? String))
            addPitchRequest.title.append(AddPitchRequest_TitleModel(value: parameters[1] as? String))
            addPitchRequest.field_location_name.append(AddPitchRequest_FieldLocationNameModel(value: parameters[2] as? String))
            addPitchRequest.field_geolocation.append(AddPitchRequest_FieldGeoLocationModel(lat: parameters[3] as? Double, long: parameters[4] as? Double))
            addPitchRequest.field_tags.append(AddPitchRequest_FieldTagsModel(name: parameters[5] as? String))
            addPitchRequest.field_details.append(AddPitchRequest_FieldDetailsModel(value: parameters[6] as? String))
            addPitchRequest.field_video_posters.append(AddPitchRequest_FieldVideoPostersModel(value: parameters[7] as? String))
            addPitchRequest.field_video_json.append(AddPitchRequest_FieldVideoJsonModel(value: parameters[8] as? String))
            
            request=addPitchRequest;
        }
    }
    
    override func requestParameters() -> Dictionary<String, AnyObject>?
    {
        let addPitchRequest:AddPitchRequest = (request as? AddPitchRequest)!;
        
        var requestParameters=Dictionary<String,AnyObject>()
        
        var typeParameterArray=Array<Dictionary<String,String>>();
        let typeParameterDict = ["target_id":(addPitchRequest.type[0].target_id!)];
        typeParameterArray.append(typeParameterDict);
        
        requestParameters["type"]=typeParameterArray as AnyObject?;
        
        
        var titleParameterArray=Array<Dictionary<String,String>>();
        let titleParameterDict = ["value":(addPitchRequest.title[0].value!)];
        titleParameterArray.append(titleParameterDict);
        
        requestParameters["title"]=titleParameterArray as AnyObject?;
        
        
        var field_location_nameParameterArray=Array<Dictionary<String,String>>();
        let field_location_name_referenceParameterDict = ["target_id":(addPitchRequest.field_location_name[0].value!)];
        field_location_nameParameterArray.append(field_location_name_referenceParameterDict);
        
        requestParameters["field_location_name"]=field_location_nameParameterArray as AnyObject?;
        
        
        var field_geolocationParameterArray=Array<Dictionary<String,Any>>();
        let field_geolocation_referenceParameterDict:Dictionary<String,Double> = ["lat":(addPitchRequest.field_geolocation[0].lat!), "lng":(addPitchRequest.field_geolocation[0].long!)];
        
        field_geolocationParameterArray.append(field_geolocation_referenceParameterDict);
        
        requestParameters["field_geolocation"]=field_geolocationParameterArray as AnyObject?;
        
        
        var field_tagsParameterArray=Array<Dictionary<String,String>>();
        let field_tags_referenceParameterDict = ["name":(addPitchRequest.field_tags[0].name!)];
        field_tagsParameterArray.append(field_tags_referenceParameterDict);
        
        requestParameters["field_tags"]=field_tagsParameterArray as AnyObject?;
        
        
        var field_detailsParameterArray=Array<Dictionary<String,String>>();
        let field_details_referenceParameterDict = ["value":(addPitchRequest.field_details[0].value!)];
        field_detailsParameterArray.append(field_details_referenceParameterDict);
        
        requestParameters["field_details"]=field_detailsParameterArray as AnyObject?;
        
        
        var field_video_postersParameterArray=Array<Dictionary<String,String>>();
        let field_video_postersParameterDict = ["value":(addPitchRequest.field_video_posters[0].value!)];
        field_video_postersParameterArray.append(field_video_postersParameterDict);
        
        requestParameters["field_video_posters"]=field_video_postersParameterArray as AnyObject?;
        
        
        var field_video_jsonParameterArray=Array<Dictionary<String,String>>();
        let field_video_jsonParameterDict = ["value":(addPitchRequest.field_video_json[0].value!)];
        field_video_jsonParameterArray.append(field_video_jsonParameterDict);
        
        requestParameters["field_video_json"]=field_video_jsonParameterArray as AnyObject?;
        
        return requestParameters;
    }
    
    override func parse()
    {
        let videoUploadResponse:VideoUploadResponse = VideoUploadResponse()
        videoUploadResponse.videoUploadBO = VideoUploadBO()
        
        if let videoUploadResponseObj=self.jsonResponse as? Dictionary<String,AnyObject>
        {
            if let errorMessage = (videoUploadResponseObj["message"] as? String)
            {
                print("errorMessage - \(errorMessage)");
                
                videoUploadResponse.videoUploadBO.returnMsg = errorMessage;
            }
            else
            {
                let jsonobj = JSON(videoUploadResponseObj)
                
                print(jsonobj);
                
                videoUploadResponse.videoUploadBO.url = jsonobj["field_video_json"]["url"].stringValue;
                videoUploadResponse.videoUploadBO.secure_url = jsonobj["field_video_json"]["secure_url"].stringValue;
                videoUploadResponse.videoUploadBO.created_at = jsonobj["field_video_json"]["created_at"].stringValue;
                videoUploadResponse.videoUploadBO.resource_type = jsonobj["field_video_json"]["resource_type"].stringValue;
                
                videoUploadResponse.videoUploadBO.field_video_posters = jsonobj["field_video_posters"].stringValue;
                videoUploadResponse.videoUploadBO.nid = jsonobj["nid"].stringValue;
                videoUploadResponse.videoUploadBO.title = jsonobj["title"].stringValue;
                videoUploadResponse.videoUploadBO.uid_target_id = jsonobj["uid"].arrayValue[0].dictionaryValue["target_id"]!.stringValue;
                videoUploadResponse.videoUploadBO.uuid = jsonobj["uuid"].stringValue;
                videoUploadResponse.videoUploadBO.vid = jsonobj["vid"].stringValue;
                
                videoUploadResponse.videoUploadBO.tags = [];
                
                if let tags = videoUploadResponseObj["tags"] as? Array<Dictionary<String,String>>
                {
                    for item in tags
                    {
                        let tagsModel = TagsModel(tid: item["target_id"],
                                                  name: item["target_type"])
                        
                        videoUploadResponse.videoUploadBO.tags!.append(tagsModel);
                    }
                }
            }
        }
        
        response = videoUploadResponse;
    }

    
}
