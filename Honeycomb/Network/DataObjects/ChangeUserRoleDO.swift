//
//  ChangeUserRoleDO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 05/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import Alamofire

class ChangeUserRoleDO: BaseDO
{
    override func methodType() -> DOBOConstants.HTTPMethodType
    {
        return DOBOConstants.HTTPMethodType.Patch;
    }
    
    override func methodName()->String?
    {
        return "change/user/role?_format=json"
    }
    
    override func xCSRFToken() -> String?
    {
        return UserRegistrationHelper.getInstance().getCSRFToken() // Vineet - Get the user's xCSRFToken from UserDefaults.
    }
    
    override func encoding() -> ParameterEncoding
    {
        return JSONEncoding.default;
    }
    
    override func parse()
    {
        print("ChangeRole JSON Response \(self.jsonResponse)");
        
        // Since the response object is same as Login's
        
        let loginResponse:LoginResponse = LoginResponse()
        loginResponse.loginBO = LoginBO()

        if let loginResponseObj=self.jsonResponse as? Dictionary<String,AnyObject>
        {
            loginResponse.loginBO.access = loginResponseObj["access"] as? String;
            loginResponse.loginBO.changed = loginResponseObj["changed"] as? String;
            loginResponse.loginBO.created = loginResponseObj["created"] as? String;
            loginResponse.loginBO.csrf_token = loginResponseObj["csrf_token"] as? String;
            loginResponse.loginBO.default_langcode = loginResponseObj["default_langcode"] as? String;

            loginResponse.loginBO.field_address = loginResponseObj["field_address"] as? String;
            loginResponse.loginBO.field_description = loginResponseObj["field_description"] as? String;
            loginResponse.loginBO.field_device_token = loginResponseObj["field_device_token"] as? String;
            loginResponse.loginBO.field_name = loginResponseObj["field_name"] as? String;
            loginResponse.loginBO.field_phone = loginResponseObj["field_phone"] as? String;
            loginResponse.loginBO.init_ = loginResponseObj["init"] as? String;
            loginResponse.loginBO.langcode = loginResponseObj["langcode"] as? String;

            loginResponse.loginBO.langcode = loginResponseObj["langcode"] as? String;
            loginResponse.loginBO.login = loginResponseObj["login"] as? String;
            loginResponse.loginBO.mail = loginResponseObj["mail"] as? String;
            loginResponse.loginBO.name = loginResponseObj["name"] as? String;

            loginResponse.loginBO.pass = loginResponseObj["pass"] as? Array<Any?>;

            loginResponse.loginBO.preferred_admin_langcode = loginResponseObj["preferred_admin_langcode"] as? String;
            loginResponse.loginBO.preferred_langcode = loginResponseObj["preferred_langcode"] as? String;

            loginResponse.loginBO.roles = [];

            if let roles = loginResponseObj["roles"] as? Array<Dictionary<String,Any>> //loginResponse.loginBO.roles
            {
                for item in roles
                {
                    let targetModel = TargetModel(target_id: item["target_id"] as? String,
                                                  target_type: item["target_type"] as? String,
                                                  target_uuid: item["target_uuid"] as? String)

                    loginResponse.loginBO.roles!.append(targetModel);
                }
            }

            loginResponse.loginBO.status = loginResponseObj["status"] as? String;
            loginResponse.loginBO.timezone = loginResponseObj["timezone"] as? String;
            loginResponse.loginBO.uid = loginResponseObj["uid"] as? String;
            loginResponse.loginBO.user_picture = loginResponseObj["user_picture"] as? String;
            loginResponse.loginBO.uuid = loginResponseObj["uuid"] as? String;
        }

        response = loginResponse;
    }
}
