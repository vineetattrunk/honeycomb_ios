//
//  ForgotPasswordDO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 31/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ForgotPasswordDO: BaseDO
{
    override func methodType() -> DOBOConstants.HTTPMethodType
    {
        return DOBOConstants.HTTPMethodType.Post;
    }
    
    override func methodName()->String?
    {
        return "recovery/password?_format=json"
    }
    
    override func encoding() -> ParameterEncoding
    {
        return JSONEncoding.default;
    }
    
    override func prepareRequestWithParameters(parameters: AnyObject?...)
    {
        if(!parameters.isEmpty)
        {
            let forgotPasswordRequest:ForgotPasswordRequest =  ForgotPasswordRequest();
            forgotPasswordRequest.emailID = parameters[0] as? String;
            
            request=forgotPasswordRequest;
        }
    }
    
    override func requestParameters() -> Dictionary<String, AnyObject>?
    {
        let forgotPasswordRequest:ForgotPasswordRequest = (request as? ForgotPasswordRequest)!;
        
        var params=Dictionary<String,AnyObject>()
        
        params["mail"] = (forgotPasswordRequest.emailID) as AnyObject?;
        
        print("params - \(params)");
        
        return params as Dictionary<String, AnyObject>;
    }
    
    override func parse()
    {
        let forgotPasswordResponse:ForgotPasswordResponse = ForgotPasswordResponse();
        forgotPasswordResponse.forgotPasswordBO = ForgotPasswordBO()
        
        //        print("LoginBO - JSON Response \(self.jsonResponse)");
        
        let responseObj=self.jsonResponse as! Dictionary<String,AnyObject>
        
        if let errorMessage = (responseObj["message"] as? String)
        {
            print("errorMessage - \(errorMessage)");
            
            forgotPasswordResponse.forgotPasswordBO.returnMsg = errorMessage;
        }
        else
        {
            forgotPasswordResponse.forgotPasswordBO.message = responseObj["message"] as? String;
        }
        
        response = forgotPasswordResponse;
    }
}
