//
//  MyUploadsDO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 10/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MyUploadsDO: BaseDO
{
    override func methodType() -> DOBOConstants.HTTPMethodType
    {
        return DOBOConstants.HTTPMethodType.Get;
    }
    
    override func methodName()->String?
    {
        return "purchased-videos/\((request as! MyUploadsRequest).uid!)?_format=json&time=\((request as! MyUploadsRequest).epochTime)"
    }
    
    override func xCSRFToken() -> String?
    {
        return UserRegistrationHelper.getInstance().getCSRFToken();
    }
    
    override func encoding() -> ParameterEncoding
    {
        return JSONEncoding.default;
    }
    
    override func prepareRequestWithParameters(parameters: AnyObject?...)
    {
        if(!parameters.isEmpty)
        {
            let myUploadsRequest:MyUploadsRequest = MyUploadsRequest();
            myUploadsRequest.uid = parameters[0] as? String;
            myUploadsRequest.epochTime = parameters[1] as? Int64;

            request=myUploadsRequest;
        }
    }
    
    override func parse()
    {
//        print("MyUploadsDO - JSON Response \(self.jsonResponse)");
        
        let myUploadsResponse:MyUploadsResponse = MyUploadsResponse()
        myUploadsResponse.myUploadsBOsArray = [];
        
        if let myUploadsResponseObj=self.jsonResponse as? Array<Dictionary<String,AnyObject>>
        {
            let jsonobj = JSON(myUploadsResponseObj).arrayValue
            
            for item in jsonobj
            {
                print(item["title"].stringValue);
                
                let myUploadsBO = MyUploadsBO(created_at: item["field_video_json"]["created_at"].stringValue,
                                              field_video_posters: item["field_video_posters"].stringValue,
                                              title: item["title"].stringValue,
                                              nid: item["nid"].stringValue,
                                              uuid: item["uuid"].stringValue,
                                              vid: item["vid"].stringValue,
                                              isPitch: nil);
                
                myUploadsResponse.myUploadsBOsArray!.append(myUploadsBO);
            }
//            myUploadsResponse.myUploadsBO.isPitch = jsonobj["isPitch"].stringValue;       // May be added in the future
        }
        
        response = myUploadsResponse;
    }
}
