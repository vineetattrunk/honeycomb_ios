//
//  BaseDO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 05/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import Alamofire

class BaseDO
{
    var request:BaseRequest?;
    var response:BaseResponse?;
    var jsonResponse:AnyObject!;
    var serverErrorBO:ServerErrorBO?=nil;
    
    var tagOfThisDO:Int=0;
    
    func methodType()->DOBOConstants.HTTPMethodType
    {
        return DOBOConstants.HTTPMethodType.Post;
    }
    
    func baseURL()->String?
    {
        return DOBOConstants.BASE_URL_OF_SERVER;
    }
    
    func methodName()->String?
    {
        return nil;
    }
    
    func prepareRequestWithParameters(parameters:AnyObject?...)->Void
    {
        
    }
    
    func requestParameters()->Dictionary<String,AnyObject>?
    {
        return nil;
    }
    
    func accessToken()->String?
    {
        return nil;
    }
    
    func xCSRFToken()->String?
    {
        return nil;
    }
    
    func encoding()->ParameterEncoding
    {
        return URLEncoding.default;
    }
    
    func queryTheServer(baseDO:@escaping (BaseDO)->Void)->Void
    {
        if(self.methodType() == DOBOConstants.HTTPMethodType.Post)
        {
            print("\nPOST METHOD\n")
            print(self.baseURL()! + self.methodName()!)
            
            var headers = [
                "cache-control": "no-cache",
                "Content-Type":"application/json"
            ]
            
            if(self.accessToken() != nil)
            {
                headers["Authorization"] = "Bearer " + self.accessToken()!;
            }
            
            if(self.xCSRFToken() != nil)
            {
                headers["X-CSRF-Token"] = self.xCSRFToken()!;
            }
            
            let request = Alamofire.request((self.baseURL()! + self.methodName()!), method: .post, parameters: self.requestParameters(), encoding: self.encoding(), headers: headers)
            
            // print(request.debugDescription)
            
            request.responseJSON(completionHandler: { (response) in
                
                if let JSON = response.result.value
                {
                    print("\n\n Status Code in BaseDO - \(response.response!.statusCode)");
                    
                    self.jsonResponse=JSON as AnyObject!;
                    
                    if let jsonData = self.jsonResponse as? Dictionary<String,String>
                    {
                        if let errorText = jsonData["message"]
                        {
                            self.serverErrorBO=ServerErrorBO(errorText: errorText);
                            print("Server error - \(self.serverErrorBO!.errorText!)");
                        }
                    }
                    
                    self.parse();
                    baseDO(self)
                }
                else
                {
                    /*
                     This block is called if response.result.value has a 'nil' value, which means the server has returned with some error
                     But this block can also be called in case the statusCode is 200
                     for ex. In case of signing up of a new user
                     This block can also be called if statusCode is 'nil' which might be a case when the network is not reachable.
                     */
                    
                    let statusCode = response.response?.statusCode
                    
                    if(statusCode != nil)
                    {
                        if(statusCode != 200)
                        {
                            self.serverErrorBO=ServerErrorBO(errorText: self.getServerErrorString(statusCode: statusCode!));
                            baseDO(self)
                        }
                        else if(statusCode == 200)
                        {
                            // statusCode is 200 : OK
                            print("Status Code - 200! All OK.. :)")
                            baseDO(self)
                        }
                    }
                    else if(statusCode == nil)
                    {
                        self.serverErrorBO=ServerErrorBO(errorText: "Network not reachable.");
                        baseDO(self)
                    }
                }
            })
        }
        else if(self.methodType() == DOBOConstants.HTTPMethodType.Patch)
        {
            print("\nPATCH METHOD\n")
            print(self.baseURL()! + self.methodName()!)
            
            var headers = [
                "cache-control": "no-cache",
                "Content-Type":"application/json"
            ]
            
            if(self.xCSRFToken() != nil)
            {
                headers["X-CSRF-Token"] = self.xCSRFToken()!;
            }
            
            let request = Alamofire.request((self.baseURL()! + self.methodName()!), method: .patch, parameters: self.requestParameters(), encoding: self.encoding(), headers: headers)
            
            // print(request.debugDescription)
            
            request.responseJSON(completionHandler: { (response) in
                
                if let JSON = response.result.value
                {
                    print("\n\n Status Code in BaseDO - \(response.response!.statusCode)");
                    
                    self.jsonResponse=JSON as AnyObject!;
                    
                    if let jsonData = self.jsonResponse as? Dictionary<String,String>
                    {
                        if let errorText = jsonData["message"]
                        {
                            self.serverErrorBO=ServerErrorBO(errorText: errorText);
                            print("Server error - \(self.serverErrorBO!.errorText!)");
                        }
                    }
                    
                    self.parse();
                    baseDO(self)
                }
                else
                {
                    /*
                         This block is called if response.result.value has a 'nil' value, which means the server has returned with some error
                         But this block can also be called in case the statusCode is 200
                         for ex. In case of signing up of a new user
                         This block can also be called if statusCode is 'nil' which might be a case when the network is not reachable.
                     */
                    
                    let statusCode = response.response?.statusCode
                    
                    if(statusCode != nil)
                    {
                        if(statusCode != 200)
                        {
                            self.serverErrorBO=ServerErrorBO(errorText: self.getServerErrorString(statusCode: statusCode!));
                            baseDO(self)
                        }
                        else if(statusCode == 200)
                        {
                            // statusCode is 200 : OK
                            print("Status Code - 200! All OK.. :)")
                            baseDO(self)
                        }
                    }
                    else if(statusCode == nil)
                    {
                        self.serverErrorBO=ServerErrorBO(errorText: "Network not reachable.");
                        baseDO(self)
                    }
                }
            })
        }
        else if(self.methodType() == DOBOConstants.HTTPMethodType.Get)
        {
            print("\nGET METHOD\n____________________")
            print(self.baseURL()! + self.methodName()!)
            
            var headers = [
                "cache-control": "no-cache",
                "Accept": "application/json",
                "Content-Type":"application/json"
            ]
            
            if(self.accessToken() != nil)
            {
                headers["Authorization"] = "Bearer " + self.accessToken()!;
            }
            
            if(self.xCSRFToken() != nil)
            {
                headers["X-CSRF-Token"] = self.xCSRFToken()!;
            }
            
            //            print("Headers -\n \(headers)");
            //            print("Parameters - \n \(self.requestParameters())");
            
            Alamofire.request((self.baseURL()! + self.methodName()!), method: .get, parameters: self.requestParameters(), encoding: self.encoding(), headers: headers).responseJSON(completionHandler: { (response) in
                
                if let JSON = response.result.value
                {
                    print("\n\n RESPONSE VALUE IN BASEDO - \(JSON)");
                    
                    self.jsonResponse=JSON as AnyObject!;
                    
                    if let jsonData = self.jsonResponse as? Dictionary<String,String>
                    {
                        if let errorText = jsonData["message"]
                        {
                            self.serverErrorBO=ServerErrorBO(errorText: errorText);
                            print("Server error - \(self.serverErrorBO!.errorText!)");
                        }
                    }
                    
                    self.parse();
                    baseDO(self)
                }
                else
                {
                    /*
                     This block is called if response.result.value has a 'nil' value, which means the server has returned with some error
                     This block can also be called if statusCode is 'nil' which might be a case when the network is not reachable.
                     */
                    
                    let statusCode = response.response?.statusCode
                    
                    if(statusCode != nil)
                    {
                        if(statusCode != 200)
                        {
                            self.serverErrorBO=ServerErrorBO(errorText: self.getServerErrorString(statusCode: statusCode!));
                            baseDO(self)
                        }
                    }
                    else if(statusCode == nil)
                    {
                        self.serverErrorBO=ServerErrorBO(errorText: "Network not reachable.");
                        baseDO(self)
                    }
                }
            })
        }
    }
    
    func parse()->Void
    {
        
    }
    
    func cancel()->Void
    {
        //        operationManager.operationQueue.cancelAllOperations();
    }
    
    //MARK: Private Methods
    
    private func getServerErrorString(statusCode:Int) ->String
    {
        var errorString:String = "" ;
        
        switch statusCode
        {
        case 400:
            errorString = "Location out of operating area";
        case 401:
            errorString = "Username / password mismatch";
        case 403:
            errorString = "User is banned";
        case 404:
            errorString = "No cabs available";
        case 409:
            errorString = "User already exists";
        case 500:
            errorString = "Server Error";
        default:
            errorString = "OK"
        }
        
        return errorString;
    }
}

