//
//  VideoUploadDO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 09/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class VideoUploadDO: BaseDO
{
    private var xcrfToken:String?;
    
    override func methodType() -> DOBOConstants.HTTPMethodType
    {
        return DOBOConstants.HTTPMethodType.Post;
    }
    
    override func methodName()->String?
    {
        return "entity/node?_format=json"
    }
    
    override func encoding() -> ParameterEncoding
    {
        return JSONEncoding.default;
    }
    
    override func xCSRFToken() -> String?
    {
        return xcrfToken;
    }
    
    override func prepareRequestWithParameters(parameters: AnyObject?...)
    {
        if(!parameters.isEmpty)
        {
            let videoUploadRequest:VideoUploadRequest = VideoUploadRequest();
            
            self.xcrfToken = parameters[0] as? String;
            
            videoUploadRequest.type.append(VideoUploadRequest_TypeModel(target_id: parameters[1] as? String))
            
            videoUploadRequest.title.append(VideoUploadRequest_TitleModel(value: parameters[2] as? String))
            
            videoUploadRequest.field_video_request_reference.append(VideoUploadRequest_FieldVideoReqRefModel(target_id: parameters[3] as? String))
            
            videoUploadRequest.field_video_posters.append(VideoUploadRequest_FieldVideoPostersModel(value: parameters[4] as? String))
            
            videoUploadRequest.field_video_json.append(VideoUploadRequest_FieldVideoJsonModel(value: parameters[5] as? String))
            
            request=videoUploadRequest;
        }
    }
    
    override func requestParameters() -> Dictionary<String, AnyObject>?
    {
        let videoUploadRequest:VideoUploadRequest = (request as? VideoUploadRequest)!;
        
        var requestParameters=Dictionary<String,AnyObject>()
        
        var typeParameterArray=Array<Dictionary<String,String>>();
        let typeParameterDict = ["target_id":(videoUploadRequest.type[0].target_id!)];
        typeParameterArray.append(typeParameterDict);
        
            requestParameters["type"]=typeParameterArray as AnyObject?;
        
        
        var titleParameterArray=Array<Dictionary<String,String>>();
        let titleParameterDict = ["value":(videoUploadRequest.title[0].value!)];
        titleParameterArray.append(titleParameterDict);
        
            requestParameters["title"]=titleParameterArray as AnyObject?;
        
        
        var field_video_request_referenceParameterArray=Array<Dictionary<String,String>>();
        let field_video_request_referenceParameterDict = ["target_id":(videoUploadRequest.field_video_request_reference[0].target_id!)];
        field_video_request_referenceParameterArray.append(field_video_request_referenceParameterDict);
        
            requestParameters["field_video_request_reference"]=field_video_request_referenceParameterArray as AnyObject?;
        
        
        var field_video_postersParameterArray=Array<Dictionary<String,String>>();
        let field_video_postersParameterDict = ["value":(videoUploadRequest.field_video_posters[0].value!)];
        field_video_postersParameterArray.append(field_video_postersParameterDict);
        
            requestParameters["field_video_posters"]=field_video_postersParameterArray as AnyObject?;
        
        
        var field_video_jsonParameterArray=Array<Dictionary<String,String>>();
        let field_video_jsonParameterDict = ["value":(videoUploadRequest.field_video_json[0].value!)];
        field_video_jsonParameterArray.append(field_video_jsonParameterDict);
        
            requestParameters["field_video_json"]=field_video_jsonParameterArray as AnyObject?;
        
        
        return requestParameters;
    }
    
    override func parse()
    {
        let videoUploadResponse:VideoUploadResponse = VideoUploadResponse()
        videoUploadResponse.videoUploadBO = VideoUploadBO()
        
        if let videoUploadResponseObj=self.jsonResponse as? Dictionary<String,AnyObject>
        {
            if let errorMessage = (videoUploadResponseObj["message"] as? String)
            {
                print("errorMessage - \(errorMessage)");
                
                videoUploadResponse.videoUploadBO.returnMsg = errorMessage;
            }
            else
            {
                let jsonobj = JSON(videoUploadResponseObj)
                
                videoUploadResponse.videoUploadBO.url = jsonobj["field_video_json"]["url"].stringValue;
                videoUploadResponse.videoUploadBO.secure_url = jsonobj["field_video_json"]["secure_url"].stringValue;
                videoUploadResponse.videoUploadBO.created_at = jsonobj["field_video_json"]["created_at"].stringValue;
                videoUploadResponse.videoUploadBO.resource_type = jsonobj["field_video_json"]["resource_type"].stringValue;
                
                videoUploadResponse.videoUploadBO.field_video_posters = jsonobj["field_video_posters"].stringValue;
                videoUploadResponse.videoUploadBO.nid = jsonobj["nid"].stringValue;
                videoUploadResponse.videoUploadBO.title = jsonobj["title"].stringValue;
                videoUploadResponse.videoUploadBO.uid_target_id = jsonobj["uid"].arrayValue[0].dictionaryValue["target_id"]!.stringValue;
                videoUploadResponse.videoUploadBO.uuid = jsonobj["uuid"].stringValue;
                videoUploadResponse.videoUploadBO.vid = jsonobj["vid"].stringValue;
                
                videoUploadResponse.videoUploadBO.tags = [];
                
                if let tags = videoUploadResponseObj["tags"] as? Array<Dictionary<String,String>>
                {
                    for item in tags
                    {
                        let tagsModel = TagsModel(tid: item["target_id"],
                                                  name: item["target_type"])
                        
                        videoUploadResponse.videoUploadBO.tags!.append(tagsModel);
                    }
                }                
            }
        }
        
        response = videoUploadResponse;
    }
}
