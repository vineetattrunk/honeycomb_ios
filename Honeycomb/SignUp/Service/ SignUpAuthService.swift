//
//  AuthService.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//
import Foundation

protocol SignUpAuthService {
    
    //func signUp(_ user:User, response:@escaping (_ data:Data?, _ error:NSError?) -> (Void))
    func signUp(_ registerUser: RegisterUser, CompletionHandler:@escaping (_ data: Data?, _ error:NSError?) -> (Void))
}
