//
//  AuthService.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//
import Foundation

class SignUpRemoteAuthService : NSObject, SignUpAuthService {
    
    
    let host:String = "http://34.202.232.126/honeycomb/tokenized_auth/user/"
    
    fileprivate func requestWithBody(_ path: String, method: String, body: Data) -> NSMutableURLRequest {
        let request : NSMutableURLRequest = NSMutableURLRequest()
        request.url = URL(string: host + path)
        print(request.url!)
        request.httpMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        if method == "POST" {
            request.httpBody = body
        }
        
        return request
    }
    
    
    func signUp(_ registerUser: RegisterUser, CompletionHandler: @escaping (Data?, NSError?) -> (Void)) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: RegisterUser.encode(object: registerUser), options: .prettyPrinted)
            let request = self.requestWithBody("register?_format=json", method: "POST", body: jsonData)
            print(request)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
                (
                data, response, error) in
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    print("error")
                    return
                }
//                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//                print(dataString!)
                if let data = data {
                                        do {
                                            let json = try JSONSerialization.jsonObject(with: data, options: [])
                                            print(json)
                                            CompletionHandler(data,nil)
                    
                                        } catch {
                                            print(error)
                                        }
                                    }
                
            })
            // Start the task on a background thread
            task.resume()
        } catch {
            CompletionHandler(nil, NSError(domain: "invalid Json", code: 1, userInfo: ["registerUser": registerUser]))
        }
    }
    
//    func signUp(_ user:User, response:@escaping (_ data:Data?, _ error:NSError?) -> (Void)){
//
//        do {
//            let jsonData = try JSONSerialization.data(withJSONObject: User.encode(object: user), options: .prettyPrinted)
//            let request = self.requestWithBody("login?_format=json", method: "POST", body: jsonData)
//            print(request)
//
//            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
//                (
//                data, response, error) in
//                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
//                    print("error")
//                    return
//                }
//                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//                print(dataString!)
//
//            })
//            // Start the task on a background thread
//            task.resume()
//        } catch {
//            response(nil, NSError(domain: "invalid Json", code: 1, userInfo: ["user": user]))
//        }
//    }
//
}
