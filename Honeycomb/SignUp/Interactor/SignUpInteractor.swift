//
//  SignUpInteractor.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//
import Foundation
import UIKit


class SignUpInteractor: SignUpInteractorInput {
    
    weak var output: SignUpInteractorOutput!
    let signUpauthService : SignUpAuthService!
    
    init(signUpauthService :SignUpAuthService) {
        self.signUpauthService  = signUpauthService
    }
    
    
    func  performSignUpAPICall(_ username:String,usermail:String,useraddress:String,userdevicetoken:String,userfullname:String,userphone:String, userpassword:String)
    {
        // BYPass the Validationpart  by adding loginDidComplete out side do try catself.output.loginDidComplete()
      
        self.signUpauthService.signUp(RegisterUser(username: username, userpassword: userpassword, userMail: usermail, userAddress: useraddress, userDevice_token: userdevicetoken, userFullname: userfullname, userPhone: userphone)) { (data, error) -> (Void) in

            let genericError = NSError(domain: "Network/Server Error", code: 100, userInfo: nil)
            do {
                if let data = data,
                    let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String:AnyObject] {
                    print(json)
                    
                    

                    let currentUser = User.decode(json: json)

                    if currentUser.isValid() {

                        // set here local current user here

                        self.output.signUpDidComplete()

                    } else {

                        let code:Int? = json["errorCode"]?.intValue
                          //self.output.signUpDidFailWithError(NSError(domain: "Invalid LOGIN Error", code: code!, userInfo: nil))
//                        let alertController = UIAlertController(title: "Message", message:
//                            "This Email Id is already registered with us.", preferredStyle: UIAlertControllerStyle.alert)
//                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
//
//                        self.present(alertController, animated: true, completion: nil)

                        

                    }

                } else {
                    self.output.signUpDidFailWithError(error ?? genericError)
                }
            } catch {
                self.output.signUpDidFailWithError(genericError)
            }


        }
    }
    }
//    func performSignUpAPICall(_ username:String, pass:String) {
//
//        // BYPass the Validationpart  by adding loginDidComplete out side do try catself.output.loginDidComplete()
//        self.output.signUpDidComplete()
//
//        self.signUpauthService.signUp(RegisterUser(username:username, password: pass)) { (data, error) -> (Void) in
//
//            let genericError = NSError(domain: "Network/Server Error", code: 100, userInfo: nil)
//            do {
//                if let data = data,
//                    let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String:AnyObject] {
//
//                    let currentUser = User.decode(json: json)
//
//                    if currentUser.isValid() {
//
//                        // set here local current user here
//
//                        self.output.signUpDidComplete()
//
//                    } else {
//
//                        let code:Int? = json["errorCode"]?.intValue
//                        self.output.signUpDidFailWithError(NSError(domain: "Invalid LOGIN Error", code: code!, userInfo: nil))
//
//                    }
//
//                } else {
//                    self.output.signUpDidFailWithError(error ?? genericError)
//                }
//            } catch {
//                self.output.signUpDidFailWithError(genericError)
//            }
//
//
//        }
//
//
//    }


