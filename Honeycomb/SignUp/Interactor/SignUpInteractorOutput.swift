//
//  LoginLoginInteractorOutput.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//
import Foundation

protocol SignUpInteractorOutput: class {
    func signUpDidComplete()
    func signUpDidFailWithError(_ error:NSError)
}
