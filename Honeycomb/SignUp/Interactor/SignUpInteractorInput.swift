//
//  LoginLoginInteractorInput.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

protocol SignUpInteractorInput {

    func performSignUpAPICall(_ username:String,usermail:String,useraddress:String,userdevicetoken:String,userfullname:String,userphone:String, userpassword:String)
}
