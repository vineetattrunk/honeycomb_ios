//
//  User.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation

enum RegisterUserError: Error {
    case InvalidDecodeData
}

final class RegisterUser: NSObject  {

    var username: String
    var userpassword: String
    var userMail:String
    var userAddress:String
    var userDevice_token:String
    var userFullname:String
    var userPhone:String

    override init() {
        username = ""
        userpassword = ""
        userMail = ""
        userAddress = ""
        userDevice_token = ""
        userFullname = ""
        userPhone = ""
   
     }
    init(username: String, userpassword: String,userMail:String,userAddress:String,userDevice_token:String,userFullname:String,userPhone:String) {
                self.username = username
                self.userpassword = userpassword
                self.userMail = userMail
                self.userAddress = userAddress
                self.userDevice_token = userDevice_token
                self.userFullname = userFullname
                self.userPhone = userPhone
            }

    func isValid() -> Bool {
        return username.characters.count > 0
    }

    class func decode(json: [String:AnyObject]) -> RegisterUser {
        return RegisterUser(
            username: json["name"]?.description ?? "",
            userpassword: json["pass"]?.description ?? "",
            userMail: json["mail"]?.description ?? "",
            userAddress: json["field_address"]?.description ?? "",
            userDevice_token: json["field_device_token"]?.description ?? "",
            userFullname: json["field_name"]?.description ?? "",
            userPhone: json["field_phone"]?.description ?? ""
        )
    }

    class func encode(object: RegisterUser) -> AnyObject {
        
        var user = [String: AnyObject]()
        user["name"] = object.username as AnyObject
        user["pass"] = object.userpassword as AnyObject
        user["mail"] = object.userMail as AnyObject
        user["field_address"] = object.userAddress as AnyObject
        user["field_device_token"] = object.userDevice_token as AnyObject
        user["field_phone"] = object.userPhone as AnyObject
        user["field_name"] = object.userFullname as AnyObject
        
        print(user)
        
        return user as AnyObject
    }
}



