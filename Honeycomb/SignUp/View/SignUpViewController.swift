//
//  SignUpViewController.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//
import UIKit

class SignUpViewController: UIViewController, SignUpViewInput, UITextFieldDelegate {

    var output: SignUpViewOutput!
    
    @IBOutlet var userfullnameTextField: UITextField!
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var userphoneTextField: UITextField!
    @IBOutlet var userlocationTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var RepasswordTextField: UITextField!
    
    @IBOutlet var errorMessageLabel: UILabel!
    @IBOutlet var signUpButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usernameTextField.addTarget(self, action: #selector(SignUpViewController.textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(SignUpViewController.textFieldDidChange(_:)), for: .editingChanged)
        usernameTextField.becomeFirstResponder()
        
        userfullnameTextField.attributedPlaceholder = NSAttributedString(string: "Name",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: AppConstants.COLOR_1]);
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: AppConstants.COLOR_1]);
        userphoneTextField.attributedPlaceholder = NSAttributedString(string: "Phone",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: AppConstants.COLOR_1]);
        userlocationTextField.attributedPlaceholder = NSAttributedString(string: "Location",
                                                                      attributes: [NSAttributedStringKey.foregroundColor: AppConstants.COLOR_1]);
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                         attributes: [NSAttributedStringKey.foregroundColor: AppConstants.COLOR_1]);
        RepasswordTextField.attributedPlaceholder = NSAttributedString(string: "RePassword",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: AppConstants.COLOR_1]);
        
        
        self.userfullnameTextField.delegate = self
        self.usernameTextField.delegate = self
         self.userphoneTextField.delegate = self
         self.userlocationTextField.delegate = self
        self.passwordTextField.delegate = self
        self.RepasswordTextField.delegate = self
        
        
        output.viewIsReady()
//        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//        
              
    }
    

    @objc func textFieldDidChange(_ textField:UITextField) {
        signUpButton.isEnabled = self.output.isValidSignUpForm(userfullnameTextField.text!, usermail: usernameTextField.text!, useraddress: userlocationTextField.text!, userdevicetoken: "", userfullname: userfullnameTextField.text!, userphone: userphoneTextField.text!, userpassword: passwordTextField.text!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       errorMessageLabel.alpha = 0

    }

    @IBAction func backwardNav(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    @IBAction func handleSignUp(_ sender: AnyObject)
    {
        let signupDO = SignupDO();
        signupDO.prepareRequestWithParameters(parameters: userfullnameTextField.text as AnyObject?,
                                              usernameTextField.text as AnyObject?,
                                              userlocationTextField.text as AnyObject?,
                                              userfullnameTextField.text as AnyObject?, // TODO: Vineet - Change to Firebase deviceToken
                                              userfullnameTextField.text as AnyObject?, // TODO : Vineet - Change to Field name
                                              userphoneTextField.text as AnyObject?,
                                             passwordTextField.text as AnyObject?)
        signupDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                    
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    let errorTextToDisplay = baseDO.serverErrorBO!.errorText!;
                    
                    let alertController = UIAlertController(title: "Something's Wrong!", message: errorTextToDisplay, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "Ok!", style: .default, handler: nil)
                    alertController.addAction(defaultAction);
                    
                    self.present(alertController, animated: true, completion: nil);
                }
            }
            else
            {
                // Response received successfully.
                
                if let signupBO = (signupDO.response as! SignupResponse).signupBO
                {
                    if(signupBO.returnMsg != nil)
                    {
                        // There is some problem therefore display an alert to the user.
                        
                        let alertController = UIUtil.standardUIAlertController(title: "Alert", message: signupBO.returnMsg!, preferredStyle: .alert)
                        self.present(alertController, animated: true, completion: nil);
                    }
                    else
                    {
                        print("\n\n\n\n CSRF_TOKEN - \(signupBO.csrf_token!)");
                        
                        for item in signupBO.roles!
                        {
                            print("\ntarget_id - \(item.target_id!)");
                            
                            UserRegistrationHelper.getInstance().setUserRegistrationInfo(name: signupBO.name!,
                                                                                         emailID: signupBO.mail!,
                                                                                         mobileNumber: signupBO.field_phone!,
                                                                                         location: signupBO.field_address!,
                                                                                         csrfToken: signupBO.csrf_token!,
                                                                                         uid: signupBO.uid!);
                        }
                        
                        //                    self.output.performLogin(self.usernameTextField.text ?? "", pass: self.passwordTextField.text ?? "");         // Old implementation
                        App_UI_Helper.appDelegate().window?.rootViewController = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController()
                    }
                    
                }
            }
        }
        
        // Old implementation
//        self.output.performSignUp(userfullnameTextField.text ?? "", usermail: usernameTextField.text ?? "", useraddress: userlocationTextField.text ?? "", userdevicetoken: "21236", userfullname: userfullnameTextField.text ?? "", userphone: userphoneTextField.text ?? "", userpassword: passwordTextField.text ?? "")
        
    }
    
    //// Dismiss keyboard Notification Methodes
//    func keyboardWillShow(_ notification:Notification) {
//        adjustingHeight(true, notification: notification)
//    }
//
//    func keyboardWillHide(_ notification:Notification) {
//        adjustingHeight(false, notification: notification)
//    }
//
//    func adjustingHeight(_ show:Bool, notification:Notification) {
//
//        let userInfo = notification.userInfo!
//
//        let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
//
//        let changeInHeight = (keyboardFrame.height + 40) * (show ? 1 : -1)
//
//        scrollView.contentInset.bottom += changeInHeight
//
//        scrollView.scrollIndicatorInsets.bottom += changeInHeight
//
//    }
//
   
    // Dismiss keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }


    // MARK: SignUpViewInput
    func setupInitialState() {
        
    }

    func showErrorMessage(_ message: String) {
        DispatchQueue.main.async(execute: { () -> Void in
            self.errorMessageLabel.alpha = 0
            self.errorMessageLabel.text = message
            
            UIView.animate(withDuration: 0.3, animations: {
                self.errorMessageLabel.alpha = 1
            }) 
            })
        
        
    }
}

//extension SignUpViewController: UITextFieldDelegate{
//    func textFieldShouldReturn(textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//
//       // self.view.endEditing(true)
//       // return false
//    }}

