//
//  SignUpViewInput.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

protocol SignUpViewInput: class {

    /**
        @author Samarjeet Dubey
        Setup initial state of the view
    */

    func setupInitialState()
    func showErrorMessage(_ message:String)
    
}
