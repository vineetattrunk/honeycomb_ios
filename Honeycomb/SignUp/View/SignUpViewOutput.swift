//
//  SignUpViewOutput.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

protocol SignUpViewOutput {
    
    func viewIsReady()
    func performSignUp(_ username:String,usermail:String,useraddress:String,userdevicetoken:String,userfullname:String,userphone:String, userpassword:String)
    func isValidSignUpForm(_ username:String,usermail:String,useraddress:String,userdevicetoken:String,userfullname:String,userphone:String, userpassword:String) -> Bool
}
