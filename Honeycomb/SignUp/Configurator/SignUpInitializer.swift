//
//  SignUpinInitializer.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

class SignUpModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var signUpViewController: SignUpViewController!

    override func awakeFromNib() {

        let configurator = SignUpModuleConfigurator()
        configurator.configureModuleForViewInput(signUpViewController)
    }

}
