//
//  SignUpConfigurator.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

class SignUpModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(_ viewInput: UIViewController) {

        if let viewController = viewInput as? SignUpViewController {
            configure(viewController)
        }
    }
    fileprivate func configure(_ viewController: SignUpViewController) {

        let router = SignUpRouter()

        let presenter = SignUpPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = SignUpInteractor(signUpauthService:SignUpRemoteAuthService())
        interactor.output = presenter

        presenter.interactor = interactor 
        viewController.output = presenter
    }

}
