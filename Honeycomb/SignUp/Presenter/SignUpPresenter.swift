//
//  SignUpPresenter.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//
import UIKit

class SignUpPresenter: SignUpModuleInput, SignUpViewOutput, SignUpInteractorOutput {
    
    weak var view: SignUpViewInput!
    var interactor: SignUpInteractorInput!
    var router: SignUpRouterInput!

    func viewIsReady() {
    
    }
    
    
    func isValidSignUpForm(_ username: String, usermail: String, useraddress: String, userdevicetoken: String, userfullname: String, userphone: String, userpassword: String) -> Bool {
        
        return true
        
    }
    
    
//    func isValidSignUpForm(_ username:String?, pass:String?) -> Bool {
//        if let username = username,
//            let pass = pass, !username.isEmpty && !pass.isEmpty {
//
//            return true
//
//        } else {
//            return false
//        }
//
//    }
    
    func performSignUp(_ username: String, usermail: String, useraddress: String, userdevicetoken: String, userfullname: String, userphone: String, userpassword: String) {
        if self.isValidSignUpForm(username, usermail: usermail, useraddress: useraddress, userdevicetoken: userdevicetoken, userfullname: userfullname, userphone: userphone, userpassword: userpassword)
        {
            self.interactor.performSignUpAPICall(username, usermail: usermail, useraddress: useraddress, userdevicetoken: userdevicetoken, userfullname: userfullname, userphone: userphone, userpassword: userpassword)
        } else {
            view.showErrorMessage("Username and password can't be empty.")
        }
    }
    
    
//    func performSignUp(_ username:String, pass:String)  {
//
//        if self.isValidSignUpForm(username, pass: pass) {
//
//          //  self.interactor.performSignUpAPICall(username, pass: pass)
//
//        } else {
//            view.showErrorMessage("Username and password can't be empty.")
//        }
//
//    }
    
    // MARK: SignUpInteractorOutput
    
    func signUpDidComplete() {
        
        if let from = self.view as? UIViewController {
            self.router.showWelcomeViewController(from)
        }
    }
    
    func signUpDidFailWithError(_ error: NSError) {
        switch error.code {
        case 1:
            view.showErrorMessage("Localized SignUp error for code: \(error.code) try pawan@123./123")
        default:
            view.showErrorMessage("Localized NETWORK error for code: \(error.code).")
        }
        
    }
}
