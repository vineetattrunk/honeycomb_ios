//
//  SignUpRouter.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//
import UIKit


class SignUpRouter: SignUpRouterInput {
    
    func showWelcomeViewController(_ fromVC:UIViewController) {
        
        
        DispatchQueue.main.async{
            
            
            self.appDelegate.window?.rootViewController = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController()
            // let alert = CustomAlert(title: "Hello there!! 👋🏻👋🏻", image: UIImage(named: "img")!)
            //alert.show(animated: true)
            //            if let welcomeVC = fromVC.storyboard?.instantiateViewController(withIdentifier: "NBRequest") {
            //
            //                fromVC.navigationController?.pushViewController(welcomeVC, animated: true)
            //            }
            
        }
        
    }
    
}
extension SignUpRouterInput {
    var appDelegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}

