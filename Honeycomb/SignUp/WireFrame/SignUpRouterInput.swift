//
//  SignUpRouterInput.swift
//  Honeycomb
//
//  Created by Samarjeet on 28/07/17.
//  Copyright © 2017 Trunk. All rights reserved.
//
import UIKit

protocol SignUpRouterInput {
    func showWelcomeViewController(_ fromVC:UIViewController)
}
