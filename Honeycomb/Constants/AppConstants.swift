//
//  BaseDO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 05/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import Foundation
import UIKit

class AppConstants:NSObject
{
    
    static let GOOGLE_API_KEY:String = "AIzaSyCAaHacEq85AUMiMrAku4hdBMtgZTvIDnA"; //"AIzaSyDQ5RW0W-3ZRYyHWdY8SVzWGaFLBclvR1c";//"AIzaSyDisnm2tcQjpt5kxxslwY6PB5ztnz6pk2I";   // Idealake's test key
    
    static let COVERAGE_MILES:Double = 50.0;
    
    static let APP_VERSION:String=(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String);
    static let DEVICE_MODEL = UIDevice.current.model;
    static let DEVICE_SYSTEM_VERSION = UIDevice.current.systemVersion
    
    static let SHARE_APP_DETAILS_STRING:String="Hi, I am using 'FGI-MyDrive', You must try it too...\n\n";
    
    static let STATUS_BAR_HEIGHT = UIApplication.shared.statusBarFrame.size.height
    
    static let INVALID_INDEX:Int = -1;
    
    //MARK: Padding Constants
    static let PADDING_1  = CGFloat(1.0)
    static let PADDING_2  = CGFloat(2.0)
    static let PADDING_3  = CGFloat(3.0)
    static let PADDING_4  = CGFloat(4.0)
    static let PADDING_5  = CGFloat(5.0)
    static let PADDING_6  = CGFloat(6.0)
    static let PADDING_7  = CGFloat(7.0)
    static let PADDING_8  = CGFloat(8.0)
    static let PADDING_9  = CGFloat(9.0)
    static let PADDING_10 = CGFloat(10.0)
    static let PADDING_15 = CGFloat(15.0)
    static let PADDING_20 = CGFloat(20.0)
    static let PADDING_25 = CGFloat(25.0)
    static let PADDING_30 = CGFloat(30.0)
    static let PADDING_35 = CGFloat(35.0)
    static let PADDING_40 = CGFloat(40.0)
    static let PADDING_45 = CGFloat(45.0)
    static let PADDING_50 = CGFloat(50.0)
    static let PADDING_55 = CGFloat(55.0)
    static let PADDING_60 = CGFloat(60.0)
    static let PADDING_75 = CGFloat(75.0)
    static let PADDING_80 = CGFloat(80.0)
    static let PADDING_90 = CGFloat(90.0)
    static let PADDING_100 = CGFloat(100.0)
    static let PADDING_110 = CGFloat(110.0)
    static let PADDING_120 = CGFloat(120.0)
    static let PADDING_150 = CGFloat(150.0)
    static let PADDING_200 = CGFloat(200.0)
    static let PADDING_220 = CGFloat(220.0)
    static let PADDING_1000 = CGFloat(1000.0)
    
    //MARK: Colors
   
    static let COLOR_1:UIColor=UIColor(red:249.0/255.0,green:190.0/255.0,blue:64.0/255.0,alpha:1.0); // HoneyComb Yellow color
    static let COLOR_2:UIColor=UIColor(red:40.0/255.0,green:72.0/255.0,blue:108.0/255.0,alpha:1.0); // Onboarding bg blue color
    
    //MARK: Error Messages
    static let TEXT_1 = "You have been logged off as you have logged in on another device."
    
    
    
    
    
    //MARK: Notification Names
//    static let TRIP_STARTED_NOTIFICATION = NSNotification.Name("TRIP_STARTED");
    
    
    //MARK: Fonts
    static let APPLE_SD_GOTHIC_REGULAR = "AppleSDGothicNeo-Regular"
    static let APPLE_SD_GOTHIC_BOLD = "AppleSDGothicNeo-Bold"
    static let HELVETICA_NEUE = "HelveticaNeue"
    static let HELVETICA_NEUE_BOLD = "HelveticaNeue-Bold"
    
    //MARK: System Info
    static let iOSVersion = NSString(string: UIDevice.current.systemVersion).doubleValue
    static let iOS11 = iOSVersion >= 11
    static let iOS8 = iOSVersion >= 8
    static let iOS7 = iOSVersion >= 7 && iOSVersion < 8
    
    //MARK: Enums
    enum LineViewOrientation
    {
        case Horizontal
        case Vertical
    }
    
    enum ButtonStates
    {
        case Normal
        case Selected
        case Disabled
    }
    
    enum TableViewDataSortingOrder
    {
        case Ascending
        case Descending
    }
    
    enum DateAndTimeManager_TimeFormat
    {
        case hours_minutes  // am/pm format
        case Hours_Minutes  // 24 hrs format
        case Hours_Minutes_Seconds
    }
}
