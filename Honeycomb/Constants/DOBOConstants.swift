//
//  BaseDO.swift
//  Honeycomb
//
//  Created by Vineet Sansare on 05/10/17.
//  Copyright © 2017 Trunk. All rights reserved.
//
import Foundation


struct DOBOConstants
{
    static let BASE_URL_OF_SERVER:String="http://34.202.232.126/honeycomb/";   // Test Server (Public)
    
    enum HTTPMethodType
    {
        case Get
        case Post
        case Put
        case Delete
        case Patch
    }
}
