//
//  ForgetPwVC.swift
//  Honeycomb
//
//  Created by iSparsh on 10/30/17.
//  Copyright © 2017 Trunk. All rights reserved.
//

import UIKit

class ForgotPwVC: UIViewController,UITextFieldDelegate
{
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.emailTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    // Dismiss keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    
    @IBAction func backbuttonAction(_ sender: Any)
    {
     
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func resetPassword(_ sender: Any)
    {
        self.callForgotPasswordAPI();
    }
    
    //MARK: Private Methods (Vineet)
    
    private func callForgotPasswordAPI()
    {
        let forgotPasswordDO = ForgotPasswordDO();
        
        print("Email - \(emailTextField.text)");
        
        forgotPasswordDO.prepareRequestWithParameters(parameters: emailTextField.text as AnyObject?)
        forgotPasswordDO.queryTheServer  {
            (baseDO:BaseDO!) in
            
            if(baseDO.serverErrorBO != nil)
            {
                if(baseDO.serverErrorBO!.isSessionExpired)
                {
                    
                }
                else if(baseDO.serverErrorBO!.errorText != nil)
                {
                    let errorTextToDisplay = baseDO.serverErrorBO!.errorText!;
                    
                    var titleText = "Something's Wrong!";
                    
                    if(errorTextToDisplay == "Further instructions have been sent to your registered Email-id.")
                    {
                        titleText = "";
                    }
                    
                    let alertController = UIUtil.standardUIAlertController(title: titleText, message: errorTextToDisplay, preferredStyle: .alert)
                    self.present(alertController, animated: true, completion: nil);
                }
            }
            else
            {
                // Response received successfully.
                
                if let forgotPasswordBO = (forgotPasswordDO.response as! ForgotPasswordResponse).forgotPasswordBO
                {
                    if(forgotPasswordBO.returnMsg == nil)
                    {
                        print("\n\n\n\n Message - \(forgotPasswordBO.message!)");
                    }
                    else
                    {
                        self.present(UIUtil.standardUIAlertController(title: "Alert!", message: forgotPasswordBO.message!, preferredStyle: .alert), animated: true, completion: nil);
                    }
                }
            }
        }
    }

}
